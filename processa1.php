<?php

include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

if ($_SESSION[$_idTarefa] > 0 && $_SESSION[$_idTarefa] != 1) {
	header('location:login.php');
}
	
include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

$sql = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idEvento = ' . $_SESSION[$_evento];
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);

if (isset($_GET['idem']) && $_GET['idem'] > 0) {
	$sql = 'update eventos_medicos set ';
	if ($_GET['ind'] == 1) $sql .= 'indRSVP';
	if ($_GET['ind'] == 2) $sql .= 'indAereo';
	if ($_GET['ind'] == 3) $sql .= 'indTransfer';
	if ($_GET['ind'] == 4) $sql .= 'indHospedagem';
	if ($_GET['ind'] == 5) $sql .= 'indInscricao';
	$sql .= ' = ' . $_GET['valor'];
	$atualizaTudo = 0;
	if ($_GET['ind'] == 1 && $_GET['valor'] == 0) {
		$sql .= ', indAereo = 0, indTransfer = 0, indHospedagem = 0, indInscricao = 0';
		$atualizaTudo = 1;
	}
	$sql .= ' where idEvento_medico = '.$_GET['idem'];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	atualizaPorcConclusao($conexao,$_SESSION[$_evento],1);
	if ($atualizaTudo == 1) {
		atualizaPorcConclusao($conexao,$_SESSION[$_evento],2);
		atualizaPorcConclusao($conexao,$_SESSION[$_evento],3);
		atualizaPorcConclusao($conexao,$_SESSION[$_evento],4);
	}
}

if (isset($_GET['muda']) && $_GET['muda'] > 0) {
	$sql = 'update eventos_medicos set idStatusConvite = ' . $_GET['para'];
	$sql .= ' where idEvento_medico = '.$_GET['muda'];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
}

if (strlen($_GET['filtro']) > 2) {
	$txtFiltro = $_GET['filtro'];
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<?php
include 'head.php';
?>
<script type="text/javascript">


function salva(id, ind, valor) {
//	alert(id+':'+ind+':'+valor);
	window.location = 'processa1.php?idem='+id+'&ind='+ind+'&valor='+valor;
}

function muda(id, valor) {
//	alert(id+'-'+valor);	
	window.location = 'processa1.php?muda='+id+'&para='+valor;
}

function filtrar() {
	var fil = document.getElementById("txtFiltro").value;
	window.location = 'processa1.php?filtro='+fil;
}


</script>
</head>

<body>
<?php
include 'header.php';
?> 
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
<?php
include 'navbar.php';
?>		
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                <a href="index.php">Dashboard</a> > R.S.V.P.
<?php

// SELECT em.*,m.*, u.idUsuario, u.idUsuarioPai
// FROM eventos_medicos em 
// join medicos m on em.idMedico = m.idMedico 
// join usuarios_medicos um on m.idMedico = um.idMedico
// join usuarios u on um.idUsuario = u.idUsuario
// WHERE em.idEvento = 1

$sql = 'SELECT em.*,m.* FROM eventos_medicos em join medicos m on em.idMedico = m.idMedico WHERE em.idEvento = ' . $_SESSION[$_evento];

if ($_SESSION[$_adm] > 2) {
	$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
}

if (strlen($txtFiltro) > 2) {
	$sql .= ' and m.nome like "%' . $txtFiltro . '%"';	
}

$sql .= ' order by m.nome';

$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$total = mysqli_num_rows($qr);
$totRSVP1 = 0;
$totAereo1 = 0;
$totTransfer1 = 0;
$totHospedagem1 = 0;
$totInscricao1 = 0;
$totRSVP0 = 0;
$totAereo0 = 0;
$totTransfer0 = 0;
$totHospedagem0 = 0;
$totInscricao0 = 0;
?>
                <h2 class="login-form-text2"><?php echo $qt['nome'] . ' ('. $total . ')'; ?></h2>
				<input type="text" id="txtFiltro" name="txtFiltro" value="<?php echo $txtFiltro; ?>">
                <button class="btn waves-effect waves-light" type="button" onClick="javascript:filtrar();">Filtrar<i class="material-icons right">send</i>
                </button>
                 <table class="bordered striped responsive-table">
                        <thead>
                          <tr>
                              <th data-field="nome">Nome</th>
                              <th data-field="rsvp">Aceitou?</th>
                              <th data-field="aereo">Aéreo?</th>
                              <th data-field="aereo">Transfer?</th>
                              <th data-field="aereo">Hospedagem?</th>
                              <th data-field="aereo">Inscrição?</th>
                          </tr>
                        </thead>
                        <tbody>
                         <!--começa o loop-->
<?php
echo '<tr>';
echo '<td>Totais</td>';
echo '<td><span class="totais" id="totRSVP1"></span><span class="totais" id="totRSVP0"></span></td>';
echo '<td><span class="totais" id="totAereo1"></span><span class="totais" id="totAereo0"></span></td>';
echo '<td><span class="totais" id="totTransfer1"></span><span class="totais" id="totTransfer0"></span></td>';
echo '<td><span class="totais" id="totHospedagem1"></span><span class="totais" id="totHospedagem0"></span></td>';
echo '<td><span class="totais" id="totInscricao1"></span><span class="totais" id="totInscricao0"></span></td>';
echo '</tr>';

while ($qe = mysqli_fetch_assoc($qr)) {


	if ($_SESSION[$_adm] == 1) {
		echo '<tr><td><div class="row"><div class="col l6 s12"><a href="altera-medicos.php?id=' . $qe['idMedico'] . '">' . $qe['nome'] . '</a></div><div class="col l3 s12"><div class="chip"><a href="edita-historico.php?id=' . $qe['idEvento_medico'] . '">Historico</a></div></div>';
		echo '<div class="col l3 s12"><select id="cboStatusConvite" name="cboStatusConvite" class="browser-default"  style="height: 2rem;" onChange="javascript:muda('.$qe['idEvento_medico'].',this.value);">
		<option value="0" selected>nenhum</option>';
		$sql = 'SELECT * FROM statusConvite order by idStatusConvite';
	//	echo $sql;
		$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
		while ($qt2 = mysqli_fetch_assoc($qr2)) {
			echo '<option value="' . $qt2['idStatusConvite'] . '"';
				if ($qt2['idStatusConvite'] == $qe['idStatusConvite']) echo ' selected';
			echo '>' . $qt2['descricao'] . '</option>'; 	
		}
		echo '</select></div></div></div></td>';
	}
	else {
		echo '<tr><td nowrap><a href="exibe-evento-medico.php?id=' . $qe['idEvento_medico'] . '">' . $qe['nome'] . '</a>';
		$sql3 = 'SELECT * FROM statusConvite where idStatusConvite = ' . $qe['idStatusConvite'];
		$qr3 = mysqli_query($conexao,$sql3) or die(mysqli_error());
		while ($qt3 = mysqli_fetch_assoc($qr3)) {
			echo ': ('.$qt3['descricao'].') ';
		}
		echo '</td>';
	}
	
	echo '<td nowrap><i ';
	if ($_SESSION[$_adm] == 1) echo 'onClick="javascript:salva('.$qe['idEvento_medico'].',1,1);" ';
	echo 'class="fa fa-check '.($qe['indRSVP']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',1,0);" ';
	echo 'class="fa fa-ban '.($qe['indRSVP']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';
	
	echo '<td nowrap><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',2,1);" ';
	echo 'class="fa fa-check '.($qe['indAereo']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',2,0);" ';
	echo 'class="fa fa-ban '.($qe['indAereo']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '<td nowrap><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',3,1);" ';
	echo 'class="fa fa-check '.($qe['indTransfer']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',3,0);" ';
	echo 'class="fa fa-ban '.($qe['indTransfer']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '<td nowrap><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',4,1);" ';
	echo 'class="fa fa-check '.($qe['indHospedagem']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',4,0);" ';
	echo 'class="fa fa-ban '.($qe['indHospedagem']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '<td nowrap><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',5,1);" ';
	echo 'class="fa fa-check '.($qe['indInscricao']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',5,0);" ';
	echo 'class="fa fa-ban '.($qe['indInscricao']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '</tr>';

	if ($qe['indRSVP']==1) $totRSVP1++;
	if ($qe['indRSVP']==0) $totRSVP0++;

	if ($qe['indAereo']==1) $totAereo1++;
	if ($qe['indAereo']==0) $totAereo0++;

	if ($qe['indTransfer']==1) $totTransfer1++;
	if ($qe['indTransfer']==0) $totTransfer0++;

	if ($qe['indHospedagem']==1) $totHospedagem1++;
	if ($qe['indHospedagem']==0) $totHospedagem0++;

	if ($qe['indInscricao']==1) $totInscricao1++;
	if ($qe['indInscricao']==0) $totInscricao0++;

}
echo '<tr>';
echo '<td>Totais</td>';
echo '<td><span class="totais">'.$totRSVP1.'</span><span class="totais">'.$totRSVP0.'</span></td>';
echo '<td><span class="totais">'.$totAereo1.'</span><span class="totais">'.$totAereo0.'</span></td>';
echo '<td><span class="totais">'.$totTransfer1.'</span><span class="totais">'.$totTransfer0.'</span></td>';
echo '<td><span class="totais">'.$totHospedagem1.'</span><span class="totais">'.$totHospedagem0.'</span></td>';
echo '<td><span class="totais">'.$totInscricao1.'</span><span class="totais">'.$totInscricao0.'</span></td>';
echo '</tr>';
?>
                          <!--termina o loop-->
                        </tbody>
                </table>
				<br>
				Totais dos status<br>
				<div class="row">
<?php
$sql = 'SELECT count(em.idStatusConvite) as qtde, sc.descricao FROM eventos_medicos em join statusConvite sc on em.idStatusConvite = sc.idStatusConvite WHERE idEvento = ' . $_SESSION[$_evento] . ' group by sc.descricao';
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
while ($qe = mysqli_fetch_assoc($qr)) {
	echo '<div class="chip">' . $qe['descricao'] . ' = ' . $qe['qtde'] . '</div>  ';
}
	?>
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
            <?php //include 'sidebar.php'; ?>
            <!-- LEFT RIGHT SIDEBAR NAV-->

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <!--bootstrap-->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!--moments and locale-->
    <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <!--date time picker-->
    <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <script type="text/javascript">
$('#totRSVP1').text(<?php echo $totRSVP1; ?>);
$('#totRSVP0').text(<?php echo $totRSVP0; ?>);

$('#totAereo1').text(<?php echo $totAereo1; ?>);
$('#totAereo0').text(<?php echo $totAereo0; ?>);

$('#totTransfer1').text(<?php echo $totTransfer1; ?>);
$('#totTransfer0').text(<?php echo $totTransfer0; ?>);

$('#totHospedagem1').text(<?php echo $totHospedagem1; ?>);
$('#totHospedagem0').text(<?php echo $totHospedagem0; ?>);

$('#totInscricao1').text(<?php echo $totInscricao1; ?>);
$('#totInscricao0').text(<?php echo $totInscricao0; ?>);
</script>

    <script type="text/javascript">
        $('#dtConvite').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
        $('#dtResposta').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
    </script>
</body>
</html>