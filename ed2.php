<?php
include("conn/conn.php");
echo date_default_timezone_get();
date_default_timezone_set('America/Sao_Paulo');
if ($_SESSION[$_codigo] < 1) {
header('location:login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'");
//echo 'U:'.$_SESSION['idUsuario']; die();

$usuario = $_SESSION[$_codigo];
if ($_SESSION[$_adm] > 1) {
	header("location:index.php");
	exit();
}


    $sql = 'UPDATE eventos_medicos SET historicoContato = "' . ' [' .  
	date('d-m-Y H:i') . '] ' . ' [' .  
	date('d-m-Y H:i') . '] ' . '"  WHERE idEvento_medico = 1';
	echo $sql;

die();

$usuarioaltera = $_GET['id'];
$sql_medicos = 'SELECT m.*, em.* FROM medicos m join eventos_medicos em on m.idMedico = em.idMedico WHERE idEvento_medico = ' . $_GET['id'];
//echo $sql_medicos;
$qr = mysqli_query($conexao,$sql_medicos) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);


?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
</head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
                    include 'navbar.php';
                    ?>

                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <a href="index.php">Dashboard</a> > <a href="index.php">Lista de Médicos</a> > Historico de contato
                                <form id="form1" name="form1" class="col s12" action="edita-historico.php?id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" value="cadastrar" id="cadastrar" name="cadastrar">
                                    <h2 class="login-form-text2">Historico de contato com Médico</h2>
                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <span>Nome Completo: <?php echo $qt['nome'] ?></span>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>Empresa :<?php echo $qt['empresa'] ?></span>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>Especialidade: <?php echo $qt['especialidade'] ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <span>Email: <?php echo $qt['email'] ?></span>
                                                
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>CRM: <?php echo $qt['crm'] ?></span>
                                                
                                            </div>
                                            <div class="input-field col l2 s12">
                                                 <span>RG: <?php echo $qt['rg'] ?></span>
                                                
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>CPF: <?php echo $qt['cpf'] ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <span><?php echo $qt['telefone'] ?></span>
                                            </div>
                                            <div class="input-field col l6 s12">
                                               <span><?php echo $qt['celular'] ?></span>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <br>
                                        <div class="row">
                                            <div class="input-field col s12">
                                            <textarea readonly id='historicoAntigo' name="historicoAntigo" class="materialize-textarea"><?php echo strip_tags($qt['historicoContato']) ?></textarea>
                                            <label class="active" for="historicoAntigo">Historico</label>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="input-field col s12">
                                            <textarea id='historico' name="historico" class="materialize-textarea"></textarea>
                                            <label class="active" for="historico">Novo Historico</label>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <br>
                                        <button class="btn waves-effect waves-light" type="submit" name="atualizar" id="atualizar">atualizar
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                                <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <?php include('sidebar.php');?>
                            <!-- LEFT RIGHT SIDEBAR NAV-->
                            </div>
                            <!-- END WRAPPER -->
                </div>
                <!-- END MAIN -->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
                <!-- START FOOTER -->
                <footer class="page-footer">
                    <div class="footer-copyright">
                        <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
                    </div>
                </footer>
                <!-- END FOOTER -->
                <!-- ================================================
    Scripts
    ================================================ -->
                <script src="https://code.jquery.com/jquery-1.11.2.js"></script>
                <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                <!--materialize js-->
                <script type="text/javascript" src="js/materialize.min.js"></script>
                <!-- upload -->
                <script type="text/javascript" src="js/dropify.js"></script>
                <!--scrollbar-->
                <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
                <!--plugins.js - Some Specific JS codes for Plugin Settings-->
                <script type="text/javascript" src="js/plugins.min.js"></script>
                <script src="ckeditor/ckeditor.js"></script>
                <script>
                    CKEDITOR.replace('historico', { });
                </script>
                <script type="text/javascript">
                    $(document).ready(function() {
                        Materialize.updateTextFields();
                        $('.dropify').dropify();
                        tinymce.init({
                                selector: '#observacoes',
                                plugins: [
                                    "advlist autolink lists link image charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table contextmenu paste"
                                ],
                                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                            });
                    });
                </script>
    </body>
    </html>