<span class="user_online" id="<?php echo $_SESSION[$_codigo];?>"></span>
    <aside id="users_online">
        <!-- início do CHAT -->
        <ul id="chat-out" class="side-nav rightside-navigation chat-collapsible" data-collapsible="expandable">
            <?php
            $sql = 'SELECT * FROM usuarios WHERE idNivel = 1 and idUsuario != ' . $_SESSION[$_codigo];
            echo $sql;
            $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
            while($row = mysqli_fetch_assoc($qr)) {
                $foto = ($row['foto'] == '') ? 'default.jpg' : $row['foto'];
                $blocks = array(); 
				explode(',', $row['blocks']);
                $agora = date('Y-m-d H:i:s');
                if(!in_array($_SESSION['idUsuario'], $blocks)){
                    $status = 'on';
                    if($agora >= $row['limite']){
                        $status = 'off';
                    }
            ?>
            <li id="<?php echo $row['idUsuario'];?>">
                <div class="imgSmall"><img src="<?php echo $foto;?>" border="0" /></div>
                <a href="#" class="truncate" id="<?php echo $_SESSION[$_codigo].':'.$row['idUsuario'];?>" class="comecar">
                    <?php echo utf8_encode($row['nome']);?>
                </a>
                <span id="<?php echo $row['id'];?>" class="status <?php echo $status;?>"></span>
            </li>
            <?php 
                }
            }

            ?>
        </ul>
    </aside>
