<?php
$lifetime=600;
session_start();
setcookie(session_name(),session_id(),time()+$lifetime);
$_SESSION = array();

include ("conn/conn.php");
$_SESSION[$_codigo] = 0;
mysqli_query($conexao,"SET NAMES 'utf8'"); 

if (isset($_POST['btnLogin'])) {
	//echo 'passou';
	if (($_POST['username'] != '') && ($_POST['password'] != '')) {
		$login = $_POST['username'];
		$senha = $_POST['password'];
		$sql_usuario = 'SELECT u.*,n.txtNivel FROM usuarios u join nivels n on u.idNivel = n.idNivel WHERE login = "' . $login . '" AND senha = "' . $senha . '" and u.ativo = 1 AND u.idNivel > 0';
//		echo $sql_usuario;
		$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
		while ($qt = mysqli_fetch_assoc($qr)) {
			$_SESSION[$_codigo] = $qt['idUsuario'];
			$_SESSION[$_adm] = $qt['idNivel'];
			$_SESSION[$_foto] = $qt['foto'];
			if (strlen($qt['foto']) < 4) $_SESSION[$_foto] = 'anonymous.gif';
			$_SESSION[$_nome] = $qt['nome'];
			$_SESSION[$_role] = $qt['txtNivel'];
			$_SESSION[$_email] = $qt['email'];
			$_SESSION[$_horaLogin] = time();
			$_SESSION[$_idTarefa] = $qt['idTarefa'];
			$_SESSION[$_idEvento] = $qt['idEvento'];
	
			$agora = date('Y-m-d H:i:s');
			$limite = date('Y-m-d H:i:s', strtotime('+2 min'));
			$sql_usuario = 'UPDATE usuarios SET horario = "' . $agora . '", limite = "' . $limite . '" WHERE idUsuario = ' . $qt['idUsuario'];
//			echo $sql_usuario; die();
			$qr2 = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
			$_SESSION['emailLogado'] = $email;
			header('location:index.php');
			exit();
		}
		if ($_SESSION[$_codigo] > '0') {
			header('location:index.php');
		} else {
           $erro = 1;
        }
			
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="msapplication-tap-highlight" content="no">
	<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
	<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
	<title>Sistema RSVP</title>

	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="assets/css/estilo.css">
	<link rel="stylesheet" href="assets/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- Opera Speed Dial Favicon -->
	  <link rel="icon" type="image/png" href1="favicon.png" />
				
	<!-- Standard Favicon -->
	  <link rel="icon" type="image/x-icon" href="favicon.ico" />

	<!-- For iPhone 4 Retina display: -->
	  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="favicon.png">

	<!-- For iPad: -->
	  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="favicon.png">

	<!-- For iPhone: -->
	  <link rel="apple-touch-icon-precomposed" href="favicon.png">
	<!-- Compiled and minified JavaScript -->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
<style>
html,
body {
    height: 100%;
}
html {
    display: table;
    margin: auto;
}
body {
    display: table-cell;
    vertical-align: middle;
}
</style>  
</head>

<body class="yellow">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->



  <div id="login-page" class="row" style="width: 350px;">
    <div class="col s12 z-depth-4 card-panel">
      <form class="login-form" name="login" method="post" action="" id="login" enctype="application/x-www-form-urlencoded">
        <div class="row">
          <div class="input-field col s12 center">
            <img src="assets/img/logo-ganbatte.jpg" alt="" class="responsive-img valign profile-image-login">
            <p class="center login-form-text">ganbatte</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <input id="username" name="username" type="text" required>
            <label for="username" class="center-align"><i class="fa fa-user" aria-hidden="true"></i> Username</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <input id="password" name="password" type="password" required>
            <label for="password"><i class="fa fa-key" aria-hidden="true"></i> Password</label>
          </div>
        </div>
        <div class="row">
        <div id="message"></div>
          <div class="input-field col s12 center">
            <button class="btn waves-effect waves-light black" type="submit" value="Login" id="btnLogin" name="btnLogin">Login
                <i class="fa fa-sign-in" aria-hidden="true"></i>
              </button>
        </div>
        <div class="row">
          <div class="input-field col l12 s12">
              <p class="margin center medium-small"><a href="assets/css/page-forgot-password.html">Esqueceu a senha ?</a></p>
          </div>          
        </div>

      </form>
    </div>
  </div>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php 
	if ($erro == 1) {?> 
	<script>$( "#login-page" ).effect( "shake" );</script>
	<?php
}
?>
  </body>
</html>
