<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-tap-highlight" content="no">
<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
<title>Ganbatte - R.S.V.P.</title>

<!-- Compiled and minified CSS -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="assets/css/style.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="assets/css/materialize.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/estilo.css">
<link rel="stylesheet" href="assets/css/dropify.css">
<link rel="stylesheet" href="assets/css/bootstrap-material-datetimepicker.css" />

<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
<script type="text/javascript" src="js/jquery.sortable.js"></script> 
<script type="text/javascript" src="js/dropify.js"></script>
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<script type="text/javascript" src="js/plugins.min.js"></script> 

<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
<link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href="js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href="js/plugins/fullcalendar/css/fullcalendar.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<!-- <script type="text/javascript" src="js/jquery_play.js"></script> -->
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <script type="text/javascript">
//        $.noConflict();
    </script>

  <div class="progress">
      <div class="indeterminate"></div>
  </div>