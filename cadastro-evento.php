<?php
session_start();
include ("conn/conn.php");

if ($_SESSION[$codigo] < 1) {
	header('location:login.php');
}

include ("conn/conn.php");
mysql_query("SET NAMES 'utf8'"); 

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<?php
include 'head.php';
?>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main">
<!-- START WRAPPER -->
<div class="wrapper">
<?php
include 'navbar.php';
?>		

<!-- START CONTENT -->
<section id="content"> 
  <!--start container-->
  <div class="container">
    <h2 class="login-form-text2">Cadastro de eventos</h2>
    <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <input id="first_name" type="text" class="validate">
          <label for="first_name">Nome do Evento</label>
        </div>
        <div class="input-field col s6">
          <select>
            <option value="" disabled selected>Selecione o Lab</option>
            <option value="1">Lab 1</option>
            <option value="2">Lab 2</option>
            <option value="3">Lab 3</option>
          </select>
          <label>Lab</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col l6 s12">
          <label for="password">Data</label>
          <input type="text" class="datepicker picker__input" readonly id="P1894905190" tabindex="-1" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="P1894905190_root">
          <div class="picker" id="P1894905190_root" tabindex="0" aria-hidden="true">
            <div class="picker__holder">
              <div class="picker__frame">
                <div class="picker__wrap">
                  <div class="picker__box">
                    <div class="picker__date-display">
                      <div class="picker__weekday-display">Tuesday</div>
                      <div class="picker__month-display">
                        <div>Jul</div>
                      </div>
                      <div class="picker__day-display">
                        <div>5</div>
                      </div>
                      <div class="picker__year-display">
                        <div>2016</div>
                      </div>
                    </div>
                    <div class="picker__calendar-container">
                      <div class="picker__header">
                        <select class="picker__select--month browser-default" aria-controls="P1894905190_table" title="Select a month" disabled="disabled">
                          <option value="0">January</option>
                          <option value="1">February</option>
                          <option value="2">March</option>
                          <option value="3">April</option>
                          <option value="4">May</option>
                          <option value="5">June</option>
                          <option value="6" selected="">July</option>
                          <option value="7">August</option>
                          <option value="8">September</option>
                          <option value="9">October</option>
                          <option value="10">November</option>
                          <option value="11">December</option>
                        </select>
                        <select class="picker__select--year browser-default" aria-controls="P1894905190_table" title="Select a year" disabled="disabled">
                          <option value="2009">2009</option>
                          <option value="2010">2010</option>
                          <option value="2011">2011</option>
                          <option value="2012">2012</option>
                          <option value="2013">2013</option>
                          <option value="2014">2014</option>
                          <option value="2015">2015</option>
                          <option value="2016" selected="">2016</option>
                          <option value="2017">2017</option>
                          <option value="2018">2018</option>
                          <option value="2019">2019</option>
                          <option value="2020">2020</option>
                          <option value="2021">2021</option>
                          <option value="2022">2022</option>
                          <option value="2023">2023</option>
                        </select>
                        <div class="picker__nav--prev" data-nav="-1" role="button" aria-controls="P1894905190_table" title="Previous month"> </div>
                        <div class="picker__nav--next" data-nav="1" role="button" aria-controls="P1894905190_table" title="Next month"> </div>
                      </div>
                      <table class="picker__table" id="P1894905190_table" role="grid" aria-controls="P1894905190" aria-readonly="true">
                        <thead>
                          <tr>
                            <th class="picker__weekday" scope="col" title="Sunday">S</th>
                            <th class="picker__weekday" scope="col" title="Monday">M</th>
                            <th class="picker__weekday" scope="col" title="Tuesday">T</th>
                            <th class="picker__weekday" scope="col" title="Wednesday">W</th>
                            <th class="picker__weekday" scope="col" title="Thursday">T</th>
                            <th class="picker__weekday" scope="col" title="Friday">F</th>
                            <th class="picker__weekday" scope="col" title="Saturday">S</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1466910000000" role="gridcell" aria-label="26 June, 2016">26</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1466996400000" role="gridcell" aria-label="27 June, 2016">27</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1467082800000" role="gridcell" aria-label="28 June, 2016">28</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1467169200000" role="gridcell" aria-label="29 June, 2016">29</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1467255600000" role="gridcell" aria-label="30 June, 2016">30</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1467342000000" role="gridcell" aria-label="1 July, 2016">1</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1467428400000" role="gridcell" aria-label="2 July, 2016">2</div></td>
                          </tr>
                          <tr>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1467514800000" role="gridcell" aria-label="3 July, 2016">3</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1467601200000" role="gridcell" aria-label="4 July, 2016">4</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus picker__day--today picker__day--highlighted" data-pick="1467687600000" role="gridcell" aria-label="5 July, 2016" aria-activedescendant="true">5</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1467774000000" role="gridcell" aria-label="6 July, 2016">6</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1467860400000" role="gridcell" aria-label="7 July, 2016">7</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1467946800000" role="gridcell" aria-label="8 July, 2016">8</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468033200000" role="gridcell" aria-label="9 July, 2016">9</div></td>
                          </tr>
                          <tr>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468119600000" role="gridcell" aria-label="10 July, 2016">10</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468206000000" role="gridcell" aria-label="11 July, 2016">11</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468292400000" role="gridcell" aria-label="12 July, 2016">12</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468378800000" role="gridcell" aria-label="13 July, 2016">13</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468465200000" role="gridcell" aria-label="14 July, 2016">14</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468551600000" role="gridcell" aria-label="15 July, 2016">15</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468638000000" role="gridcell" aria-label="16 July, 2016">16</div></td>
                          </tr>
                          <tr>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468724400000" role="gridcell" aria-label="17 July, 2016">17</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468810800000" role="gridcell" aria-label="18 July, 2016">18</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468897200000" role="gridcell" aria-label="19 July, 2016">19</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1468983600000" role="gridcell" aria-label="20 July, 2016">20</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469070000000" role="gridcell" aria-label="21 July, 2016">21</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469156400000" role="gridcell" aria-label="22 July, 2016">22</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469242800000" role="gridcell" aria-label="23 July, 2016">23</div></td>
                          </tr>
                          <tr>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469329200000" role="gridcell" aria-label="24 July, 2016">24</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469415600000" role="gridcell" aria-label="25 July, 2016">25</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469502000000" role="gridcell" aria-label="26 July, 2016">26</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469588400000" role="gridcell" aria-label="27 July, 2016">27</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469674800000" role="gridcell" aria-label="28 July, 2016">28</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469761200000" role="gridcell" aria-label="29 July, 2016">29</div></td>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469847600000" role="gridcell" aria-label="30 July, 2016">30</div></td>
                          </tr>
                          <tr>
                            <td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1469934000000" role="gridcell" aria-label="31 July, 2016">31</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1470020400000" role="gridcell" aria-label="1 August, 2016">1</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1470106800000" role="gridcell" aria-label="2 August, 2016">2</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1470193200000" role="gridcell" aria-label="3 August, 2016">3</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1470279600000" role="gridcell" aria-label="4 August, 2016">4</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1470366000000" role="gridcell" aria-label="5 August, 2016">5</div></td>
                            <td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1470452400000" role="gridcell" aria-label="6 August, 2016">6</div></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="picker__footer">
                      <button class="btn-flat picker__today" type="button" data-pick="1467687600000" aria-controls="P1894905190" disabled="disabled">Today</button>
                      <button class="btn-flat picker__clear" type="button" data-clear="1" aria-controls="P1894905190" disabled="disabled">Clear</button>
                      <button class="btn-flat picker__close" type="button" data-close="true" aria-controls="P1894905190" disabled="disabled">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="input-field col l6 s12">
          <input id="end" type="text" class="validate active">
          <label for="end">Endereço</label>
        </div>
      </div>
      <br>
      <div class="divider"></div>
      <br>
      <div class="row">
      <h2 class="login-form-text2">Etapas</h2>
      
      <ul class="collection sortable">
        <li class="collection-item">
          <div class="row">
            <div class="col l6 s12"><br>
              <input class="with-gap" name="group3" type="checkbox" id="etapa1" />
              <label for="etapa1">Enviar RSVP</label>
            </div>
            <div class="col l6 s12">
              <input id="duracao" placeholder="2" type="text" class="validate">
              <label for="duracao">Dias</label>
            </div>
          </div>
        </li>
        <li class="collection-item">
          <div class="row">
            <div class="col l6 s12"><br>
              <input class="with-gap col l3 s12" name="group3" type="checkbox" id="etapa2"  />
              <label for="etapa2">Fechar Hotel</label>
            </div>
            <div class="col l6 s12">
              <input id="hotel" placeholder="10" type="text" class="validate">
              <label for="hotel">Dias</label>
            </div>
          </div>
        </li>
        <li class="collection-item">
          <div class="row">
            <div class="col l6 s12"><br>
              <input class="with-gap col l3 s12" name="group3" type="checkbox" id="etapa3"  />
              <label for="etapa3">Emitir Bilhetes Aéreos</label>
            </div>
            <div class="col l6 s12">
              <input id="aereo" placeholder="5" type="text" class="validate">
              <label for="aereo">Dias</label>
            </div>
          </div>
        </li>
        <li class="collection-item">
          <div class="row">
            <div class="col l6 s12"><br>
              <input class="with-gap col l3 s12" name="group3" type="checkbox" id="etapa4"  />
              <label for="etapa4">Contratar Transporte</label>
            </div>
            <div class="col l6 s12">
              <input id="traslado" placeholder="15" type="text" class="validate">
              <label for="traslado">Dias</label>
            </div>
          </div>
        </li>
        </div>
      </ul>
      </div>
      <div class="row">
        <button class="btn waves-effect waves-light" type="submit" name="action">cadastrar <i class="material-icons right">send</i> </button>
      </div>
    </form>
  </div>
  </div>
  <!--end container--> 
</section>
<!-- END CONTENT --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START RIGHT SIDEBAR NAV-->
<aside id="right-sidebar-nav">
  <ul id="chat-out" class="side-nav rightside-navigation">
    <li class="li-hover"> <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
      <div id="right-search" class="row">
        <form class="col s12">
          <div class="input-field"> <i class="mdi-action-search prefix"></i>
            <input id="icon_prefix" type="text" class="validate">
            <label for="icon_prefix">Search</label>
          </div>
        </form>
      </div>
    </li>
    <li class="li-hover">
      <ul class="chat-collapsible" data-collapsible="expandable">
        <li>
          <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
          <div class="collapsible-body recent-activity">
            <div class="recent-activity-list chat-out-list row">
              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i> </div>
              <div class="col s9 recent-activity-list-text"> <a href="#">just now</a>
                <p>Jim Doe Purchased new equipments for zonal office.</p>
              </div>
            </div>
            <div class="recent-activity-list chat-out-list row">
              <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i> </div>
              <div class="col s9 recent-activity-list-text"> <a href="#">Yesterday</a>
                <p>Your Next flight for USA will be on 15th August 2015.</p>
              </div>
            </div>
            <div class="recent-activity-list chat-out-list row">
              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> </div>
              <div class="col s9 recent-activity-list-text"> <a href="#">5 Days Ago</a>
                <p>Natalya Parker Send you a voice mail for next conference.</p>
              </div>
            </div>
            <div class="recent-activity-list chat-out-list row">
              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i> </div>
              <div class="col s9 recent-activity-list-text"> <a href="#">Last Week</a>
                <p>Jessy Jay open a new store at S.G Road.</p>
              </div>
            </div>
            <div class="recent-activity-list chat-out-list row">
              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> </div>
              <div class="col s9 recent-activity-list-text"> <a href="#">5 Days Ago</a>
                <p>Natalya Parker Send you a voice mail for next conference.</p>
              </div>
            </div>
          </div>
        </li>
        <li>
          <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
          <div class="collapsible-body sales-repoart">
            <div class="sales-repoart-list  chat-out-list row">
              <div class="col s8">Target Salse</div>
              <div class="col s4"><span id="sales-line-1"></span> </div>
            </div>
            <div class="sales-repoart-list chat-out-list row">
              <div class="col s8">Payment Due</div>
              <div class="col s4"><span id="sales-bar-1"></span> </div>
            </div>
            <div class="sales-repoart-list chat-out-list row">
              <div class="col s8">Total Delivery</div>
              <div class="col s4"><span id="sales-line-2"></span> </div>
            </div>
            <div class="sales-repoart-list chat-out-list row">
              <div class="col s8">Total Progress</div>
              <div class="col s4"><span id="sales-bar-2"></span> </div>
            </div>
          </div>
        </li>
        <li>
          <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
          <div class="collapsible-body favorite-associates">
            <div class="favorite-associate-list chat-out-list row">
              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
              <div class="col s8">
                <p>Eileen Sideways</p>
                <p class="place">Los Angeles, CA</p>
              </div>
            </div>
            <div class="favorite-associate-list chat-out-list row">
              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
              <div class="col s8">
                <p>Zaham Sindil</p>
                <p class="place">San Francisco, CA</p>
              </div>
            </div>
            <div class="favorite-associate-list chat-out-list row">
              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> </div>
              <div class="col s8">
                <p>Renov Leongal</p>
                <p class="place">Cebu City, Philippines</p>
              </div>
            </div>
            <div class="favorite-associate-list chat-out-list row">
              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
              <div class="col s8">
                <p>Weno Carasbong</p>
                <p>Tokyo, Japan</p>
              </div>
            </div>
            <div class="favorite-associate-list chat-out-list row">
              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> </div>
              <div class="col s8">
                <p>Nusja Nawancali</p>
                <p class="place">Bangkok, Thailand</p>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </li>
  </ul>
</aside>
<!-- LEFT RIGHT SIDEBAR NAV-->

</div>
<!-- END WRAPPER -->

</div>
<!-- END MAIN --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START FOOTER -->
<footer class="page-footer">
  <div class="footer-copyright">
    <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
  </div>
</footer>
<!-- END FOOTER --> 

<!-- ================================================
    Scripts
    ================================================ --> 

<!-- jQuery Library --> 
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
<!--materialize js--> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
<!--sortable-->
<script type="text/javascript" src="js/jquery.sortable.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="js/plugins.min.js"></script> 
<!--nestable --> 
<script type="text/javascript" src="js/plugins/jquery.nestable/jquery.nestable.js"></script> 
<!-- Toast Notification --> 
<script type="text/javascript">
    // Toast Notification
    $(window).load(function() {
        setTimeout(function() {
            Materialize.toast('<span>Evento OK. Emitir voucher?</span><a class="btn-flat yellow-text" href="#">Sim<a>', 7000);
        }, 15000);
    });
</script> 
<script>
  $(document).ready(function() {
    Materialize.updateTextFields();
  });
</script> 
<script>
    $('.sortable').sortable();
</script>
</ul></body>
</html>