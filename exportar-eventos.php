<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
	exit();
}

if ($_SESSION[$_adm] > 1) {
	header("location:login.php");
	exit();
}


include ("conn/conn.php");
//mysqli_query($conexao,"SET NAMES 'utf8'"); 

$erros = '';
$idEvento = $_GET['idEvento'];


/*
0:NOME COMPLETO
1:EMPRESA
2:ESPECIALIDADE
3:CRM
4:RG
5:CPF
6:DATA DE NASCIMENTO
7:PASSAPORTE
8:VALIDADE
9:NACIONALIDADE
10:TELEFONE
11:CELULAR
12:E-MAIL
13:ENDEREÇO
14:BAIRRO
15:CEP
16:CIDADE
17:UF
18:CONSULTOR
19:GD
20:GR
---------- novos campos de patrocinio --------
21:AEREO
22:TRANSFER
23:HOSPEDAGEM
24:INSCRICAO
25:dtCheckIn
26:dtCheckOut
27:nroQuarto
28:obsHospedagem

*/

$sql = 'SELECT em.idEvento_medico, m.nome';
$sql .= ', m.empresa';
$sql .= ', m.especialidade';
$sql .= ', m.crm';
$sql .= ', m.rg';
$sql .= ', m.cpf';
$sql .= ', CASE WHEN m.dtNascimento <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(m.dtNascimento,"%d-%m-%Y") END AS dtNascimento';
$sql .= ', m.passaporte';
$sql .= ', CASE WHEN m.validade <  "2000-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(m.validade,"%d-%m-%Y") END AS validade';
$sql .= ', m.nacionalidade';
$sql .= ', m.telefone';
$sql .= ', m.celular';
$sql .= ', m.email';
$sql .= ', m.endereco';
$sql .= ', m.bairro';
$sql .= ', m.cep';
$sql .= ', m.cidade';
$sql .= ', m.UF';
$sql .= ', u5.nome as Rep';
$sql .= ', u4.nome as GD';
$sql .= ', u3.nome as GR';

$sql .= ', CASE WHEN em.indRSVP = "1" THEN  "Sim" WHEN em.indRSVP = "0" THEN  "Nao" ELSE "NR" END AS indRSVP';
$sql .= ', CASE WHEN em.indAereo = "1" THEN  "Sim" WHEN em.indAereo = "0" THEN  "Nao" ELSE "NR" END AS indAereo';
$sql .= ', CASE WHEN em.indTransfer = "1" THEN  "Sim" WHEN em.indTransfer = "0" THEN  "Nao" ELSE "NR" END AS indTransfer';
$sql .= ', CASE WHEN em.indHospedagem = "1" THEN  "Sim" WHEN em.indHospedagem = "0" THEN  "Nao" ELSE "NR" END AS indHospedagem';
$sql .= ', CASE WHEN em.indInscricao = "1" THEN  "Sim" WHEN em.indInscricao = "0" THEN  "Nao" ELSE "NR" END AS indInscricao';
$sql .= ', em.nroInscricao';
$sql .= ', sc.descricao as descricaoStatusConvite';
$sql .= ', CASE WHEN em.dtCheckIn <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dtCheckIn,"%d-%m-%Y") END AS dtCheckIn';
$sql .= ', CASE WHEN em.dtCheckOut <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dtCheckOut,"%d-%m-%Y") END AS dtCheckOut';
$sql .= ', em.nroQuarto';
$sql .= ', em.obsHospedagem';

$sql .= ', CASE WHEN em.dataTransfer <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dataTransfer,"%d-%m-%Y") END AS dataTransfer';
$sql .= ', em.horaTransfer';
$sql .= ', em.motorista';
$sql .= ', em.txtVeiculo';
$sql .= ', em.origemTransfer';
$sql .= ', em.destinoTransfer';

$sql .= ', CASE WHEN em.dataTransfer2 <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dataTransfer2,"%d-%m-%Y") END AS dataTransfer2';
$sql .= ', em.horaTransfer2';
$sql .= ', em.motorista2';
$sql .= ', em.txtVeiculo2';
$sql .= ', em.origemTransfer2';
$sql .= ', em.destinoTransfer2';

$sql .= ', CASE WHEN em.dataTransfer3 <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dataTransfer3,"%d-%m-%Y") END AS dataTransfer3';
$sql .= ', em.horaTransfer3';
$sql .= ', em.motorista3';
$sql .= ', em.txtVeiculo3';
$sql .= ', em.origemTransfer3';
$sql .= ', em.destinoTransfer3';

$sql .= ', CASE WHEN em.dataTransfer4 <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dataTransfer4,"%d-%m-%Y") END AS dataTransfer4';
$sql .= ', em.horaTransfer4';
$sql .= ', em.motorista4';
$sql .= ', em.txtVeiculo4';
$sql .= ', em.origemTransfer4';
$sql .= ', em.destinoTransfer4';

$sql .= ', CASE WHEN em.dataTransfer5 <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dataTransfer5,"%d-%m-%Y") END AS dataTransfer5';
$sql .= ', em.horaTransfer5';
$sql .= ', em.motorista5';
$sql .= ', em.txtVeiculo5';
$sql .= ', em.origemTransfer5';
$sql .= ', em.destinoTransfer5';

$sql .= ', CASE WHEN em.dataTransfer6 <  "1900-01-01" THEN  "00-00-0000" ELSE DATE_FORMAT(em.dataTransfer6,"%d-%m-%Y") END AS dataTransfer6';
$sql .= ', em.horaTransfer6';
$sql .= ', em.motorista6';
$sql .= ', em.txtVeiculo6';
$sql .= ', em.origemTransfer6';
$sql .= ', em.destinoTransfer6';

$sql .= ', em.historicoContato';

$sql .= ' FROM eventos_medicos em join medicos m on em.idMedico = m.idMedico left join usuarios u5 on em.idUsuarioNivel5 = u5.idUsuario left join usuarios u3 on em.idUsuarioNivel3 = u3.idUsuario left join usuarios u4 on em.idUsuarioNivel4 = u4.idUsuario left join statusConvite sc on em.idStatusConvite = sc.idStatusConvite where em.idEvento = ' . $idEvento . ' order by m.nome';
$arquivo = 'planilha'.$idEvento.'_'.date('Y-m-d_H:i').'.xls';
$html = '';
$contaLinha = 0;
$html .= '<table>';
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
while ($qe = mysqli_fetch_assoc($qr)) {
	$linha = '<tr>';
	$cabLinha = '<tr>';
	$coluna = 0;
	foreach($qe as $chave => $valor) {
		if ($coluna > 0) {
			$linha .= '<td>';
			if ($coluna == 69) 
				$linha .= ''.utf8_decode( (strip_tags($valor))  ).'';
			else 
				$linha .= utf8_decode($valor);
			$linha .= '</td>';
			if ($contaLinha == 0) {
				$cabLinha .= '<td><b>'.$chave.'</b></td>';
			}
		}
		$coluna++;
	}

	$sql = 'select * from voos where idEvento_Medico = "'.$qe['idEvento_medico'].'" order by idaVolta, idVoo limit 10';
	//echo $sql; die();
	$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
	$contaVoo = 0;

	while ($qe2 = mysqli_fetch_assoc($qr2)) {
		$contaVoo += 1;
		$linha .= '<td>'.($qe2['idaVolta'] == 1 ? 'Ida' : 'Volta').'</td>';		
		$linha .= '<td>'.$qe2['eTicket'].'</td>';		
		$linha .= '<td>'.$qe2['loc'].'</td>';		
		$linha .= '<td>'.   date('d-m-Y', strtotime($qe2['dtVoo']))  .'</td>';		
		$linha .= '<td>'.$qe2['ciaAerea'].'</td>';		
		$linha .= '<td>'.$qe2['nroVoo'].'</td>';		
		$linha .= '<td>'.$qe2['origemVoo'].'</td>';		
		$linha .= '<td>'.$qe2['destinoVoo'].'</td>';		
		$linha .= '<td>'.$qe2['horaSaidaVoo'].'</td>';		
		$linha .= '<td>'.$qe2['horaChegadaVoo'].'</td>';		
		if ($contaLinha == 0) {
			$cabLinha .= '<td>Voo '.$contaVoo.'</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>';
		}
	}


	while ($contaVoo++ < 10) {
		$linha .= '<td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>';		
		if ($contaLinha == 0) {
			$cabLinha .= '<td>Voo</td><td>E-Ticket</td><td>LOC</td><td>Data</td><td>Cia aerea</td><td>Nro voo</td><td>Origem</td><td>Destino</td><td>Hora saida</td><td>Hora chegada</td>';		
		}
	}


	if ($contaLinha == 0) {
		$sql = 'SELECT p.* from perguntas p where idEvento = ' . $idEvento . ' order by idPergunta';
		//echo $sql;
		$qr3 = mysqli_query($conexao,$sql) or die(mysqli_error());
	
		while ($qe3 = mysqli_fetch_assoc($qr3)) {
			$cabLinha .= '<td>P'.utf8_decode($qe3['txtPergunta']).'</td>';		
		}
		$cabLinha .= '<td> </td>';
	}

	$sql = 'SELECT p.*, e.idEvento, e.nome, r.resposta, r.dtResposta from perguntas p join eventos e on p.idEvento = e.idEvento join respostasMedicos r on p.idPergunta = r.idPergunta join eventos_medicos em on r.idMedico = em.idMedico where idEvento_Medico = '.$qe['idEvento_medico'] . ' order by idPergunta';
	//echo $sql;
	$qr3 = mysqli_query($conexao,$sql) or die(mysqli_error());
	$dtResp = '<td> </td>';
	while ($qe3 = mysqli_fetch_assoc($qr3)) {
		$linha .= '<td>'.utf8_decode($qe3['resposta']).'</td>';
		$dtResp = '<td>'.date("d-m-Y H:i", strtotime($qe3['dtResposta'])).'</td>';
	}
	$linha .= $dtResp;

//

	if ($contaLinha == 0) {
		$cabLinha .= '</tr>';
		$html .= $cabLinha;
	}

	$linha .= '</tr>';
	$html .= $linha;
	
	$contaLinha += 1;
}

$html .= '</table>';


header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/x-msexcel;  charset=\"UTF-8\"");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

echo $html;

?>