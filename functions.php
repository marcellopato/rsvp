<?php

//global $conexao;
function atualizaPorcConclusao($conexao,$idEvento, $idTarefa) {

	if ($idTarefa == 1) {
		$sql1 = 'SELECT count(em1.indRSVP) as pronto FROM eventos_medicos em1 WHERE em1.indRSVP > -1 and em1.idEvento = ' . $idEvento;
		$sql2 = 'SELECT count(em1.indRSVP) as total FROM eventos_medicos em1 WHERE em1.idEvento = ' . $idEvento;
	}
	if ($idTarefa == 2) {
		$sql1 = 'SELECT count(distinct em.idMedico) as pronto FROM voos v join eventos_medicos em on v.idEvento_medico = em.idEvento_medico WHERE em.indAereo = 1 and em.idEvento = ' . $idEvento;
		$sql2 = 'SELECT count(distinct em.idMedico) as total FROM eventos_medicos em WHERE em.indAereo = 1 and em.idEvento = ' . $idEvento;
	}
	if ($idTarefa == 3) {
		$sql1 = 'SELECT count(em1.origemTransfer) as pronto FROM eventos_medicos em1 WHERE em1.indTransfer = 1 and  em1.origemTransfer != "" and em1.idEvento = ' . $idEvento;
		$sql2 = 'SELECT count(em1.origemTransfer) as total FROM eventos_medicos em1 WHERE em1.indTransfer = 1 and em1.idEvento = ' . $idEvento;
	}
	if ($idTarefa == 4) {
		$sql1 = 'SELECT count(em1.dtCheckIn) as pronto FROM eventos_medicos em1 WHERE em1.indHospedagem = 1 and em1.dtCheckIn > "2010-01-01" and em1.idEvento = ' . $idEvento;
		$sql2 = 'SELECT count(em1.dtCheckIn) as total FROM eventos_medicos em1 WHERE em1.indHospedagem = 1 and em1.idEvento = ' . $idEvento;
	}
	if ($idTarefa == 5) {
		$sql1 = 'SELECT count(em1.nroInscricao) as pronto FROM eventos_medicos em1 WHERE em1.indInscricao = 1 and em1.nroInscricao != "" and em1.idEvento = ' . $idEvento;
		$sql2 = 'SELECT count(em1.nroInscricao) as total FROM eventos_medicos em1 WHERE em1.indInscricao = 1 and em1.idEvento = ' . $idEvento;
	}



//echo $sql1;	die();
	$qr = mysqli_query($conexao,$sql1) or die(mysqli_error());
	$qt = mysqli_fetch_assoc($qr);
	$pronto = $qt['pronto'];
	
//echo $sql2;	
	$qr = mysqli_query($conexao,$sql2) or die(mysqli_error());
	$qt = mysqli_fetch_assoc($qr);
	$total = $qt['total'];
	if ($total > 0) $porc = ($pronto/$total)*100;
	else $porc = 0;
//echo $pronto.' - '.$total.' '.$porc;
	$porc = str_replace(',','.',$porc);
	if ($pronto == $total) $indCconclusao = 1;
	else $indCconclusao = 0;
	$sql = 'update eventos_tarefas set porcConclusao = ' . $porc . ', indConclusao = ' . $indCconclusao . '  WHERE idEvento = ' . $idEvento . ' and idTarefa = ' . $idTarefa;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());


}


function uniqid_base36($more_entropy=false) {
    $s = uniqid('', $more_entropy);
    if (!$more_entropy)
        return base_convert($s, 16, 36);
        
    $hex = substr($s, 0, 13);
    $dec = $s[13] . substr($s, 15); // skip the dot
    return base_convert($hex, 16, 36) . base_convert($dec, 10, 36);
}

function cpfLimpo($entrada) {
	//echo $entrada;
	$saida = str_replace(' ','',$entrada);
	$saida = str_replace('.','',$saida);
	$saida = str_replace('-','',$saida);
	return $saida;	
}

function achaLaboratorio($conexao,$vId) {
	$nivel = 5;
	while ($nivel > 2) {
		$sql = 'SELECT idUsuario, idUsuarioPai, idNivel FROM usuarios WHERE idUsuario = "' . $vId . '"';
		//echo $sql;
		$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
		$qt = mysqli_fetch_assoc($qr);
		$vId = $qt['idUsuarioPai'];
		$nivel = $qt['idNivel'];
	}
	return $qt['idUsuario'];
}

function montaFamilia($conexao,$vId) {
	$nivel = 5;
	$familia[] = array();
	for ($x = 0; $x < 6; $x++)
		$familia[$x] = 0;

	while ($nivel > 2) {
		$sql = 'SELECT idUsuario, idUsuarioPai, idNivel FROM usuarios WHERE idUsuario = "' . $vId . '"';
		//echo $sql;
		$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
		$qt = mysqli_fetch_assoc($qr);
		$vId = $qt['idUsuarioPai'];
		$nivel = $qt['idNivel'];
		$familia[$nivel] = $qt['idUsuario'];
	}
	return $familia;
}


function achaUsuario($conexao,$nome, $idNivel) {
	$idUsuario = 0;
	$sql = 'SELECT idUsuario FROM usuarios WHERE nome = "' . $nome . '" and idNivel = '.$idNivel;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt = mysqli_fetch_assoc($qr)) {
		if (achaLaboratorio($conexao,$qt['idUsuario']) == $_POST['cboUsuario'])
			$idUsuario = $qt['idUsuario'];
	}
	return $idUsuario;
}


function achaUsuarioComPai($conexao,$nome, $idNivel) {
	$idUsuarioComPai[] = array();
	$idUsuarioComPai[0] = 0;
	$sql = 'SELECT idUsuario, idUsuarioPai FROM usuarios WHERE nome = "' . $nome . '" and idNivel = '.$idNivel;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt = mysqli_fetch_assoc($qr)) {
		if (achaLaboratorio($conexao,$qt['idUsuario']) == $_POST['cboUsuario'])
			$idUsuarioComPai[0] = $qt['idUsuario'];
			$idUsuarioComPai[1] = $qt['idUsuarioPai'];
	}
	return $idUsuarioComPai;
}

function alteraUsuarioPai($conexao,$idUsuario, $idPai) {
	$sql = 'update usuarios set idUsuarioPai = ' . $idPai . ' WHERE idUsuario = ' . $idUsuario;
	//echo $sql;
	mysqli_query($conexao,$sql) or die(mysqli_error());
}


function criaUsuario($conexao,$nome, $idNivel, $idPai) {
	$idUsuario = 0;
	if (strlen($nome) > 0) {
		$sql = 'insert into usuarios (nome, login, senha, idNivel, idUsuarioPai) values ("' . $nome . '", "' . $nome . '", "ppp123", ' . $idNivel . ', ' . $idPai . ') ';
		$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
		$idUsuario = mysqli_insert_id($conexao);
	}
	return $idUsuario;
}

function str_to_utf8 ($str) { 
    
//	echo '('.$str;
    if (mb_detect_encoding($str, 'UTF-8', true) === false) { 
	    $str = utf8_encode($str); 
//		echo ' - '.$str;
    }
//	echo ')   ';
    return $str;
}

function pegaCell($excelObj, $pColuna, $pLinha) {
	$retu = utf8_decode($excelObj->getActiveSheet()->getCellByColumnAndRow($pColuna, $pLinha)->getValue());
//if ($pColuna == 5) echo 'XXX'.$retu.'xxx';
//	$retu = ($excelObj->getActiveSheet()->getCellByColumnAndRow($pColuna, $pLinha)->getValue());
	return utf8_encode($retu);
}

/*
function removeAcentos($string, $slug = false) {
  $string = strtolower($string);
  // Código ASCII das vogais
  $ascii['a'] = range(224, 230);
  $ascii['e'] = range(232, 235);
  $ascii['i'] = range(236, 239);
  $ascii['o'] = array_merge(range(242, 246), array(240, 248));
  $ascii['u'] = range(249, 252);
  // Código ASCII dos outros caracteres
  $ascii['b'] = array(223);
  $ascii['c'] = array(231);
  $ascii['d'] = array(208);
  $ascii['n'] = array(241);
  $ascii['y'] = array(253, 255);
  foreach ($ascii as $key=>$item) {
    $acentos = '';
    foreach ($item AS $codigo) $acentos .= chr($codigo);
    $troca[$key] = '/['.$acentos.']/i';
  }
  $string = preg_replace(array_values($troca), array_keys($troca), $string);
  // Slug?
  if ($slug) {
    // Troca tudo que não for letra ou número por um caractere ($slug)
    $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
    // Tira os caracteres ($slug) repetidos
    $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
    $string = trim($string, $slug);
  }
  return $string;
}
*/

?>
