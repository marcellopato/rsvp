<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'"); 

if (isset($_POST['btnNovo'])) {
//	echo $_POST['hUsuarioPai']; die();
	$_SESSION['idUsuario'] = 0; //$_POST['hUsuarioPai'];
	header('location:cadastro-usuarios.php');
}
	


	
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php
include 'head.php';
?>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main"> 
	<!-- START WRAPPER -->
	<div class="wrapper">
		<?php
include 'navbar.php';
?>
		
		<!-- START CONTENT -->
		<section id="content"> 
			<!--start container-->
			<div class="container">
			 <a href="index.php">Dashboard</a> > Lista de usuários
			<h2 class="login-form-text2">Lista de usuários</h2> <a class="btn right" href="cadastro-usuarios.php" style="margin-top: -56px;">Novo</a>
              <div class="divider"></div>
               <!--<ul class="pagination">
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="waves-effect"><a href="#!">2</a></li>
                <li class="waves-effect"><a href="#!">3</a></li>
                <li class="waves-effect"><a href="#!">4</a></li>
                <li class="waves-effect"><a href="#!">5</a></li>
                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
              </ul><br>-->
               <table class="bordered highlight responsive-table">
                <thead>
                  <tr>
                      <th data-field="id">Nome</th>
                      <th data-field="name">Nível</th>
                      <th data-field="price">Email</th>
                      <th data-field="price">Telefone</th>
                      <th data-field="price">Celular</th>
                      <th data-field="price">Ativo</th>
                  </tr>
                </thead>
                <tbody>
            <?php
                $sql = 'SELECT u.*,n.txtNivel FROM usuarios u join nivels n on u.idNivel = n.idNivel WHERE u.idNivel = 1';
            	//echo $sql;
                $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                while ($qt = mysqli_fetch_assoc($qr)) {
                    if ($qt['ativo'] == '1') { 
                        $ativo = '<i class="fa fa-check green-text" aria-hidden="true"></i>';
                    } else {
                        $ativo = '<i class="fa fa-ban red-text" aria-hidden="true"></i>';
                    }
                    echo '<tr><td><a href="altera-usuarios.php?id=' . $qt['idUsuario'] . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;' . $qt['nome'] . '</td><td>' . $qt['txtNivel'] . '</td><td>' . $qt['email'] . '</td><td>' . $qt['telefone'] . '</td><td>' . $qt['celular'] . '</td><td>' . $ativo . '</td></tr>';
                }
            ?>
               </tbody>
              </table>
              <br>
               <!--<div class="divider"></div>
              <ul class="pagination">
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="waves-effect"><a href="#!">2</a></li>
                <li class="waves-effect"><a href="#!">3</a></li>
                <li class="waves-effect"><a href="#!">4</a></li>
                <li class="waves-effect"><a href="#!">5</a></li>
                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
              </ul><br>-->
		<!--end container--> 
		</section>
		<!-- END CONTENT --> 
		<!-- //////////////////////////////////////////////////////////////////////////// --> 
		<!-- START RIGHT SIDEBAR NAV-->
		<aside id="right-sidebar-nav">

		</aside>
		<!-- LEFT RIGHT SIDEBAR NAV--> 
	</div>
	<!-- END WRAPPER --> 
</div>
<!-- END MAIN --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START FOOTER -->
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
	</div>
</footer>
<!-- END FOOTER --> 
<!-- ================================================
    Scripts
    ================================================ --> 
<!-- jQuery Library --> 
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script> 
<!--materialize js--> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
<!-- upload --> 
<script type="text/javascript" src="js/dropify.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="js/plugins.min.js"></script> 
<!-- Toast Notification --> 
<script>
$(document).ready(function() {


});  
</script>
<script>
<?php if (isset($_GET['alt']) && ($_GET['alt']) == '1') { ?>
    var $toastContent = $('<span>Cadastro atualizado com SUCESSO!</span>');
    Materialize.toast($toastContent, 5000);
<?php } ?>
<?php if (isset($_GET['cad']) && ($_GET['cad']) == '1') { ?>
    var $toastContent = $('<span>Cadastro realizado com SUCESSO!</span>');
    Materialize.toast($toastContent, 5000);
<?php } ?>    
</script>
</body>
</html>
