<?php
include("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
header('location:login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'");
//echo 'U:'.$_SESSION['idUsuario']; die();

$usuario = $_SESSION[$_codigo];
if ($_SESSION[$_adm] > 4) {
	header("location:index.php");
	exit();
}

	$sql_usuario = 'SELECT us.*,ni.txtNivel FROM usuarios us JOIN nivels ni on ni.idNivel = us.idNivel WHERE ((us.idUsuario = "' . $usuario . '"))';
	//echo $sql_usuario;
	$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
	$qt = mysqli_fetch_assoc($qr);


if (isset($_POST['cadastrar'])){
    $uploaddir = 'assets/img/';
    $uploadfile = $uploaddir . basename($_FILES['input-file-now']['name']);
    move_uploaded_file($_FILES['input-file-now']['tmp_name'], $uploadfile);
    $avatar = $uploadfile;
       
    $nome = $_POST['nome'];
    $login = $_POST['login'];
    $pass = $_POST['senha'];
    $nivel = $_POST['cboNivelAtual'];
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    $cel = $_POST['celular'];
    $rua = $_POST['end'];
    $avatar;
    $insere_usuario = 'INSERT INTO sistemar_rsvp.usuarios (idUsuario, nome, login, senha, idNivel, idUsuarioPai, email, foto, telefone, celular, endereco) VALUES (NULL, "' . $nome . '", "' . $login . '", "' . $pass . '", 1, 0,"' . $email . '", "' . $avatar . '", "' . $tel . '", "' . $cel . '", "' . $rua . '");';
    //echo $insere_usuario;
    mysqli_query($conexao,$insere_usuario) or die(mysqli_error());
    
    header("location:lista-usuarios.php?cad=1");
    exit();
}


?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
    </head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
include 'navbar.php';
?>

                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                               <a href="index.php">Dashboard</a> > <a href="lista-usuarios.php">Lista de usuários</a> > Cadastro de usuários
                                <form id="form1" name="form1" class="col s12" action="" method="post" enctype="multipart/form-data">
                                    <input type="hidden" value="cadastrar" id="cadastrar" name="cadastrar">
                                    <h2 class="login-form-text2">Cadastro de usuários</h2>
                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="nome" name="nome" type="text" class="validate" required>
                                                <label for="nome">Nome Completo</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <input id="login" name="login" type="text" class="validate" required>
                                                <label class="active" for="login">Nome de Usuário</label>
                                            </div>


                                            <div class="input-field col l6 s12">
                                                <input id="senha" name="senha" type="password" class="validate" required>
                                                <label class="active" for="senha">Senha do Usuário</label>
                                            </div>
                                            <div class="input-field col l6 s12">
                                                <input id="end" type="text" class="validate active" required>
                                                <label class="active" for="end">Endereço</label>
                                            </div>
                                            <div class="input-field col l6 s12">
                                                <input id="email" name="email" type="email" class="validate active" required>
                                                <label class="active" for="email">Email</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col l6 s12">
                                                <input type="file" id="input-file-now" name="input-file-now" class="dropify" data-allowed-file-extensions="png jpg jpeg gif bmp" data-show-errors="true" data-max-file-size="3M" data-default-file="http://sistemarsvp.com.br/assets/img/anonymous.gif">
                                            </div>
                                            <div class="input-field col  l6 s12">
                                                <div class="input-field">
                                                    <input id="tel" name="tel" type="text" class="validate" required>
                                                    <label class="active" for="tel">Telefone <small><i>separe por virgulas</i></small></label>
                                                </div>
                                                <div class="input-field">
                                                    <input id="celular" name="celular" type="text" class="validate" required>
                                                    <label class="active" for="celular">Celular <small><i>separe por virgulas</i></small></label>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="divider"></div><br>
                                        <div>
                                            <button class="btn waves-effect waves-light pink" type="submit" id="cadastrar" name="cadastrar">cadastrar <i class="material-icons right">send</i> </button>
                                        </div>
                                    </div>
                                </form>
                                <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <aside id="right-sidebar-nav">
                            <ul id="chat-out" class="side-nav rightside-navigation">
                                <li class="li-hover"> <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                                    <div id="right-search" class="row">
                                        <form class="col s12">
                                            <div class="input-field"> <i class="mdi-action-search prefix"></i>
                                                <input id="icon_prefix" type="text" class="validate">
                                                <label for="icon_prefix">Search</label>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li class="li-hover">
                                    <ul class="chat-collapsible" data-collapsible="expandable">
                                        <li>
                                            <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                                            <div class="collapsible-body recent-activity">
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i> </div>
                                                    <div class="col s9 recent-activity-list-text"> <a href="#">just now</a>
                                                        <p>Jim Doe Purchased new equipments for zonal office.</p>
                                                    </div>
                                                </div>
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i> </div>
                                                    <div class="col s9 recent-activity-list-text"> <a href="#">Yesterday</a>
                                                        <p>Your Next flight for USA will be on 15th August 2015.</p>
                                                    </div>
                                                </div>
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> </div>
                                                    <div class="col s9 recent-activity-list-text"> <a href="#">5 Days Ago</a>
                                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                                    </div>
                                                </div>
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i> </div>
                                                    <div class="col s9 recent-activity-list-text"> <a href="#">Last Week</a>
                                                        <p>Jessy Jay open a new store at S.G Road.</p>
                                                    </div>
                                                </div>
                                                <div class="recent-activity-list chat-out-list row">
                                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> </div>
                                                    <div class="col s9 recent-activity-list-text"> <a href="#">5 Days Ago</a>
                                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                                            <div class="collapsible-body sales-repoart">
                                                <div class="sales-repoart-list  chat-out-list row">
                                                    <div class="col s8">Target Salse</div>
                                                    <div class="col s4"><span id="sales-line-1"></span> </div>
                                                </div>
                                                <div class="sales-repoart-list chat-out-list row">
                                                    <div class="col s8">Payment Due</div>
                                                    <div class="col s4"><span id="sales-bar-1"></span> </div>
                                                </div>
                                                <div class="sales-repoart-list chat-out-list row">
                                                    <div class="col s8">Total Delivery</div>
                                                    <div class="col s4"><span id="sales-line-2"></span> </div>
                                                </div>
                                                <div class="sales-repoart-list chat-out-list row">
                                                    <div class="col s8">Total Progress</div>
                                                    <div class="col s4"><span id="sales-bar-2"></span> </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                                            <div class="collapsible-body favorite-associates">
                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="col s4"><img src="../images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
                                                    <div class="col s8">
                                                        <p>Eileen Sideways</p>
                                                        <p class="place">Los Angeles, CA</p>
                                                    </div>
                                                </div>
                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="col s4"><img src="../images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
                                                    <div class="col s8">
                                                        <p>Zaham Sindil</p>
                                                        <p class="place">San Francisco, CA</p>
                                                    </div>
                                                </div>
                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="col s4"><img src="../images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> </div>
                                                    <div class="col s8">
                                                        <p>Renov Leongal</p>
                                                        <p class="place">Cebu City, Philippines</p>
                                                    </div>
                                                </div>
                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="col s4"><img src="../images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
                                                    <div class="col s8">
                                                        <p>Weno Carasbong</p>
                                                        <p>Tokyo, Japan</p>
                                                    </div>
                                                </div>
                                                <div class="favorite-associate-list chat-out-list row">
                                                    <div class="col s4"><img src="../images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> </div>
                                                    <div class="col s8">
                                                        <p>Nusja Nawancali</p>
                                                        <p class="place">Bangkok, Thailand</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </aside>
                        <!-- LEFT RIGHT SIDEBAR NAV-->
                        </div>
                        <!-- END WRAPPER -->
                </div>
                <!-- END MAIN -->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
                <!-- START FOOTER -->
                <footer class="page-footer">
                    <div class="footer-copyright">
                        <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
                    </div>
                </footer>
                <!-- END FOOTER -->
                <!-- ================================================
    Scripts
    ================================================ -->
                <!-- jQuery Library -->
                <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
                <!--materialize js-->
                <script type="text/javascript" src="js/materialize.min.js"></script>
                <!-- upload -->
                <script type="text/javascript" src="js/dropify.js"></script>
                <!--scrollbar-->
                <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
                <!--plugins.js - Some Specific JS codes for Plugin Settings-->
                <script type="text/javascript" src="js/plugins.min.js"></script>
                <!-- Toast Notification -->
                <script type="text/javascript">
                    $(document).ready(function() {
                        Materialize.updateTextFields();
                        $('.dropify').dropify();
                    });

                </script>
                
    </body>

    </html>
