<?php
include ("conn/conn.php");
//echo $_SESSION[$_codigo];
if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}



mysqli_query($conexao,"SET NAMES 'utf8'"); 

if (isset($_POST['hIdEvento']) && $_POST['hIdEvento'] > 0) {
	$_SESSION[$_evento] = $_POST['hIdEvento'];
}

if (isset($_POST['hIdTarefa']) && $_POST['hIdTarefa'] > 0) {
	if ($_POST['hIdTarefa'] < 11)
		header('location:processa'.$_POST['hIdTarefa'].'.php');	
	else
		header('location:processaX.php?i='.$_POST['hIdTarefa']);	
}

function achaNovidade($conexao) {
	$sql = 'select count(distinct e.idEvento) as conta from eventos e join eventos_medicos em on e.idEvento = em.idEvento where e.idStatusEvento < 70 and e.idEvento not in ( SELECT distinct e.idEvento from eventos e join eventos_medicos em on e.idEvento = em.idEvento where e.idStatusEvento < 70 and indRSVP > -1 )';
	//select count(distinct idEvento) as conta from eventos_medicos where idEvento not in ( SELECT distinct e.idEvento from eventos e join eventos_medicos em on e.idEvento = em.idEvento where dtEvento > now() and indRSVP > -1 )
	
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	$conta = 0;
	while ($qt = mysqli_fetch_assoc($qr)) {
		$conta = $qt['conta'];	
	}
	return $conta;
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<?php
include 'head.php';
?>

<script type="text/javascript">

function evento(idEvento) {
	$('#hIdEvento').val(idEvento);
	document.form1.submit();
}

function processa(idTarefa) {
	$('#hIdTarefa').val(idTarefa);
	document.form1.submit();
}

</script>
</head>

<body>
<?php
include 'header.php';
?>
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
<?php
include 'navbar.php';
?>		

<!-- START CONTENT -->
<section id="content">
<form id="form1" name="form1" method="post" action="index.php">
<input type="hidden" id="hIdTarefa" name="hIdTarefa" value="0">
<input type="hidden" id="hIdEvento" name="hIdEvento" value="<?php echo ($_SESSION[$_evento] > 0 ? $_SESSION[$_evento] : 0); ?>">
</form>
    <!--start container-->
    <div class="container">
        <!-- //////////////////////////////////////////////////////////////////////////// -->
<?php 
//    echo 'XXXXXXXXXXXXXXXXXX'.$qt['idNivel'].'xxxxxxxxxxxxxxxxxx';
		if ($_SESSION[$_adm] < 2 && $_SESSION[$_idTarefa] == 0) { 
?>
        <!--card stats start-->
        <div id="card-stats">
            <div class="row" id="topo">
                <?php
                $sql = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idStatusEvento = 70 order by dtEvento desc limit 1';
                //echo $sql_usuario;
                $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                $id = 0;
                $nome = 'N/D';
                $particip = 0;
                while ($qt = mysqli_fetch_assoc($qr)) {
                    $id = $qt['idEvento'];	
                    $nome = $qt['nome'];	
                }

				$particip = 0;
                if ($id > 0) {
                    $sql = 'select porcConclusao from eventos_tarefas et where et.idEvento = '.$id.' and et.idTarefa = 1';	
                    $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                    while ($qt = mysqli_fetch_assoc($qr)) {
                        $particip = $qt['porcConclusao'];	
                    }
                }

                ?>						
                <div class="col s12 m6 l3">
                    <div class="card">
                        <div class="card-content green white-text">
                            <p class="card-stats-title truncate"><i class="fa fa-users" aria-hidden="true"></i> <?php echo $nome; ?></p>
                            <h4 class="card-stats-number"><?php echo $particip ?>% OK</h4>
                            <p class="card-stats-compare"><i class="fa fa-check" aria-hidden="true"></i><span class="green-text text-lighten-5">Todas Tarefas conluídas</span></p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l3">
                    <div class="card">
                        <div class="card-content blue lighten-1 white-text">
                            <p class="card-stats-title"><i class="fa fa-file" aria-hidden="true"></i> Novos Eventos</p>
                            <h4 class="card-stats-number"><?php echo achaNovidade($conexao); ?></h4>
                        </div>
                    </div>
                </div>
                <?php // inicio hoje
                $sql = 'SELECT e.nome, e.local, e.dtEvento, e.hrEvento FROM eventos e WHERE e.idStatusEvento < 70 and date(dtEvento) = curdate() order by dtEvento desc limit 1 ';
                //echo $sql_usuario;
                $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                $id = 0;
                $nome = '';
                $local = '';
                $data = '';
                while ($qt = mysqli_fetch_assoc($qr)) {
                    $id = $qt['idEvento'];	
                    $nome = $qt['nome'];	
                    $local = $qt['local'];	
                    $data = $qt['dtEvento'];	
                    $hora = $qt['hrEvento'];	
                }


                ?>						
                <div class="col s12 m6 l3">
                    <div class="card">
                        <div class="card-content orange white-text">
                            <p class="card-stats-title"><i class="fa fa-exclamation" aria-hidden="true"></i> Evento inicia </p>
                            <h4 class="card-stats-number">HOJE</h4>
<?php if (strlen($nome) > 1) { ?>
                            <p class="card-stats-compare"><?php echo $nome.' - '.$local.' as '.date("d-m-Y", strtotime($data)).' - '.date("H:i", strtotime($hora)); ?><br>&nbsp;</p>
<?php } ?>
                        </div>
                    </div>
                </div>
                <?php // inicio hoje e ainda tem pendencia
                $sql = 'SELECT distinct e.idEvento, e.nome, e.local, e.dtEvento, e.hrEvento, t.tarefa FROM eventos e join eventos_tarefas et on e.idEvento = et.idEvento join tarefas t on et.idTarefa = t.idTarefa WHERE e.idStatusEvento < 70 and date(dtEvento) = curdate() and et.indConclusao = 0 and et.ativo = 1 order by dtEvento limit 1 ';
                //echo $sql_usuario;
                $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                $id = 0;
                $nome = '';
                $local = '';
                $data = '';
                $tarefa = '';
                while ($qt = mysqli_fetch_assoc($qr)) {
                    $id = $qt['idEvento'];	
                    $nome = $qt['nome'];	
                    $local = $qt['local'];	
                    $data = $qt['dtEvento'];	
					$hora = $qt['hrEvento'];	
                    $tarefa = $qt['tarefa'].' nao concluida!';	
                }
                ?>						
                <div class="col s12 m6 l3">
                    <div class="card">
                        <div class="card-content red white-text">
                            <p class="card-stats-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ATENÇÃO</p>
                            <h4 class="card-stats-number">EVENTO</h4>
<?php if (strlen($nome) > 1) { ?>
                            <p class="card-stats-compare"><?php echo $nome.' - '.$local.' as '.date("d-m-Y", strtotime($data)).' - '.date("H:i", strtotime($hora)); ?></p>
                            <?php } ?>
                        </div>
<!--                        <div class="card-action red darken-2">
                            <div class="center-align yellow-text" style="text-decoration:blink"><?php //echo $nome; ?></div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!--card stats end-->
<?php 
		} 
?>
       <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!--work collections start-->
        <div id="work-collections">
            <div class="row">
            <?php if ($qt['idNivel'] < 3) { ?>
                <div class="col s12 m12 l6">
                <?php } else { ?>
                <div class="col s12 m12 l12">
                <?php } ?>
                    <ul id="projects-collection" class="collection">
                        <li class="collection-item avatar">
                            <i class="fa fa-tasks circle light-blue darken-2" aria-hidden="true"></i>
                            <span class="collection-header">Eventos</span>
                            <p>Em produção</p>
                        </li>
                        <?php	

                        $sql = 'SELECT distinct e.idEvento, e.*,u.nome as nomeUsuario, s.descricao FROM eventos e join usuarios u on e.idUsuario = u.idUsuario join statusEvento s on e.idStatusEvento = s.idStatusEvento join eventos_medicos em on e.idEvento = em.idEvento where (e.idStatusEvento < 70 and dtEvento > now() - INTERVAL 1 DAY) ';
						if ($_SESSION[$_idEvento] > 0) {
							$sql .= ' and e.idEvento = ' . $_SESSION[$_idEvento];	
						}
						
						if ($_SESSION[$_adm] > 1) {
							$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
						} 
												
						$sql .= ' order by dtEvento asc ';


                        //			echo $sql;
                        $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                        while ($qt = mysqli_fetch_assoc($qr)) {
                            $contador++;
                            if ($contador == 1) {
								if ($_SESSION[$_evento] < 1) {
									$idEvento = $qt['idEvento'];
									$_SESSION[$_evento] = $idEvento;
								}
							}
                            echo '<li class="collection-item"><div class="row">
                                    <div class="col s1" style="line-height: 0.2;">&nbsp;';
                                       if ($qt['idEvento'] == $_SESSION[$_evento]) echo ' <i class="fa fa-hand-o-right" aria-hidden="true"></i>';
                            echo'   </div>
                                     <div class="col s5">
                                        <p class="collections-title"><a href="javascript:evento(' . $qt['idEvento'] . ');">' . $qt['nome'] . '</a><p class="collections-content">' . $qt['nomeUsuario'] . '</p>
                                        </div>
                                        <div class="col s6">
                                            <span class="task-cat green lighten-1">' . $qt['descricao'] . '</span>
                                        </div>
                                        </div>
                                    </li>';                                
                        }
                        ?>                                    
                    </ul>
                </div>
                <?php 
				//echo 'XXXXXXXXXXXXXXX'.$_SESSION[$adm].'xxxxxxx';
//                if ($_SESSION[$adm] < 2) {
                    if ($_SESSION[$_evento] > 0) 	$idEvento = $_SESSION[$_evento];	
					else $idEvento = 0;
                    $sql = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idEvento = ' . $idEvento;
//                    echo ($sql); die();
                    $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                    $qt = mysqli_fetch_assoc($qr);
                ?>
                <div class="col s12 m12 l6">
                    <ul id="issues-collection" class="collection">
                        <li class="collection-item avatar">
                            <i class="fa fa-cog circle red darken-2" aria-hidden="true"></i>
                            <span class="collection-header">Pendências</span>
                            <p><?php echo $qt['nome'] ?></p>
                        </li>
                        <?php 
						if ($idEvento > 0) {
                            $sql = 'SELECT et.*, t.tarefa FROM eventos_tarefas et join tarefas t on et.idTarefa = t.idTarefa WHERE et.ativo = 1 and et.idEvento = ' . $idEvento;
							if ($_SESSION[$_adm] > 1) {
								$sql .= ' and t.idTarefa < 6 ';	
							}
							
							if ($_SESSION[$_idTarefa] > 0) {
								$sql .= ' and t.idTarefa = ' . $_SESSION[$_idTarefa];	
							}
							$sql .= ' order by et.idTarefa';
                            //echo $sql_usuario;
                            $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                            while ($qt = mysqli_fetch_assoc($qr)) {

                        ?>                                   
                        <li class="collection-item">
                            <div class="row">
                                <div>
                                    <p class="collections-title"><span class="col l6 s12">
                                        <a href="javascript:processa(<?php echo $qt['idTarefa']; ?>);"><?php echo $qt['tarefa']; ?></a></span>
<?php 
if ($_SESSION[$_adm] < 3) {
?>
                                        <span class="col l6 s12">
                                        <div class="progresss">
                                            <div class="determinate" style="width: <?php echo intval($qt['porcConclusao'])?>%"></div>
                                        </div><?php echo intval($qt['porcConclusao'])?>%</span>
<?php
}
?>
                                     </p>
                                </div>
                            </div>
                        </li>
                    <?php
                    //    }
						}
                    ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <aside id="chats"></aside>
        </div>
        <!--work collections end-->
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START RIGHT SIDEBAR NAV-->
<?php
include 'sidebar.php';
?>
<!-- LEFT RIGHT SIDEBAR NAV-->

</div>
<!-- END WRAPPER -->

</div>
<!-- END MAIN -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- sparkline -->
    <script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script>
    
    <!--jvectormap-->
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <script type="text/javascript">
	setTimeout(function() { window.location=window.location;},60*1000);
    </script>
    <!-- Utilizando o JS local CHAT -->
    <script type="text/javascript" src="js/functions.js"></script>
</body>
</html>