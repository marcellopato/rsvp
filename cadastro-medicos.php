<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

include ("conn/conn.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 


if (isset($_POST['btnCadastrar'])) {
	$sql = 'insert into medicos ( nome, crm, rg, cpf, dtNascimento, passaporte, validade, nacionalidade, telefone, celular, email, endereco, bairro, cidade, UF, cep, empresa, especialidade, foto) values ("'.$_POST['nome'].'","'.$_POST['crm'].'","'.$_POST['rg'].'","'.$_POST['cpf'].'","'.$_POST['dtNascimento'].'","'.$_POST['passaporte'].'","'.$_POST['validade'].'","'.$_POST['nacionalidade'].'","'.$_POST['telefone'].'","'.$_POST['celular'].'","'.$_POST['email'].'","'.$_POST['endereco'].'","'.$_POST['bairro'].'","'.$_POST['cidade'].'","'.$_POST['UF'].'","'.$_POST['cep'].'","'.$_POST['empresa'].'","'.$_POST['especialidade'].'","'.$_POST['foto'].'")';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	$ultimoMedico = mysqli_insert_id($conexao);

	if ($_POST['cboEvento'] > 0) {

		$sql = 'SELECT idUsuario FROM eventos where idEvento = ' . $_POST['cboEvento'];
		$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
		$qt2 = mysqli_fetch_assoc($qr2);
		$linhaComando = 'insert into eventos_medicos (idEvento, idMedico, idUsuarioNivel5, idUsuarioNivel4, idUsuarioNivel3, idUsuarioNivel2, idStatusConvite, indAereo, indTransfer, indHospedagem, indInscricao) values (' . $_POST['cboEvento'] . ',' . $ultimoMedico . ',0,0,0,' . $qt2['idUsuario'] .',0, 0, 0, 0, 0); ';	
		$qc = mysqli_query($conexao,$linhaComando) or die(mysqli_error());
	}

	header('location:lista-medicos.php');
}



?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php
include 'head.php';
?>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main">
<!-- START WRAPPER -->
<div class="wrapper">
	<?php
include 'navbar.php';
?>
	<!-- START CONTENT -->
	<section id="content">
	<!--start container-->
	<div class="container">
        <a href="index.php">Dashboard</a> > <a href="lista-medicos.php">Lista de Médicos</a> > Cadastro de Médicos
		<h2 class="login-form-text2">Cadastro de Médico</h2>
		<div class="row">
			<form class="col s12" method="post" action="cadastro-medicos.php">
			<div class="row">
				<div class="input-field col s12">
					<input name="nome" type="text" class="validate" required value="<?php echo $_POST['nome']; ?>">
					<label for="nome">Nome Completo</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input name="empresa" type="text" class="validate" value="<?php echo $_POST['empresa']; ?>">
					<label for="empresa">Empresa</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col l6 s12">
					<input name="endereco" type="text" class="validate active" value="<?php echo $_POST['endereco']; ?>">
					<label class="active" for="endereco">Endereço</label>
				</div>
				<div class="input-field col l6 s12">
					<input name="email" type="email" class="validate active" value="<?php echo $_POST['email']; ?>">
					<label class="active" for="email">Email</label>
				</div>
			</div>
			<div class="row">
				<div class="col l6 s12">
					<div class="dropify-wrapper">
						<div class="dropify-message"><span class="file-icon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
							<p class="center">Arraste a foto do usuário aqui. Se preferir, clique.</p>
							<p class="dropify-error center">Desculpe, a imagem é muito grande.</p>
						</div>
						<input type="file" id="input-file-now" class="dropify" data-default-file="">
						<button type="button" class="dropify-clear">Remover</button>
						<div class="dropify-preview"><span class="dropify-render"></span>
							<div class="dropify-infos">
								<div class="dropify-infos-inner">
									<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>
									<p class="dropify-infos-message">Arraste ou clique em "Trocar imagem"</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="input-field col  l6 s12">
					<input name="crm" type="text" class="validate" value="<?php echo $_POST['crm']; ?>">
					<label class="active" for="crm">CRM</label>
				</div>
				<div class="input-field col l6 s12">
					<input name="rg" type="text" class="validate" value="<?php echo $_POST['rg']; ?>">
					<label class="active" for="rg">RG <small><i>somente números</i></small></label>
				</div>
				<div class="input-field col l6 s12">
					<input name="cpf" type="text" class="validate" required value="<?php echo $_POST['cpf']; ?>">
					<label class="cpf" for="cpf">CPF <small><i>somente números</i></small></label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col  l6 s12">
					<input name="passaporte" type="text" class="validate" value="<?php echo $_POST['passaporte']; ?>">
					<label class="active" for="passaporte">Passaporte</label>
				</div>

			</div>
			<div class="row">
				<div class="input-field col  l6 s12">
					<input name="telefone" type="text" class="validate" value="<?php echo $_POST['telefone']; ?>">
					<label class="active" for="telefone">Telefone <small><i>separe por vírgulas</i></small></label>
				</div>
				<div class="input-field col l6 s12">
					<input name="celular" type="text" class="validate" value="<?php echo $_POST['celular']; ?>">
					<label class="active" for="celular">Celular <small><i>separe por vírgulas</i></small></label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col l6 s12">
					<input name="bairro" type="text" class="validate" value="<?php echo $_POST['bairro']; ?>">
					<label class="active" for="bairro">Bairro </label>
				</div>
				<div class="input-field col l6 s12">
					<input name="cep" type="text" class="validate" value="<?php echo $_POST['cep']; ?>">
					<label class="active" for="cep">CEP <small><i>somente números</i></small></label>
				</div>
			</div>
            <div>
            <select id="cboEvento" name="cboEvento" class="browser-default">
				<option value="0" selected>Nenhum</option>
<?php 
$sql = 'SELECT * FROM eventos where dtEvento > ' . date('Y-m-d') . ' order by idEvento';
$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
while ($qt2 = mysqli_fetch_assoc($qr2)) {
echo '<option value="' . $qt2['idEvento'] . '"';
//if ($qt2['idEvento'] == $qt['idEvento']) echo ' selected';
echo '>' . $qt2['nome'] . '</option>'; 	
}
?>
            </select>
            </div>


			<div class="row">
				<button class="btn waves-effect waves-light pink" type="submit" id="btnCadastrar" name="btnCadastrar" value="Alterar">Cadastrar <i class="material-icons right">send</i> </button>
			</div>
			<!--end container-->
			</section>
			<!-- END CONTENT --> 
			<!-- //////////////////////////////////////////////////////////////////////////// --> 
			<!-- START RIGHT SIDEBAR NAV-->
			<aside id="right-sidebar-nav">
			<ul id="chat-out" class="side-nav rightside-navigation">
			<li class="li-hover"> <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
				<div id="right-search" class="row">
			<form class="col s12">
				<div class="input-field"> <i class="mdi-action-search prefix"></i>
					<input id="icon_prefix" type="text" class="validate">
					<label for="icon_prefix">Search</label>
				</div>
			</form>
		</div>
		</li>

		</ul>
		</aside>
		<!-- LEFT RIGHT SIDEBAR NAV--> 
	</div>
	<!-- END WRAPPER --> 
</div>
<!-- END MAIN --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START FOOTER -->
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
	</div>
</footer>
<!-- END FOOTER --> 
<!-- ================================================
    Scripts
    ================================================ --> 
<!-- jQuery Library --> 
<script type="text/javascript" src="../js/plugins/jquery-1.11.2.min.js"></script> 
<!--materialize js--> 
<script type="text/javascript" src="../js/materialize.min.js"></script> 
<!-- upload --> 
<script type="text/javascript" src="../js/dropify.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="../js/plugins.min.js"></script> 
<!-- Toast Notification --> 
<script>
  $(document).ready(function() {
    Materialize.updateTextFields();
  });
  
function novo() {
	getElementById('btnCadastrar').value = "Cadastrar";
}

</script>
<?php if (isset($_GET['cadastro']) && ($_GET['cadastro'] == '1')) { ?>
	<script>
		var $toastContent = $('<span>Cadastro atualizado com SUCESSO!</span>');
		Materialize.toast($toastContent, 5000);
</script>
	<?php } ?>
</body>
</html>
