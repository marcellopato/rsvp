<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

include ("conn/conn.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
include 'head.php';
?>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main"> 
	<!-- START WRAPPER -->
	<div class="wrapper"> 
<?php
include 'navbar.php';
?>		
		
		<!-- //////////////////////////////////////////////////////////////////////////// --> 
		
		<!-- START CONTENT -->
		<section id="content"> 
			
			<!--start container-->
			<div class="container"> 
				
				<!--chart dashboard start--> 
				<!--chart dashboard end--> 
				
				<!-- //////////////////////////////////////////////////////////////////////////// --> 
				
				<!--card stats start-->
				<div id="card-stats">
					<div class="row" id="topo">
<?php
//loop 4 eventos fechados
$sql_usuario = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idStatusEvento > 69 order by dtEvento desc limit 4';
//echo $sql_usuario;
$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
while ($qt = mysqli_fetch_assoc($qr)) {
?>
						<div class="col s12 m6 l3">
							<div class="card">
								<div class="card-content white black-text">
									<p class="card-stats-title"><i class="fa fa-file" aria-hidden="true"></i><?php echo $qt['nome']; ?></p>
									<h4 class="card-stats-number"><?php echo date_format(date_create($qt['dtConclusao']), 'd-m-Y'); ?></h4>
									<p class="card-stats-compare"><span class="grey-text"><?php echo $qt['descricao']; ?></span> </p>
								</div>
								<div class="card-action yellow light">
									<div class="center-align black-text">Detalhes do evento</div>
								</div>
							</div>
						</div>
<?php
}
?>
					</div>
				</div>
				
				<!--card stats end--> 
				

				
				<!-- //////////////////////////////////////////////////////////////////////////// --> 
				
				<!--work collections start-->
				<div id="work-collections">
					<div class="row">
						<div class="col s12 m12 l12">
							<ul id="projects-collection" class="collection">
								<li class="collection-item avatar"> <i class="fa fa-folder circle light-blue darken-2" aria-hidden="true"></i> <span class="collection-header">Eventos</span>
									<p>Realizados</p>
									<!-- <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>--> 
								</li>
<?php
//loop 4 eventos fechados
$sql_usuario = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idStatusEvento < 70 order by dtEvento desc limit 4';
//echo $sql_usuario;
$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
while ($qt = mysqli_fetch_assoc($qr)) {

//	$_SESSION[$codigo] = $qt['idUsuario'];

?>

								<li class="collection-item">
									<div class="row">
										<div class="col s6">
											<p class="collections-title"><?php echo $qt['nome']; ?></p>
											<p class="collections-content"><?php echo $qt['lab']; ?></p>
										</div>
										<div class="col s6"> <span class="task-cat green lighten-1"><?php echo $qt['status']; ?></span> </div>
									</div>
								</li>
<?php
}
?>								
								
							</ul>
						</div>
					</div>
				</div>
				
			</div>
			<!--end container--> 
		</section>
		<!-- END CONTENT --> 
		
		<!-- //////////////////////////////////////////////////////////////////////////// --> 
		<!-- START RIGHT SIDEBAR NAV-->
		<aside id="right-sidebar-nav">
			<ul id="chat-out" class="side-nav rightside-navigation">
				<li class="li-hover"> <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
					<div id="right-search" class="row">
						<form class="col s12">
							<div class="input-field"> <i class="mdi-action-search prefix"></i>
								<input id="icon_prefix" type="text" class="validate">
								<label for="icon_prefix">Search</label>
							</div>
						</form>
					</div>
				</li>
				<li class="li-hover">
					<ul class="chat-collapsible" data-collapsible="expandable">
						<li>
							<div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
							<div class="collapsible-body recent-activity">
								<div class="recent-activity-list chat-out-list row">
									<div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i> </div>
									<div class="col s9 recent-activity-list-text"> <a href="#">just now</a>
										<p>Jim Doe Purchased new equipments for zonal office.</p>
									</div>
								</div>
								<div class="recent-activity-list chat-out-list row">
									<div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i> </div>
									<div class="col s9 recent-activity-list-text"> <a href="#">Yesterday</a>
										<p>Your Next flight for USA will be on 15th August 2015.</p>
									</div>
								</div>
								<div class="recent-activity-list chat-out-list row">
									<div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> </div>
									<div class="col s9 recent-activity-list-text"> <a href="#">5 Days Ago</a>
										<p>Natalya Parker Send you a voice mail for next conference.</p>
									</div>
								</div>
								<div class="recent-activity-list chat-out-list row">
									<div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i> </div>
									<div class="col s9 recent-activity-list-text"> <a href="#">Last Week</a>
										<p>Jessy Jay open a new store at S.G Road.</p>
									</div>
								</div>
								<div class="recent-activity-list chat-out-list row">
									<div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i> </div>
									<div class="col s9 recent-activity-list-text"> <a href="#">5 Days Ago</a>
										<p>Natalya Parker Send you a voice mail for next conference.</p>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
							<div class="collapsible-body sales-repoart">
								<div class="sales-repoart-list  chat-out-list row">
									<div class="col s8">Target Salse</div>
									<div class="col s4"><span id="sales-line-1"></span> </div>
								</div>
								<div class="sales-repoart-list chat-out-list row">
									<div class="col s8">Payment Due</div>
									<div class="col s4"><span id="sales-bar-1"></span> </div>
								</div>
								<div class="sales-repoart-list chat-out-list row">
									<div class="col s8">Total Delivery</div>
									<div class="col s4"><span id="sales-line-2"></span> </div>
								</div>
								<div class="sales-repoart-list chat-out-list row">
									<div class="col s8">Total Progress</div>
									<div class="col s4"><span id="sales-bar-2"></span> </div>
								</div>
							</div>
						</li>
						<li>
							<div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
							<div class="collapsible-body favorite-associates">
								<div class="favorite-associate-list chat-out-list row">
									<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
									<div class="col s8">
										<p>Eileen Sideways</p>
										<p class="place">Los Angeles, CA</p>
									</div>
								</div>
								<div class="favorite-associate-list chat-out-list row">
									<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
									<div class="col s8">
										<p>Zaham Sindil</p>
										<p class="place">San Francisco, CA</p>
									</div>
								</div>
								<div class="favorite-associate-list chat-out-list row">
									<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> </div>
									<div class="col s8">
										<p>Renov Leongal</p>
										<p class="place">Cebu City, Philippines</p>
									</div>
								</div>
								<div class="favorite-associate-list chat-out-list row">
									<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image"> </div>
									<div class="col s8">
										<p>Weno Carasbong</p>
										<p>Tokyo, Japan</p>
									</div>
								</div>
								<div class="favorite-associate-list chat-out-list row">
									<div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image"> </div>
									<div class="col s8">
										<p>Nusja Nawancali</p>
										<p class="place">Bangkok, Thailand</p>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</aside>
		<!-- LEFT RIGHT SIDEBAR NAV--> 
		
	</div>
	<!-- END WRAPPER --> 
	
</div>
<!-- END MAIN --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START FOOTER -->
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
	</div>
</footer>
<!-- END FOOTER --> 

<!-- ================================================
    Scripts
    ================================================ --> 

<!-- jQuery Library --> 
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script> 
<!--materialize js--> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 

<!-- chartist --> 
<script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script> 

<!-- chartjs --> 
<script type="text/javascript" src="js/plugins/chartjs/chart.min.js"></script> 
<script type="text/javascript" src="js/plugins/chartjs/chart-script.js"></script> 

<!-- sparkline --> 
<script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script> 
<script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script> 

<!-- google map api --> 
<!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAZnaZBXLqNBRXjd-82km_NO7GUItyKek"></script>
--> 
<!--jvectormap--> 
<script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> 
<script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 
<script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script> 

<!--google map--> 
<!--    <script type="text/javascript" src="js/plugins/google-map/google-map-script.js"></script>
--> 

<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="js/plugins.min.js"></script> 
<!--custom-script.js - Add your own theme custom JS--> 
<!--    <script type="text/javascript" src="js/custom-script.js"></script>
--> <!-- Toast Notification --> 
<script type="text/javascript">
    // Toast Notification
    $(window).load(function() {
        setTimeout(function() {
            Materialize.toast('<span>RSVP confirmado!</span>', 3000);
        }, 1500);
        setTimeout(function() {
            Materialize.toast('<span>RSVP confirmado!</span>', 5000);
        }, 5000);
        setTimeout(function() {
            Materialize.toast('<span>Evento OK. Emitir voucher?</span><a class="btn-flat yellow-text" href="#">Sim<a>', 7000);
        }, 15000);
    });
    </script>
</body>
</html>