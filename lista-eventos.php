<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

include ("conn/conn.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

if (isset($_POST['btnNovo'])) {
	header('location:cadastro-eventos.php');
}

if (isset($_GET['play']) && $_GET['play'] > 0) {
	$sql = 'update eventos set idStatusEvento = 20 where idEvento = ' . $_GET['play'];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
}

if (isset($_GET['pause']) && $_GET['pause'] > 0) {
	$sql = 'update eventos set idStatusEvento = 50 where idEvento = ' . $_GET['pause'];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<?php
include 'head.php';
?>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main">
<!-- START WRAPPER -->
<div class="wrapper">
<?php
include 'navbar.php';
?>		

<!-- START CONTENT -->
<section id="content"> 
  <!--start container-->
  <div class="container">
   <a href="index.php">Dashboard</a> > Lista de eventos
   <form id="form1" name="form1" class="col s12" method="post">
   <h2 class="login-form-text2">Cadastrar novos eventos</h2>
    <button class="btn waves-effect waves-light" type="submit" name="btnNovo">cadastrar <i class="material-icons right">send</i> </button>
    <br><br>
    <div class="divider"></div>
    <br>
    <h2 class="login-form-text2">Lista de eventos</h2>
    <div class="row">
    
      <div class="row">
        <div class="input-field col s6">
		<select id="cboUsuario" name="cboUsuario" onChange="form1.submit(); " class="browser-default">
		<?php	
            echo '<option value="0"';
            if ($_POST['cboUsuario'] < 1) echo ' selected';
            echo '>Todos</option>';

            $sql = 'SELECT * FROM usuarios WHERE idNivel = 2 order by nome';
            echo $sql;
            $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
            while ($qt = mysqli_fetch_assoc($qr)) {
                echo '<option value="' . $qt['idUsuario'] . '"';
                    if ($_POST['cboUsuario'] == $qt['idUsuario']) echo ' selected';
                echo '>' . $qt['nome'] . '</option>'; 	
            }
 ?>
        </select>
       <label></label>
        </div>
        <div class="input-field col l6 s12">
            <input id="dataInicialEvento" name="dataInicialEvento" type="text" class="validate active" value="<?php echo ($_POST['dataInicialEvento'] != '' ? date("d-m-Y",strtotime($_POST['dataInicialEvento'])) : date('d-m-Y')   ); ?>" onChange="form1.submit(); ">
            <label for="dataEvento">Data - Você pode ver eventos anteriores usando o calendário para voltar na linha de tempo.</label>
        </div>
      </div>
<?php	
$sql = 'SELECT e.idEvento, e.nome, e.dtEvento, e.local, e.foto, se.descricao, (select count(*) from eventos_medicos where idEvento = e.idEvento group by idEvento) as contador FROM eventos e join statusEvento se on e.idStatusEvento = se.idStatusEvento where 1=1 ';
if (($_POST['cboUsuario']) > 0) {
	$sql .= ' and idUsuario = ' . $_POST['cboUsuario'];
}
$dataInicio = date('Y-m-d');
if ($_POST['dataInicialEvento'] != '') $dataInicio = date("Y-m-d",strtotime($_POST['dataInicialEvento']));
$sql .= ' and e.dtEvento >= "' . $dataInicio . '" group by e.idEvento, e.nome, e.dtEvento, se.descricao order by dtEvento asc ';

//echo $sql;
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
while ($qt = mysqli_fetch_assoc($qr)) {
	echo '<div class="row">
			<div class="col col s3 m3 l3" style="background-image: url(' . $qt['foto'] . ');background-size:cover;height:25px;" class="circle"></div>
			<div class="col col s9 m9 l9">
				<div class="row">Nome:<a href="altera-eventos.php?idEvento=' . $qt['idEvento'] . '">' . $qt['nome'] . '</a></div>
				<div class="row">Local:' . $qt['local'] . '</div>
				<div class="row">Data:' . date("d-m-Y", strtotime($qt['dtEvento'])) . ' - ' . date("d-m-Y", strtotime($qt['dtFinal'])) . '   Qtde:' . $qt['contador'] . '   Status:' . $qt['descricao'] . '</div>
				<div><a href="lista-eventos.php?play=' . $qt['idEvento'] . '"><i class="fa fa-play green-text" aria-hidden="true"></i></a>&nbsp;&nbsp;<a href="lista-eventos.php?pause=' . $qt['idEvento'] . '"><i class="fa fa-pause yellow-text" aria-hidden="true"></i></a></div>
			</div>
		</div><br>';
            }
         ?>
      <br>
      <div class="divider"></div>
      <br>
      <div class="row">
        <button class="btn waves-effect waves-light" type="submit" name="btnNovo">cadastrar <i class="material-icons right">send</i> </button>
      </div>
    </form>
  </div>
  </div>
  <!--end container--> 
</section>
<!-- END CONTENT --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START RIGHT SIDEBAR NAV-->

<!-- LEFT RIGHT SIDEBAR NAV-->

</div>
<!-- END WRAPPER -->

</div>
<!-- END MAIN --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START FOOTER -->
<footer class="page-footer">
  <div class="footer-copyright">
    <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
  </div>
</footer>
<!-- END FOOTER --> 

<!-- ================================================
    Scripts
    ================================================ --> 

<!-- jQuery Library --> 
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
<!--materialize js--> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
<!--sortable-->
<script type="text/javascript" src="js/jquery.sortable.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="js/plugins.min.js"></script> 
<!--nestable --> 
<script type="text/javascript" src="js/plugins/jquery.nestable/jquery.nestable.js"></script> 
<script type="text/javascript" src="js/jquery.mask.min.js"></script>
<script>
  $(document).ready(function() {
                Materialize.updateTextFields();
                    jQuery(function($){
                        $('#dataInicialEvento').mask('00-00-0000');
                    });
  });
</script> 
<script>
    $('.sortable').sortable();
</script>
</ul></body>
</html>