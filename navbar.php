<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav dir fixed leftside-navigation">
        <li class="user-details yellow darken-2">
            <div class="row">
            <div class="col col s4 m4 l4"> <img src="<?php echo $_SESSION[$_foto] ?>" alt="" class="circle responsive-img valign profile-image"> </div>
                <div class="col col s8 m8 l8">
                <span class="white-text profile-btn truncate"><?php echo $_SESSION[$_nome] ?></span>
                <p class="user-roal"><?php echo $_SESSION[$_role] ?></p>
                </div>
            </div>
        </li>
        <li class="bold"><a href="index.php" class="waves-effect waves-cyan"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a> </li>
<?php
if ($_SESSION[$_adm] == 1 and $_SESSION[$_idTarefa] == 0) {
?>
<!--        <li class="bold"><a href="app-webmail.php" class="waves-effect waves-cyan disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Email </a> </li> -->
        <li class="bold"><a href="calendario.php" class="waves-effect waves-cyan"><i class="fa fa-calendar" aria-hidden="true"></i> Calendário</a> </li>
        <li class="bold"><a href="lista-usuarios.php" class="waves-effect waves-cyan"><i class="fa fa-users" aria-hidden="true"></i>Usuários</a> </li>
        <li class="bold"><a href="lista-laboratorios.php" class="waves-effect waves-cyan"><i class="fa fa-flask" aria-hidden="true"></i>Clientes</a> </li>
        <li class="bold"><a href="lista-laboratorios-tree.php" class="waves-effect waves-cyan"><i class="fa fa-user" aria-hidden="true"></i>Funcionarios</a> </li>
        <li class="bold"><a href="lista-eventos.php" class="waves-effect waves-cyan"><i class="fa fa-tasks" aria-hidden="true"></i>Eventos</a> </li>
        <li class="bold"><a href="lista-medicos.php" class="waves-effect waves-cyan"><i class="fa fa-user-md" aria-hidden="true"></i>Médicos</a> </li>
<?php
}
?>
    </ul>
     <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="fa fa-bars white-text" aria-hidden="true"></i></a>
</aside>
<!-- END LEFT SIDEBAR NAV--> 
