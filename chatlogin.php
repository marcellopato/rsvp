<?php
    session_start();
    include_once "conn/conn.php";
    include_once "defines.php";
    require_once('classes/BD.class.php');
    BD::conn();
?>
   <html>
    <body>
        <div class="formulario">
            <h1>Faca login</h1>
            <?php
                if(isset($_POST['acao']) && $_POST['acao'] == 'logar'){
                    $email  = strip_tags(trim(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING)));
                    if($email == ''){
                        echo 'informe o email';
                    }else{
                        $pegaUser   = BD::conn()->prepare("SELECT * FROM 'usuarios' WHERE 'email' = ?");
                        $pegaUser->execute(array($email));
                        
                        if($pegaUser->rowCount() == 0){
                            echo 'Não existe este login';
                        }else{
                            while($row = $pegaUser->fetchObject()){
                                $_SESSION['email_logado'] = $email;
                                $_SESSION['id_user'] = $row->id;
                                header("Location: app-chat.php");
                            }
                        }
                    }
                }
            ?>
            <form action="" method="post" enctype="multipart/form-data">
                <label>
                    <span>insira email</span>
                    <input type="text" name="email" placeholder="use seu email" />
                </label>
                <input type="hidden" name="acao" value="logar" />
                <input type="submit" value="Entrar" class="botao right" />
            </form>
        </div>
    </body>
</html>