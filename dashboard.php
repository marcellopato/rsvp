<?php
session_start();
include ("conn/conn.php");
mysql_query("SET NAMES 'utf8'"); 
//echo $_SESSION[$adm];
$usuario = $_SESSION[$codigo];
if (!isset($_SESSION[$adm])) {
	header("location:Erro-de-Login");
	exit();
}
if ($_SESSION[$adm] == '') {
	header("location:Erro-de-Login");
	exit();
}
if ($_SESSION[$adm] < 1) {
	header("location:Erro-de-Login");
	exit();
}
if ($_SESSION[$adm] > 4) {
	header("location:Detalhe-dos-Eventos");
	exit();
}

//echo $usuario;
	$sql_usuario = 'SELECT us.*,ni.txtNivel FROM usuarios us JOIN nivels ni on ni.idNivel = us.idNivel WHERE ((us.idUsuario = "' . $usuario . '"))';
	//echo $sql_usuario;
	$qr = mysql_query($sql_usuario) or die(mysql_error());
	$qt = mysql_fetch_assoc($qr);
	
	
	
	
	

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>RSVP - Ganbatte</title>

    <!-- Compiled and minified CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.min.css">
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/style.min.css">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
	<!-- Opera Speed Dial Favicon -->
	<link rel="icon" type="image/png" href1="favicon.png" />

	<!-- Standard Favicon -->
	<link rel="icon" type="image/x-icon" href="favicon.ico" />

	<!-- For iPhone 4 Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="favicon.png">

	<!-- For iPad: -->
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="favicon.png">

	<!-- For iPhone: -->
	<link rel="apple-touch-icon-precomposed" href="favicon.png">

</head>

<body>
     <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="index.html" class="brand-logo darken-1"><img src="assets/img/logo-horizontal.png" alt="ganbatte"></a> <span class="logo-text">ganbatte</span></h1></li>
                    </ul>
                       <ul class="right hide-on-med-and-down black-text">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen black-text"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                        </li>
						<?php if ($qt['idNivel'] < 3) { ?>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button black-text" data-activates="notifications-dropdown"><i class="fa fa-bell-o" aria-hidden="true"><small class="notification-badge">5</small></i>
                        </a>
                        </li>
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse black-text"><i class="fa fa-comments-o" aria-hidden="true"></i></a>
                        </li><?php } ?>
                    </ul>
                    <!-- notifications-dropdown -->
					<?php if ($qt['idNivel'] < 3) { ?>
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>AVISOS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="fa fa-check" aria-hidden="true"></i> CHECK-IN completo</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 horas atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-certificate" aria-hidden="true"></i> Evento OK.</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 horas atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-cog" aria-hidden="true"></i> Sistema atualizado</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 dias atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Reunião de Pauta</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 horas atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-line-chart" aria-hidden="true"></i> Relatório gerado</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 semana atrás</time>
                      </li>
                    </ul>
					<?php } ?>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details yellow darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="assets/img/<?php echo $qt['foto'];?>" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="Perfil"><i class="fa fa-user" aria-hidden="true"></i> Perfil</a>
                            </li>
                            <li><a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> Confs</a>
                            </li>
                            <li><a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i> Ajuda</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="Login"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                            </li>
                        </ul>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?php echo $qt['nome'] ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal"><?php echo $qt['txtNivel'];?></p>
                    </div>
                </div>
                </li>
                <li class="bold active"><a href="#!" class="waves-effect waves-cyan"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a>
                </li>
                <?php if ($qt['idNivel'] < 3) { ?>
                <li class="bold"><a href="app-email.html" class="waves-effect waves-cyan disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Email <span class="new badge">4</span></a>
                </li>
                <li class="bold"><a href="app-calendar.html" class="waves-effect waves-cyan"><i class="fa fa-calendar" aria-hidden="true"></i> Calendário</a>
                </li>
                <?php } ?>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="fa fa-archive" aria-hidden="true"></i> Cadastro Geral</a>
                            <div class="collapsible-body">
                                <ul>
                                    <?php if ($qt['idNivel'] < 3) { ?>
                                    <li><a href="Cadastro-de-Eventos" class="bold active">Eventos</a>
                                    </li>
                                    <?php } ?>
                                    <li><a href="Cadastro-de-Usuarios">Usuários</a>
                                    </li>
                                    <li><a href="Cadastro-de-Medicos">Médicos</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="li-hover"><div class="divider"></div></li>
            </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <?php if ($qt['idNivel'] < 3) { ?>
					<!--card stats start-->
                    <div id="card-stats">
                        <div class="row" id="topo">
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content  green white-text">
                                        <p class="card-stats-title"><i class="fa fa-users  " aria-hidden="true"></i> 22º Encontro em NY</p>
                                        <h4 class="card-stats-number">100% OK</h4>
                                        <p class="card-stats-compare"><i class="fa fa-check" aria-hidden="true"></i><span class="green-text text-lighten-5">Todas etapas conluídas</span>
                                        </p>
                                    </div>
                                    <div class="card-action  green darken-2">
                                        <div class="center-align white-text">Parabenizar a equipe!</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content blue lighten-1 white-text">
                                        <p class="card-stats-title"><i class="fa fa-file" aria-hidden="true"></i> Novos Eventos</p>
                                        <h4 class="card-stats-number">4</h4>
                                        <p class="card-stats-compare"><i class="fa fa-hand-o-right" aria-hidden="true"></i> 83% <span class="deep-purple-text text-lighten-5">da equipe ciente</span>
                                        </p>
                                    </div>
                                    <div class="card-action  blue darken-2">
                                        <div class="center-align white-text">Acionar a equipe</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content orange white-text">
                                        <p class="card-stats-title"><i class="fa fa-exclamation" aria-hidden="true"></i> Evento inicia HOJE</p>
                                        <h4 class="card-stats-number">19h de L.A.</h4>
                                        <p class="card-stats-compare"><i class="fa fa-check-circle" aria-hidden="true"></i> Tudo pronto e checado</p>
                                    </div>
                                    <div class="card-action orange darken-2">
                                        <div class="center-align white-text">Avisar equipe e Cliente!</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content red white-text">
                                        <p class="card-stats-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ATENÇÃO</p>
                                        <h4 class="card-stats-number">EVENTO</h4>
                                        <p class="card-stats-compare">de oncologistas de HOJE às 19h</p>
                                    </div>
                                    <div class="card-action red darken-2">
                                        <div class="center-align yellow-text" style="text-decoration:blink">Palestrante não fez check-in!</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--card stats end-->
					<?php } ?>
                   <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <!--work collections start-->
                    <div id="work-collections">
                        <div class="row">
                        <?php if ($qt['idNivel'] < 3) { ?>
                            <div class="col s12 m12 l6">
                            <?php } else { ?>
                            <div class="col s12 m12 l12">
                            <?php } ?>
                                <ul id="projects-collection" class="collection">
                                    <li class="collection-item avatar">
                                        <i class="fa fa-folder circle light-blue darken-2" aria-hidden="true"></i>
                                        <span class="collection-header"><a href="app-eventos.html" class="black-text">Eventos</a></span>
                                        <p>Em produção</p>
                                       <!-- <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>-->
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Convenção de Cardiologia</p>
                                                <p class="collections-content">TEVA</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat green lighten-1">Completo</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">2º Encontro de Ginecologistas</p>
                                                <p class="collections-content">ABG</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat blue lighten-1">90%</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Lançamento PROVASIC</p>
                                                <p class="collections-content">Abott</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat orange">30%</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Palestra de Neurologia</p>
                                                <p class="collections-content">BAYER</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat red yellow-text">10%</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <?php if ($qt['idNivel'] < 3) { ?>
                            <div class="col s12 m12 l6">
                                <ul id="issues-collection" class="collection">
                                    <li class="collection-item avatar">
                                        <i class="fa fa-bug  circle red darken-2" aria-hidden="true"></i>
                                        <span class="collection-header">Pendências</span>
                                        <p>Palestra de Neurologia</p>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s7">
                                                <p class="collections-title"><strong>#102</strong> Bilhetes ainda não emitidos</p>
                                                <p class="collections-content"><i class="fa fa-envelope" aria-hidden="true"></i> - <i class="fa fa-phone-square" aria-hidden="true"></i></p>
                                            </div>
                                            <div class="col s2">
                                                <span class="task-cat pink accent-2">PAX</span>
                                            </div>
                                            <div class="col s3">
                                                <div class="progress">
                                                     <div class="determinate" style="width: 70%;background-color: #FF5722;"></div>   
                                                </div>                                                
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s7">
                                                <p class="collections-title"><strong>#108</strong> Enviar lista para traslado</p>
                                                <p class="collections-content"><i class="fa fa-envelope" aria-hidden="true"></i> - <i class="fa fa-phone-square" aria-hidden="true"></i></p>
                                            </div>
                                            <div class="col s2">
                                                <span class="task-cat yellow darken-4">TRANS</span>
                                            </div>
                                            <div class="col s3">
                                                <div class="progress">
                                                    <div class="determinate" style="width: 40%;background-color: #64B5F6;"></div>   
                                                </div>                                                
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s7">
                                                <p class="collections-title"><strong>#205</strong> Emitir vouchers</p>
                                                <p class="collections-content"><i class="fa fa-envelope" aria-hidden="true"></i> - <i class="fa fa-phone-square" aria-hidden="true"></i></p>
                                            </div>
                                            <div class="col s2">                                                
                                                <span class="task-cat light-green darken-3">VOU</span>
                                            </div>
                                            <div class="col s3">
                                                <div class="progress">
                                                    <div class="determinate" style="width: 95%;background-color: #1976D2;"></div>   
                                                </div>                                                
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s7">
                                                <p class="collections-title"><strong>#188</strong> Palestra de Neurologia</p>
                                                <p class="collections-content"><i class="fa fa-envelope" aria-hidden="true"></i> - <i class="fa fa-phone-square" aria-hidden="true"></i></p>
                                            </div>
                                            <div class="col s2">
                                                <span class="task-cat pink accent-2">RSVP</span>
                                            </div>
                                            <div class="col s3">
                                                <div class="progress">
                                                     <div class="determinate" style="width: 10%;background-color: #f44336;"></div>   
                                                </div>                                                
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!--work collections end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
            <aside id="right-sidebar-nav">
                <ul id="chat-out" class="side-nav rightside-navigation">
                    <li class="li-hover">
                    <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                    <div id="right-search" class="row">
                        <form class="col s12">
                            <div class="input-field">
                                <i class="mdi-action-search prefix"></i>
                                <input id="icon_prefix" type="text" class="validate">
                                <label for="icon_prefix">Search</label>
                            </div>
                        </form>
                    </div>
                    </li>
                    <li class="li-hover">
                        <ul class="chat-collapsible" data-collapsible="expandable">
                        <li>
                            <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                            <div class="collapsible-body recent-activity">
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">just now</a>
                                        <p>Jim Doe Purchased new equipments for zonal office.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Yesterday</a>
                                        <p>Your Next flight for USA will be on 15th August 2015.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Last Week</a>
                                        <p>Jessy Jay open a new store at S.G Road.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                            <div class="collapsible-body sales-repoart">
                                <div class="sales-repoart-list  chat-out-list row">
                                    <div class="col s8">Target Salse</div>
                                    <div class="col s4"><span id="sales-line-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Payment Due</div>
                                    <div class="col s4"><span id="sales-bar-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Delivery</div>
                                    <div class="col s4"><span id="sales-line-2"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Progress</div>
                                    <div class="col s4"><span id="sales-bar-2"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                            <div class="collapsible-body favorite-associates">
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Eileen Sideways</p>
                                        <p class="place">Los Angeles, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Zaham Sindil</p>
                                        <p class="place">San Francisco, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Renov Leongal</p>
                                        <p class="place">Cebu City, Philippines</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Weno Carasbong</p>
                                        <p>Tokyo, Japan</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Nusja Nawancali</p>
                                        <p class="place">Bangkok, Thailand</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        </ul>
                    </li>
                </ul>
            </aside>
            <!-- LEFT RIGHT SIDEBAR NAV-->

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer class="page-footer">
        <?php if ($qt['idNivel'] < 3) { ?>
        <div class="container">
            <div class="row section">
                <div class="col l12 s12">
                    <h5 class="white-text">Aonde estamos!</h5>
                    <p class="grey-text text-lighten-4">Eventos sendo realizados agora</p>
                    <div id="world-map-markers"></div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    

    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>   

    <!-- chartjs -->
    <script type="text/javascript" src="js/plugins/chartjs/chart.min.js"></script>
    <script type="text/javascript" src="js/plugins/chartjs/chart-script.js"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script>
    
    <!-- google map api -->
<!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAZnaZBXLqNBRXjd-82km_NO7GUItyKek"></script>
-->
    <!--jvectormap-->
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script>
    
    <!--google map-->
<!--    <script type="text/javascript" src="js/plugins/google-map/google-map-script.js"></script>
-->
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
<!--    <script type="text/javascript" src="js/custom-script.js"></script>
-->    <!-- Toast Notification -->
    <script type="text/javascript">
    // Toast Notification
    $(window).load(function() {
        setTimeout(function() {
            Materialize.toast('<span>RSVP confirmado!</span>', 3000);
        }, 1500);
        setTimeout(function() {
            Materialize.toast('<span>RSVP confirmado!</span>', 5000);
        }, 5000);
        setTimeout(function() {
            Materialize.toast('<span>Evento OK. Emitir voucher?</span><a class="btn-flat yellow-text" href="#">Sim<a>', 7000);
        }, 15000);
    });
    </script>
</body>
</html>