<IfModule mod_rewrite.c>

RewriteEngine On

RewriteRule ^Login?$                            	                                                    index.php [NC]
RewriteRule ^Dashboard?$                                                                              	dashboard.php [NC]
RewriteRule ^Erro-de-Login?$													                        index.php?erro=$1 [NC]
RewriteRule ^Perfil?$                                                                     				edita_perfil.php?idUsuario=$1 [NC]
RewriteRule ^Detalhe-dos-Eventos?$                                                    				    estrutura-organizacional.php [NC]
RewriteRule ^Cadastro-de-Eventos?$   	                                                                admin/cadastro-evento.php [NC]
RewriteRule ^Cadastro-de-Usuarios?$   			                                                        admin/cadastro-usuario.php [NC]
RewriteRule ^Cadastro-de-Medicos?$                                                                 		admin/cadastro-medicos.php [NC]
RewriteRule ^ARSEP/([0-9]+)/([a-z0-9-]+)?$                                                              arsep.php?id=$1 [NC]
RewriteRule ^Legislacao?$                                                                               legislacao.php [NC]
RewriteRule ^Legislacao/Resolucoes?$                                                                    resolucoes.php [NC]
RewriteRule ^Legislacao/([0-9]+)/([a-z0-9-]+)?$                                                         legislacao.php?id=$1 [NC]
RewriteRule ^Legislacao/([0-9]+)/([a-z0-9-]+)/([0-9]+)/([a-z0-9-]+)?$                                   legislacao.php?id=$1&idSub=$3 [NC]
RewriteRule ^Fiscalizacao?$                                                                             fiscalizacao.php [NC]
RewriteRule ^Fiscalizacao/([0-9]+)/([a-z0-9-]+)?$                                                       fiscalizacao.php?id=$1&o=1 [NC]
RewriteRule ^Fiscalizacao/([0-9]+)/([a-z0-9-]+)/Obras-Executadas?$                                      fiscalizacao.php?id=$1&o=1 [NC]
RewriteRule ^Fiscalizacao/([0-9]+)/([a-z0-9-]+)/Obras-em-Execucao?$                                     fiscalizacao.php?id=$1&o=0 [NC]
RewriteRule ^Duvidas-Frequentes?$                                                                       duvidas-frequentes.php [NC]
RewriteRule ^Duvidas-Frequentes/([0-9]+)/([a-z0-9-]+)?$                                                 duvidas-frequentes.php?id=$1 [NC]
RewriteRule ^Links-Uteis?$                                                                              links-uteis.php [NC]
RewriteRule ^Contato?$                                                                                  contato.php [NC]
RewriteRule ^Resultados-busca?$                                                                         resultado-busca.php [NC]
RewriteRule ^Mapa-do-site?$                                                                             mapa-site.php [NC]

ErrorDocument 404 																	                    http://ganbatte.ahcme.com.br/error404
RewriteRule ^error404?$ 																                error.php [NC]

</IFModule>

#############
# TYPES FIX #
#############
AddType text/css .css
AddType text/javascript .js

####################
# GZIP COMPRESSION #
####################
SetOutputFilter DEFLATE
AddOutputFilterByType DEFLATE text/html text/css text/plain text/xml text/javascript application/x-javascript application/x-httpd-php
BrowserMatch ^Mozilla/4 gzip-only-text/html
BrowserMatch ^Mozilla/4\.0[678] no-gzip
BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html
SetEnvIfNoCase Request_URI \.(?:gif|jpe?g|png)$ no-gzip
Header append Vary User-Agent env=!dont-vary