<?php
session_start();
include ("../conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:/login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'"); 

if(isset($_POST['mensagem'])){

	$mensagem = utf8_decode( strip_tags(trim(filter_input(INPUT_POST, 'mensagem', FILTER_SANITIZE_STRING))) );
	$de = (int)$_POST['de'];
	$para = (int)$_POST['para'];

	if($mensagem != ''){

		$sql = 'INSERT INTO mensagens (id_de, id_para, mensagem, time, lido) VALUES ('.$de.','.$para.',"'.$mensagem.'","'.time().'",0)';
		//echo $sql;
		$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
		if(mysqli_insert_id($conexao) > 0) echo 'ok';
		else echo 'no';
	}
}
?>