<?php
session_start();
include ("../conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:/login.php');
}

mysql_query("SET NAMES 'utf8'"); 


if(isset($_GET)){

	$userOnline = (int)$_GET['user'];
	$timestamp = ($_GET['timestamp'] == 0) ? time() : strip_tags(trim($_GET['timestamp']));
	$lastid = (isset($_GET['lastid']) && !empty($_GET['lastid'])) ? $_GET['lastid'] : 0;

	$usersOn = array();
	$agora = date('Y-m-d H:i:s');
	$expira = date('Y-m-d H:i:s', strtotime('+1 min'));

	$sql = 'UPDATE usuarios SET limite = ' . $expira . ' WHERE idUsuario = ' . $userOnline;
	$qr = mysql_query($sql) or die(mysql_error());

	$sql = 'SELECT * FROM usuarios WHERE idUsuario != ' . $userOnline . ' ORDER BY idUsuario DESC';
	$qr = mysql_query($sql) or die(mysql_error());
	while ($onlines = mysql_fetch_assoc($qr)) { 
		$foto = ($onlines['foto'] == '') ? 'default.jpg' : $onlines['foto'];
		$blocks = explode(',', $onlines['blocks']);
		if(!in_array($userOnline, $blocks)){
			if($agora >= $onlines['limite']){
				$usersOn[] = array('idUsuario' => $onlines['idUsuario'], 'nome' => utf8_encode($onlines['nome']), 'foto' => $foto, 'status' => 'off');
			}else{
				$usersOn[] = array('idUsuario' => $onlines['idUsuario'], 'nome' => utf8_encode($onlines['nome']), 'foto' => $foto, 'status' => 'on');
			}
		}
	}

	if(empty($timestamp)){
		die(json_encode(array('status' => 'erro')));
	}

	$tempoGasto = 0;
	$lastidQuery = '';

	if(!empty($lastid)){
		$lastidQuery = ' AND id > '.$lastid;
	}

	if($_GET['timestamp'] == 0){
		$sql = 'SELECT * FROM mensagens WHERE lido = 0 ORDER BY idUsuario DESC';
	}else{
		$sql = 'SELECT * FROM mensagens WHERE time >= "' . $timestamp . '" '.$lastidQuery.' AND lido = 0 ORDER BY idUsuario DESC';
	}
    $qr = mysql_query($sql) or die(mysql_error());
	$resultados = mysql_num_rows($qr);

	if($resultados <= 0){
		while($resultados <= 0){
			if($resultados <= 0){
				//durar 30 segundos verificando
				if($tempoGasto >= 30){
					die(json_encode(array('status' => 'vazio', 'lastid' => 0, 'timestamp' => time(), 'users' => $usersOn)));
					exit;
				}

				//descansar o script por um segundo
				sleep(1);
				$sql2 = 'SELECT * FROM mensagens WHERE time >= "' . $timestamp . '" '.$lastidQuery.' AND lido = 0 ORDER BY idUsuario DESC';
				$qr2 = mysql_query($sql2) or die(mysql_error());
				$resultados = mysql_num_rows($qr2);
				$tempoGasto += 1;
			}
		}
	}

	$novasMensagens = array();
	if($resultados >= 1){
		$emotions = array(':)', ':@', '8)', ':D', ':3', ':(', ';)');
		$imgs = array(
			'<img src="emotions/nice.png" width="14"/>',
			'<img src="emotions/angry.png" width="14"/>',
			'<img src="emotions/cool.png" width="14"/>',
			'<img src="emotions/happy.png" width="14"/>',
			'<img src="emotions/ooh.png" width="14"/>',
			'<img src="emotions/sad.png" width="14"/>',
			'<img src="emotions/right.png" width="14"/>'
		);

		while($row = mysql_fetch_assoc($resultados)){
			$fotoUser = '';
			$janela_de = 0;

			if($userOnline == $row['id_de']){
				$janela_de = $row['id_para'];

			}elseif($userOnline == $row['id_para']){
				$janela_de = $row['id_de'];

				$sql3 = 'SELECT foto FROM usuarios WHERE idUsuario = '.$row[id_de];
				$qr3 = mysql_query($sql3) or die(mysql_error());
				while($usr = mysql_fetch_assoc($qr3)){
					$fotoUser = ($usr['foto'] == '') ? 'default.jpg' : $usr['foto'];
				}
			}
			$msg = str_replace($emotions, $imgs, $row['mensagem']);
			$novasMensagens[] = array(
				'id' => $row['idUsuario'],
				'mensagem' => utf8_encode($msg),
				'fotoUser' => $fotoUser,
				'id_de' => $row['id_de'],
				'id_para' => $row['id_para'],
				'janela_de' => $janela_de
			);
		}
	}

	$ultimaMsg = end($novasMensagens);
	$ultimoId = $ultimaMsg['idUsuario'];
	die(json_encode(array('status' => 'resultados', 'timestamp' => time(), 'lastid' => $ultimoId, 'dados' => $novasMensagens, 'users' => $usersOn)));
}
?>