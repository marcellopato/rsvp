<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
	exit();
}

if ($_SESSION[$_adm] > 1) {
	header("location:login.php");
	exit();
}


require_once "Classes/PHPExcel.php";
include ("functions.php");

$erros = '';

$idMedico = 0;


if ($_POST['hIdEventoMedico'] > 0) {
	$sql = 'delete from eventos_medicos where idEvento_medico = ' . $_POST['hIdEventoMedico'];
//	echo $sql;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());

}




$idEvento = $_GET['idEvento'];

if (isset($_POST['btnCadastrar'])) {
    $uploaddir = 'assets/img/';
	if (strlen(basename($_FILES['foto']['name'])) > 2) {
		$uploadfile = $uploaddir . basename($_FILES['foto']['name']);
		move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile);
		$logoEvento = $uploadfile;
	}
//	echo $_POST['dataEvento'].' - '.date("Y-m-d",strtotime(str_replace('/','-',$_POST['dataEvento']))); die();
	
	$sql = 'update eventos set nome = "' . $_POST['nomeEvento'] . '", dtEvento = "' . 
	date("Y-m-d",strtotime(str_replace('/','-',$_POST['dataEvento']))) . '", hrEvento = "' . 
	$_POST['horaEvento'] . '", dtFinal = "' . 
	date("Y-m-d",strtotime(str_replace('/','-',$_POST['dataFinal']))) . '", hrFinal = "' . 
	$_POST['horaFinal'] . '", local = "' . $_POST['enderecoEvento'] . '", ';
	if (strlen($logoEvento) > 2) {
		$sql .= 'foto ="' . $logoEvento . '", ';
	}
	$sql .= 'idStatusEvento = ' . $_POST['cboStatusEvento'] . '   where idEvento = ' . $idEvento;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());

	$sql = "update eventos set mapa = '";
	$sql .= $_POST['mapa'] . "' where idEvento = " . $idEvento;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());



	$sql = 'SELECT et.* FROM eventos_tarefas et where idEvento = ' . $idEvento . ' ';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt = mysqli_fetch_assoc($qr)) {
			$sql = 'update eventos_tarefas set ativo = ' . ($_POST['chkTarefa_' . $qt['idEvento_tarefa']] == 'on' ? 1 : 0) . ' where idEvento_tarefa = ' . $qt['idEvento_tarefa'];
			$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
	}

// carga de mais convidados

include ("excell-carga.php");

// fim da carga



	//header('location:lista-eventos.php');

}

$sql = 'SELECT * FROM eventos where idEvento = ' . $idEvento;
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qe = mysqli_fetch_assoc($qr);


?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php
include 'head.php';
?>
<script type="text/javascript">

function deletar(id) {
	if (confirm("Deseja mesmo retirar o medico deste evento?")) {
		$('#hIdEventoMedico').val(id);
		form1.submit();	
	}
}

</script>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main"> 
	<!-- START WRAPPER -->
	<div class="wrapper">
		<?php
include 'navbar.php';
?>
		
		<!-- START CONTENT -->
		<section id="content"> 
			<!--start container-->
			<div class="container">
			<a href="index.php">Dashboard</a>  > <a href="lista-eventos.php">Lista de eventos</a> > Alteração de Eventos
				<h2 class="login-form-text2">Alteração de eventos</h2><a href="https://www.google.com.br/maps/" target="_blank"><img src="assets/img/google-maps.png" class="right"></a>
				<span id="lblErros"><?php echo '<br>'.$erros; ?></span>
				<div class="row">
					<form class="col s12" name="form1" method="post" enctype="multipart/form-data">
					<input type="hidden" id="hIdEvento" name="hIdEvento" value="<?php echo $_POST['hIdEvento']; ?>">
					<input type="hidden" id="hIdEventoMedico" name="hIdEventoMedico" value="0">
						<div class="row">
							<div class="input-field col s6">
								<input type="hidden" id="cboUsuario" name="cboUsuario" value="<?php	echo $qe['idUsuario']; ?>">
								<input type="text" id="txtUsuario" name="txtUsuario" readonly value="<?php	
	$sql = 'SELECT * FROM usuarios WHERE idNivel = 2 and idUsuario = ' . $qe['idUsuario'];
//	echo $sql;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt = mysqli_fetch_assoc($qr)) {
		echo $qt['nome'];
	}

 ?>" />
							<label>Cliente</label>
							</div>
							<div class="input-field col s6">
								<input id="nomeEvento" name="nomeEvento" type="text" class="validate" value="<?php echo $qe['nome']; ?>">
								<label for="nomeEvento">Nome do Evento</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col l3 s12">
								<label for="dataEvento">Data</label>
                                <input id="dataEvento" name="dataEvento" type="text" class="validate active" value="<?php echo date("d/m/Y", strtotime($qe['dtEvento'])); ?>">
							</div>
							<div class="input-field col l3 s12">
								<label for="horaEvento">Hora</label>
                                <input id="horaEvento" name="horaEvento" type="text" class="validate active" value="<?php echo $qe['hrEvento']; ?>">
							</div>
                        </div>
                        <div class="row">
                            <div class="input-field col l3 s12">
                                <label for="dataFinal">Data Final</label>
                                <input id="dataFinal" name="dataFinal" type="text" class="validate active" value="<?php echo date("d/m/Y", strtotime($qe['dtFinal'])); ?>">
                            </div>
                            <div class="input-field col l3 s12">
                                <label for="horaFinal">Hora Final</label>
                                <input id="horaFinal" name="horaFinal" type="text" class="validate active" value="<?php echo $qe['hrFinal']; ?>">
                            </div>
                            <div class="input-field col l12 s12">
                            </div>
                        </div>
                        <div class="input-field col l6 s12">
                            <input id="enderecoEvento" name="enderecoEvento" type="text" class="validate active" value="<?php echo $qe['local']; ?>">
                            <label for="enderecoEvento">Endereço <small><i>copie e cole no google maps</i></small></label>
                        </div>
                        <div class="input-field col l12 s12">
                            <input id="mapa" name="mapa" type="text" class="validate active" value='<?php echo $qe['mapa']; ?>'>
                            <label for="mapa">Mapa do local <small><i>No Google Maps escolha tamanho personalizado em 1056 x 456</i></small></label>
                        </div>
						<div class="divider"></div>
						<br>
						<div class="row">
						    <div class="col l12 s12">
                                <select id="cboStatusEvento" name="cboStatusEvento" class="browser-default">
<?php 
//	echo '<option value="0"';
//	if ($_POST['cboUsuario'] < 1) echo ' selected';
//	echo '>Todos</option>';

	$sql = 'SELECT * FROM statusEvento order by idStatusEvento';
//	echo $sql;
	$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt2 = mysqli_fetch_assoc($qr2)) {
		echo '<option value="' . $qt2['idStatusEvento'] . '"';
			if ($qt2['idStatusEvento'] == $qe['idStatusEvento']) echo ' selected';
		echo '>' . $qt2['descricao'] . '</option>'; 	
	}
?>
								</select>
                            </div>
						</div>
						<br>
						<div class="row">
                        <?php echo '<strong>Essa imagem está neste link:http://sistemarsvp.com.br/' . $qe['foto'] . '</strong>'; ?>
						    <div class="col l12 s12">
                                <input type="file" id="foto" name="foto" class="dropify" data-allowed-file-extensions="png jpg jpeg gif bmp" data-show-errors="true" data-max-file-size="3M" data-default-file="<?php echo $qe['foto'] ?>">
                            </div>
						</div>
						<br>
						<div class="row">
							<button class="btn waves-effect waves-light" type="submit" name="btnCadastrar">atualizar <i class="material-icons right">send</i> </button>
                            <button class="btn waves-effect waves-light" type="button" name="btnExportar" onClick="javascript:window.open('exportar-eventos.php?idEvento=<?php echo $idEvento; ?>','');">exportar <i class="material-icons right">send</i> </button>
						</div>
						<div class="divider"></div>
						<br>
						<div class="row">
							<h2 class="login-form-text2">Tarefas</h2>
							<br>
							<ul class="collection">
								<?php
                                    $sql = 'SELECT et.ativo, et.idEvento_tarefa, t.* FROM tarefas t join eventos_tarefas et on t.idTarefa = et.idTarefa and et.idEvento = ' . $idEvento . ' order by t.idTarefa';
                                //	echo $sql;
                                    $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
									$linhaOld = '';
                                    while ($qt = mysqli_fetch_assoc($qr)) {
										//$sql2 = 'select count(idTarefa) as conta from eventos_tarefas where idEvento = ' . $idEvento . ' and idTarefa = ' . $qt['idTarefa'];
										//echo $sql2;
										//$qr2 = mysqli_query($conexao,$sql2) or die(mysqli_error());
										//$qt2 = mysqli_fetch_assoc($qr2);
                                        echo '<li class="collection-item">
                                            <input type="checkbox" id="chkTarefa_' . $qt['idEvento_tarefa'] . '" name="chkTarefa_' . $qt['idEvento_tarefa'] . '" ' . ($qt['ativo'] == 1 ? ' checked' : '') . '/>
                                            <label for="chkTarefa_' . $qt['idEvento_tarefa'] . '">' . $qt['tarefa'] . '</label>
                                            </li>';
                                    }
                                ?>
							</ul>
						</div>
						<div class="row">
						<h2 class="login-form-text2">Carga de novos Convidados</h2>
							<div class="col l6 s12">
								<input type="file" id="arquivo" name="arquivo" class="dropify" data-allowed-file-extensions="xlsx" data-show-errors="true" data-max-file-size="3M" data-default-file="">
						    </div>
							<button class="btn waves-effect waves-light" type="submit" name="btnCadastrar">cadastrar <i class="material-icons right">send</i> </button>
						</div>
						<br>
                        <h2 class="login-form-text2">Lista de Convidados para esse evento</h2>
                        <br>
                           <table class="bordered highlight responsive-table">
                            <thead>
                              <tr>
                                  <th data-field="id">Nome</th>
                                  <th data-field="name">crm</th>
                                  <th data-field="rg">rg</th>
                                  <th data-field="cpf">cpf</th>
                                  <th data-field="email">email</th>
                                  <th data-field="x"></th>
                              </tr>
                            </thead>
                            <tbody>
<?php
	$sql = 'SELECT m.nome, m.crm, m.rg, m.cpf, m.email, em.idEvento_medico from medicos m join eventos_medicos em on m.idMedico = em.idMedico where idEvento = ' . $idEvento . ' order by m.nome';
//	echo $sql;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	$linhaOld = '';
	while ($qt = mysqli_fetch_assoc($qr)) {
		echo '<tr><td>' .
//		achaLaboratorio($conexao,$qt['idUsuario']) . ' - ' .
		 $qt['nome'] . '</td><td>' . $qt['crm'] . '</td><td>' . $qt['rg'] . '</td><td>' . $qt['cpf'] . '</td><td>' . $qt['email'] . '</td><td><input type="button" value="Retirar" id="btnDeletar" name="btnDeletar" onClick="deletar(' . $qt['idEvento_medico'] . ');" /></tr>';	}
?>
                           </tbody>
                          </table>
                          <br>
					</form>
				</div>
			</div>
			<!--end container--> 
		</section>
		<!-- END CONTENT --> 
		<!-- //////////////////////////////////////////////////////////////////////////// --> 
		<!-- START RIGHT SIDEBAR NAV--> 
		
		<!-- LEFT RIGHT SIDEBAR NAV--> 
		
	</div>
	<!-- END WRAPPER --> 
	
</div>
<!-- END MAIN --> 

<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START FOOTER -->
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
	</div>
</footer>
<!-- END FOOTER --> 

<!-- ================================================
    Scripts
    ================================================ --> 

<!-- jQuery Library --> 
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script> 
<!--materialize js--> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
        <!-- upload -->
        <script type="text/javascript" src="js/dropify.js"></script>
        <!--moments and locale-->
        <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
        <!--date time picker-->
        <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>
<!--sortable--> 
<script type="text/javascript" src="js/jquery.sortable.js"></script> 
<!-- upload --> 
<script type="text/javascript" src="js/dropify.js"></script>
<!--scrollbar--> 
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="js/plugins.min.js"></script> 
<!--nestable --> 
<script type="text/javascript" src="js/plugins/jquery.nestable/jquery.nestable.js"></script> 
<script type="text/javascript" src="js/jquery.mask.min.js"></script>
<!-- Toast Notification --> 
<script>
  $(document).ready(function() {
      Materialize.updateTextFields();
      $('.dropify').dropify();
		jQuery(function($){
			$('#dataEvento').mask('00-00-0000');
			$("#horaEvento").mask("00:00")
		});
  });
</script> 
<script>
    $('.sortable').sortable();
</script>
</ul>
</body>
</html>