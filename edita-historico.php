<?php
include("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
header('location:login.php');
}

include ("functions.php");

mysqli_query($conexao,"SET NAMES 'utf8'");
//echo 'U:'.$_SESSION['idUsuario']; die();

$usuario = $_SESSION[$_codigo];
if ($_SESSION[$_adm] > 1) {
	header("location:index.php");
	exit();
}

if (isset($_GET['idem']) && $_GET['idem'] > 0) {
	$sql = 'update eventos_medicos set ';
	if ($_GET['ind'] == 1) $sql .= 'indRSVP';
	if ($_GET['ind'] == 2) $sql .= 'indAereo';
	if ($_GET['ind'] == 3) $sql .= 'indTransfer';
	if ($_GET['ind'] == 4) $sql .= 'indHospedagem';
	if ($_GET['ind'] == 5) $sql .= 'indInscricao';
	$sql .= ' = ' . $_GET['valor'];
	$atualizaTudo = 0;
	if ($_GET['ind'] == 1 && $_GET['valor'] == 0) {
		$sql .= ', indAereo = 0, indTransfer = 0, indHospedagem = 0, indInscricao = 0';
		$atualizaTudo = 1;
	}
	$sql .= ' where idEvento_medico = '.$_GET['idem'];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	atualizaPorcConclusao($conexao,$_SESSION[$_evento],1);
	if ($atualizaTudo == 1) {
		atualizaPorcConclusao($conexao,$_SESSION[$_evento],2);
		atualizaPorcConclusao($conexao,$_SESSION[$_evento],3);
		atualizaPorcConclusao($conexao,$_SESSION[$_evento],4);
	}
}


//$usuarioaltera = $_GET['id'];
$sql_medicos = 'SELECT m.*, em.* FROM medicos m join eventos_medicos em on m.idMedico = em.idMedico WHERE idEvento_medico = ' . $_GET['id'];
//echo $sql_medicos;
$qr = mysqli_query($conexao,$sql_medicos) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);

if (isset($_POST['atualizar'])){
    $sql = 'UPDATE eventos_medicos SET historicoContato = "' . $qt['historicoContato'] . ' [' .  
	date('d-m-Y H:i') . '] [' . $_SESSION[$_codigo] . '-' . $_SESSION[$_nome] . '] ' . $_POST['historico'] . '"  WHERE idEvento_medico = '.$_GET['id'];
//	echo $sql;
    mysqli_query($conexao,$sql);
    //header("location:lista-medicos.php");
    //exit();
}


//$usuarioaltera = $_GET['id'];
$sql_medicos = 'SELECT m.*, em.* FROM medicos m join eventos_medicos em on m.idMedico = em.idMedico WHERE idEvento_medico = ' . $_GET['id'];
//echo $sql_medicos;
$qr = mysqli_query($conexao,$sql_medicos) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);


?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
<script type="text/javascript">


function salva(id, ind, valor) {
	window.location = 'edita-historico.php?idem='+id+'&ind='+ind+'&valor='+valor+'&id=<?php echo $_GET['id']; ?>';
}
</script>
</head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
                    include 'navbar.php';
                    ?>

                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <a href="index.php">Dashboard</a> > <a href="index.php">Lista de Médicos</a> > Historico de contato
                                <form id="form1" name="form1" class="col s12" action="edita-historico.php?id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" value="cadastrar" id="cadastrar" name="cadastrar">
                                    <h2 class="login-form-text2">Historico de contato com Médico</h2>
                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <span>Nome Completo: <?php echo $qt['nome'] ?></span>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>Empresa :<?php echo $qt['empresa'] ?></span>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>Especialidade: <?php echo $qt['especialidade'] ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <span>Email: <?php echo $qt['email'] ?></span>
                                                
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>CRM: <?php echo $qt['crm'] ?></span>
                                                
                                            </div>
                                            <div class="input-field col l2 s12">
                                                 <span>RG: <?php echo $qt['rg'] ?></span>
                                                
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <span>CPF: <?php echo $qt['cpf'] ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <span><?php echo $qt['telefone'] ?></span>
                                            </div>
                                            <div class="input-field col l6 s12">
                                               <span><?php echo $qt['celular'] ?></span>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <br>
										<div class="row">
<?php

$sql = 'SELECT em.*,m.* FROM eventos_medicos em join medicos m on em.idMedico = m.idMedico WHERE m.idMedico = ' . $qt['idMedico'];

if ($_SESSION[$_adm] > 2) {
	$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
}

$sql .= ' and idEvento_medico = ' . $_GET['id'];
$sql .= ' order by m.nome';

$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$total = mysqli_num_rows($qr);
?>
                 <table class="bordered striped responsive-table">
                        <thead>
                          <tr>
                              <th data-field="nome">Nome</th>
                              <th data-field="rsvp">Aceitou?</th>
                              <th data-field="aereo">Aéreo?</th>
                              <th data-field="aereo">Transfer?</th>
                              <th data-field="aereo">Hospedagem?</th>
                              <th data-field="aereo">Inscrição?</th>
                          </tr>
                        </thead>
                        <tbody>
                         <!--começa o loop-->
<?php

while ($qe = mysqli_fetch_assoc($qr)) {


	if ($_SESSION[$_adm] == 1) {
		echo '<tr><td><a href="altera-medicos.php?id=' . $qe['idMedico'] . '">' . $qe['nome'] . '</a></td>';
	}
	else {
		echo '<tr><td><a href="exibe-evento-medico.php?id=' . $qe['idEvento_medico'] . '">' . $qe['nome'] . '</a></td>';
	}
	
	echo '<td><i ';
	if ($_SESSION[$_adm] == 1) echo 'onClick="javascript:salva('.$qe['idEvento_medico'].',1,1);" ';
	echo 'class="fa fa-check '.($qe['indRSVP']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',1,0);" ';
	echo 'class="fa fa-ban '.($qe['indRSVP']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';
	
	echo '<td><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',2,1);" ';
	echo 'class="fa fa-check '.($qe['indAereo']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',2,0);" ';
	echo 'class="fa fa-ban '.($qe['indAereo']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '<td><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',3,1);" ';
	echo 'class="fa fa-check '.($qe['indTransfer']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',3,0);" ';
	echo 'class="fa fa-ban '.($qe['indTransfer']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '<td><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',4,1);" ';
	echo 'class="fa fa-check '.($qe['indHospedagem']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',4,0);" ';
	echo 'class="fa fa-ban '.($qe['indHospedagem']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '<td><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',5,1);" ';
	echo 'class="fa fa-check '.($qe['indInscricao']==1 ? 'blue' : 'grey') . '-text light btn-flat" aria-hidden="true"></i><i ';
	if ($_SESSION[$_adm] == 1 && $qe['indRSVP']==1) echo 'onClick="javascript:salva('.$qe['idEvento_medico']. ',5,0);" ';
	echo 'class="fa fa-ban '.($qe['indInscricao']==0 ? 'red' : 'grey') . '-text light btn-flat" aria-hidden="true"></i></td>';

	echo '</tr>';

}
?>
                          <!--termina o loop-->
                        </tbody>
                </table>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <br>
                                        <div class="row">
                                            <div class="input-field col s12">
                                            <textarea readonly id='historicoAntigo' name="historicoAntigo" class="materialize-textarea"><?php echo strip_tags($qt['historicoContato']) ?></textarea>
                                            <label class="active" for="historicoAntigo">Historico</label>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="input-field col s12">
                                            <textarea id='historico' name="historico" class="materialize-textarea"></textarea>
                                            <label class="active" for="historico">Novo Historico</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
<?php 
// acessar dropbox e mostrar todas ligacoes que like fone ou cel deste medico
?>
<span>lista de chamados aos telefones conhecidos deste medico neste evento
</span>
<iframe id="frmDropBox" name="frmDropBox" src="web-file-browser.php" width="90%" height="200"></iframe>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <br>
                                        <button class="btn waves-effect waves-light" type="submit" name="atualizar" id="atualizar">atualizar
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                                <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <?php include('sidebar.php');?>
                            <!-- LEFT RIGHT SIDEBAR NAV-->
                            </div>
                            <!-- END WRAPPER -->
                </div>
                <!-- END MAIN -->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
                <!-- START FOOTER -->
                <footer class="page-footer">
                    <div class="footer-copyright">
                        <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
                    </div>
                </footer>
                <!-- END FOOTER -->
                <!-- ================================================
    Scripts
    ================================================ -->
                <script src="https://code.jquery.com/jquery-1.11.2.js"></script>
                <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                <!--materialize js-->
                <script type="text/javascript" src="js/materialize.min.js"></script>
                <!-- upload -->
                <script type="text/javascript" src="js/dropify.js"></script>
                <!--scrollbar-->
                <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
                <!--plugins.js - Some Specific JS codes for Plugin Settings-->
                <script type="text/javascript" src="js/plugins.min.js"></script>
                <script src="ckeditor/ckeditor.js"></script>
                <script>
                    CKEDITOR.replace('historico', { });
                </script>
                <script type="text/javascript">
                    $(document).ready(function() {
                        Materialize.updateTextFields();
                        $('.dropify').dropify();
                        tinymce.init({
                                selector: '#observacoes',
                                plugins: [
                                    "advlist autolink lists link image charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table contextmenu paste"
                                ],
                                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                            });
                    });
                </script>
    </body>
    </html>