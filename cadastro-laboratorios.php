<?php
include("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
header('location:login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'");
//echo 'U:'.$_SESSION['idUsuario']; die();

$usuario = $_SESSION[$_codigo];
if ($_SESSION[$_adm] > 4) {
	header("location:index.php");
	exit();
}

	$sql_usuario = 'SELECT us.*,ni.txtNivel FROM usuarios us JOIN nivels ni on ni.idNivel = us.idNivel WHERE ((us.idUsuario = "' . $usuario . '"))';
	//echo $sql_usuario;
	$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
	$qt = mysqli_fetch_assoc($qr);


if (isset($_POST['cadastrar'])){
    $uploaddir = 'assets/img/';
    $uploadfile = $uploaddir . basename($_FILES['input-file-now']['name']);
    move_uploaded_file($_FILES['input-file-now']['tmp_name'], $uploadfile);
    $avatar = $uploadfile;
       
    $nome = $_POST['nome'];
    $login = $_POST['login'];
    $pass = $_POST['senha'];
    $nivel = $_POST['cboNivelAtual'];
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    $cel = $_POST['celular'];
    $rua = $_POST['end'];
    $avatar;
    $insere_usuario = 'INSERT INTO sistemar_rsvp.usuarios (idUsuario, nome, login, senha, idNivel, idUsuarioPai, email, foto, telefone, celular, endereco) VALUES (NULL, "' . $nome . '", "' . $login . '", "' . $pass . '", 2, 0,"' . $email . '", "' . $avatar . '", "' . $tel . '", "' . $cel . '", "' . $rua . '");';
    //echo $insere_usuario;
    mysqli_query($conexao,$insere_usuario) or die(mysqli_error());
    header("location:lista-laboratorios.php");
    exit();
}


?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
    </head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
include 'navbar.php';
?>

                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                               <a href="index.php">Dashboard</a> > <a href="lista-laboratorios.php">Lista de clientes</a> > Cadastro de clientes
                                <form id="form1" name="form1" class="col s12" action="" method="post" enctype="multipart/form-data">
                                    <input type="hidden" value="cadastrar" id="cadastrar" name="cadastrar">
                                    <h2 class="login-form-text2">Cadastro de clientes</h2>
                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="nome" name="nome" type="text" class="validate" required>
                                                <label for="nome">Nome Completo</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <input id="login" name="login" type="text" class="validate" required>
                                                <label class="active" for="login">Nome</label>
                                            </div>


                                            <div class="input-field col l6 s12">
                                                <input id="senha" name="senha" type="password" class="validate" required>
                                                <label class="active" for="senha">Senha</label>
                                            </div>
                                            <div class="input-field col l6 s12">
                                                <input id="end" name="end" type="text" class="validate active" required>
                                                <label class="active" for="end">Endereço</label>
                                            </div>
                                            <div class="input-field col l6 s12">
                                                <input id="email" name="email" type="email" class="validate active" required>
                                                <label class="active" for="email">Email</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col l6 s12">
                                                <input type="file" id="input-file-now" name="input-file-now" class="dropify" data-allowed-file-extensions="png jpg jpeg gif bmp" data-show-errors="true" data-max-file-size="3M" data-default-file="http://sistemarsvp.com.br/assets/img/anonymous.gif">
                                            </div>
                                            <div class="input-field col  l6 s12">
                                                <div class="input-field">
                                                    <input id="tel" name="tel" type="text" class="validate" required>
                                                    <label class="active" for="tel">Telefone <small><i>separe por virgulas</i></small></label>
                                                </div>
                                                <div class="input-field">
                                                    <input id="celular" name="celular" type="text" class="validate" required>
                                                    <label class="active" for="celular">Celular <small><i>separe por virgulas</i></small></label>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="divider"></div><br>
                                        <div>
                                            <button class="btn waves-effect waves-light pink" type="submit" id="cadastrar" name="cadastrar">cadastrar <i class="material-icons right">send</i> </button>
                                        </div>
                                    </div>
                                </form>
                                <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <aside id="right-sidebar-nav">

                        </aside>
                        <!-- LEFT RIGHT SIDEBAR NAV-->
                        </div>
                        <!-- END WRAPPER -->
                </div>
                <!-- END MAIN -->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
                <!-- START FOOTER -->
                <footer class="page-footer">
                    <div class="footer-copyright">
                        <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
                    </div>
                </footer>
                <!-- END FOOTER -->
                <!-- ================================================
    Scripts
    ================================================ -->
                <!-- jQuery Library -->
                <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
                <!--materialize js-->
                <script type="text/javascript" src="js/materialize.min.js"></script>
                <!-- upload -->
                <script type="text/javascript" src="js/dropify.js"></script>
                <!--scrollbar-->
                <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
                <!--plugins.js - Some Specific JS codes for Plugin Settings-->
                <script type="text/javascript" src="js/plugins.min.js"></script>
                <!-- Toast Notification -->
                <script type="text/javascript">
                    $(document).ready(function() {
                        Materialize.updateTextFields();
                        $('.dropify').dropify();
                    });

                </script>
                <?php if (isset($_GET['cad']) && ($_GET['cad']) == '1') { ?>
                    <script>
                        var $toastContent = $('<span>Cadastro atualizado com SUCESSO!</span>');
                        Materialize.toast($toastContent, 5000);

                    </script>
                    <?php } ?>
    </body>

    </html>
