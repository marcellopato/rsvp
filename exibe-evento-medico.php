<?php

include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

if (isset($_GET['id']) && $_GET['id'] > 0) {
	$sql = 'SELECT e.mapa, m.*,em.* FROM medicos m join eventos_medicos em on m.idMedico = em.idMedico join eventos e on em.idEvento = e.idEvento WHERE em.idEvento_medico = ' . $_GET['id'];
	if ($_SESSION[$_adm] > 2) {
		$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
	}
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	if (mysqli_num_rows($qr) < 1) {
		header('location:login.php');
		exit();
	}
	$qt = mysqli_fetch_assoc($qr);
}



function mostraVoo($qv) {
		echo ' <div class="flight-info style="text-align: justify;">';
		echo '   <p><span class="grey-text text-lighten-4">Data:</span> '.date("d-m-Y", strtotime($qv['dtVoo'])).'</p>';
		echo '   <p><span class="grey-text text-lighten-4">Cia Aérea:</span> '.$qv['ciaAerea'].'</p>';
		echo '   <p><span class="grey-text text-lighten-4">Vôo Nº:</span> '.$qv['nroVoo'].'</p>';
		echo '   <p><span class="grey-text text-lighten-4">Origem:</span> '.$qv['origemVoo'].'</p>';
		echo '   <p><span class="grey-text text-lighten-4">Destino:</span> '.$qv['destinoVoo'].'</p>';
		echo '   <p><span class="grey-text text-lighten-4">Saída:</span> '.date("H:i", strtotime($qv['horaSaidaVoo'])).'</p>';
		echo '   <p><span class="grey-text text-lighten-4">Chegada:</span> '.date("H:i", strtotime($qv['horaChegadaVoo'])).'</p>';
		echo '   <p><span class="grey-text text-lighten-4">   </span> ';
		echo ' </div>';
}

?>
  <!DOCTYPE html>
  <html lang="pt-BR">

  <head>
    <?php
include 'head.php';
?>
  </head>

  <body>
    <?php
include 'header.php';
?>
      <!-- START MAIN -->
      <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
          <?php
include 'navbar.php';
?>
            <!-- START CONTENT -->
            <section id="content">
              <!--start container-->
              <div class="container">
                <h4 class="card-stats-number">
                  <?php echo $qt['nome']; ?>
                </h4>
                <!--card widgets start-->
                <!--<div id="card-widgets"> -->
                <div class="row">
                  <div class="card">
                    <div class="card-content">
                      <?php echo str_replace('[','<br>[',$qt['historicoContato']); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
<?php
if ($qt['indAereo'] == 1) {
?>                  
                  <div class="col s12 m12 l4">
                    <div id="flight-card" class="card">
                      <div class="card-header amber darken-2">
                        <div class="card-title">
                          <h4 class="flight-card-title">Aéreo</h4>
                        </div>
                      </div>
                      <div class="card-content-bg white-text">
                        <div class="card-content">
                          <div class="row flight-state-wrapper">
                            <div class="col s5 m5 l5 center-align">
                              <div class="flight-state">
                                <h4 class="margin">IDA</h4>
                              </div>
                              <?php
	$sql = 'SELECT v.* FROM voos v join eventos_medicos em on v.idEvento_medico = em.idEvento_medico where v.idaVolta = 1 and v.idEvento_medico = ' . $_GET['id'];
	if ($_SESSION[$_adm] > 1) {
		$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
	}
	$sql .=  ' order by idVoo';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qv = mysqli_fetch_assoc($qr)) {
		mostraVoo($qv);
	}
?>
                            </div>
                            <div class="col s2 m2 l2 center-align"> <i class="fa fa-plane fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col s5 m5 l5 center-align">
                              <div class="flight-state">
                                <h4 class="margin">VOLTA</h4>
                              </div>
                              <?php
	$sql = 'SELECT v.* FROM voos v join eventos_medicos em on v.idEvento_medico = em.idEvento_medico where v.idaVolta = 2 and v.idEvento_medico = ' . $_GET['id'];
	if ($_SESSION[$_adm] > 1) {
		$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
	}
	$sql .=  ' order by idVoo';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qv = mysqli_fetch_assoc($qr)) {
		mostraVoo($qv);
	}
?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
<?php
}

if ($qt['indTransfer'] == 1) {
?>                  
                  
                  
                  <div class="col s12 m12 l4">
                    <div class="card">
                      <div class="card-image">
                        <img src="assets/img/limousine.jpg">
                        <span class="card-title">Traslado</span>
                      </div>
                      <div class="card-content">
                        <p>Dados de seu traslado</p><br>
                        <?php              
   if (strlen($qt['origemTransfer']) > 1) echo '<p><i class="fa fa-home" aria-hidden="true"></i> - <i class="fa fa-plane" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qt['dataTransfer'])).'</p><p>Hora: '.date("H:i", strtotime($qt['horaTransfer'])).'</p><p>Motorista: '.$qt['motorista'].'</p><p>Veículo: '.$qt['txtVeiculo'].'</p><p>de: '.$qt['origemTransfer'].'</p><p>para: '.$qt['destinoTransfer'].'</p>';
   if (strlen($qt['origemTransfer2']) > 1) echo '<p><i class="fa fa-plane" aria-hidden="true"></i> - <i class="fa fa-bed" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qt['dataTransfer2'])).'</p><p>Hora: '.date("H:i", strtotime($qt['horaTransfer2'])).'</p><p>Motorista: '.$qt['motorista2'].'</p><p>Veículo: '.$qt['txtVeiculo2'].'</p><p>de: '.$qt['origemTransfer2'].'</p><p>para: '.$qt['destinoTransfer2'].'</p>';
   if (strlen($qt['origemTransfer3']) > 1) echo '<p > <i class="fa fa-bed" aria-hidden="true"></i> - <i class="fa fa-building" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qt['dataTransfer3'])).'</p><p>Hora: '.date("H:i", strtotime($qt['horaTransfer3'])).'</p><p>Motorista: '.$qt['motorista3'].'</p><p>Veículo: '.$qt['txtVeiculo3'].'</p><p>de: '.$qt['origemTransfer3'].'</p><p>para: '.$qt['destinoTransfer3'].'</p>';
   if (strlen($qt['origemTransfer4']) > 1) echo '<p><i class="fa fa-building" aria-hidden="true"></i> - <i class="fa fa-bed" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qt['dataTransfer4'])).'</p><p>Hora: '.date("H:i", strtotime($qt['horaTransfer4'])).'</p><p>Motorista: '.$qt['motorista4'].'</p><p>Veículo: '.$qt['txtVeiculo4'].'</p><p>de: '.$qt['origemTransfer4'].'</p><p>para: '.$qt['destinoTransfer4'].'</p>';
   if (strlen($qt['origemTransfer5']) > 1) echo '<p > <i class="fa fa-bed" aria-hidden="true"></i> - <i class="fa fa-plane" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qt['dataTransfer5'])).'</p><p>Hora: '.date("H:i", strtotime($qt['horaTransfer5'])).'</p><p>Motorista: '.$qt['motorista5'].'</p><p>Veículo: '.$qt['txtVeiculo5'].'</p><p>de: '.$qt['origemTransfer5'].'</p><p>para: '.$qt['destinoTransfer5'].'</p>';
   if (strlen($qt['origemTransfer6']) > 1) echo '<p><i class="fa fa-plane" aria-hidden="true"></i> - <i class="fa fa-home" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qt['dataTransfer6'])).'</p><p>Hora: '.date("H:i", strtotime($qt['horaTransfer6'])).'</p><p>Motorista: '.$qt['motorista6'].'</p><p>Veículo: '.$qt['txtVeiculo6'].'</p><p>de: '.$qt['origemTransfer6'].'</p><p>para: '.$qt['destinoTransfer6'].'</p>';
?>
                      </div>
                    </div>
                  </div>
<?php
}

if ($qt['indHospedagem'] == 1) {
?>                  
                  <div class="col s12 m12 l4">
                    <div class="card">
                      <div class="card-image waves-effect waves-block waves-light"> <img src="assets/img/hotel.jpg" alt="blog-img"> </div>
                      <div class="card-content">
                        <p class="row">CheckIn:
                          <?php echo ($qt['dtCheckIn'] < date("Y-m-d") ? '' : date("d-m-Y", strtotime($qt['dtCheckIn']))); ?>
                        </p>
                        <p class="row">CheckOut:
                          <?php echo ($qt['dtCheckOut'] < date("Y-m-d") ? '' : date("d-m-Y", strtotime($qt['dtCheckOut']))); ?>
                        </p>
                        <p class="row">Quarto:
                          <?php echo $qt['nroQuarto']; ?>
                        </p>
                        <p class="row">Observações:
                          <?php echo $qt['obsHospedagem']; ?>
                        </p>
                      </div>
                    </div>
                  </div>
<?php
}

if ($qt['indInscricao'] == 1) {
?>                  
                  <div class="col s12 m12 l4">
                    <div class="card">
                      <div class="card-image">
                        <img src="assets/img/inscricoes.jpg">
                        <span class="card-title black-text">Inscrição</span>
                        
                      </div>
                      <div class="card-content">
                      <p class="row">Numero:
                          <?php echo $qt['nroInscricao']; ?>
                      </p>
                      </div>
                    </div>
                  </div>
<?php
}
?>
                </div>
                <div class="row center">
                  <div class="card">
                    <?php
                        if (strlen($qt['mapa']) > 2) {
                          echo $qt['mapa'];
                        }
                        ?>
                  </div>
                </div>
                
                                        <div class="divider"></div>
                                        <br>
               <table class="bordered highlight responsive-table">
                <thead>
                  <tr>
                      <th data-field="id">Evento</th>
                      <th data-field="name">Pergunta</th>
                      <th data-field="price">Resposta</th>
                      <th data-field="price">Data</th>
                  </tr>
                </thead>
                <tbody>

<?php

	$sql = 'SELECT p.*, e.nome, r.resposta, r.dtResposta from perguntas p join eventos e on p.idEvento = e.idEvento join respostasMedicos r on p.idPergunta = r.idPergunta where idMedico = ' . $qt['idMedico'] . ' order by idEvento, idPergunta';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qe = mysqli_fetch_assoc($qr)) {
		echo '<tr><td>' . $qe['nome'] . '</td><td>' . $qe['txtPergunta'] . '</td><td>' . $qe['resposta'] . '</td><td>' . date("d-m-Y H:i", strtotime($qe['dtResposta'])) . '</td></tr>';
		
	}



?>                                       
				</tbody>
                </table>
                                        <div class="divider"></div>
                                        <br>
                
                <!--end container-->
            </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
            </div>
            <!-- END WRAPPER -->
        </div>
        <!-- END MAIN -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START FOOTER -->
        <footer class="page-footer">
          <div class="footer-copyright">
            <div class="container">
              Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a>              Todos os reservados.
              <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
          </div>
        </footer>
        <!-- END FOOTER -->
        <!-- ================================================
    Scripts
    ================================================ -->
        <!-- jQuery Library -->
        <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
        <!--materialize js-->
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <!--scrollbar-->
        <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <!-- chartist -->
        <script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>
        <!-- chartjs -->
        <script type="text/javascript" src="js/plugins/chartjs/chart.min.js"></script>
        <script type="text/javascript" src="js/plugins/chartjs/chart-script.js"></script>
        <!-- sparkline -->
        <script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script>
        <!-- google map api -->
        <!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAZnaZBXLqNBRXjd-82km_NO7GUItyKek"></script>
-->
        <!--jvectormap-->
        <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script>
        <!--plugins.js - Some Specific JS codes for Plugin Settings-->
        <script type="text/javascript" src="js/plugins.min.js"></script>
  </body>

  </html>