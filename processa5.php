<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

if ($_SESSION[$_idTarefa] > 0 && $_SESSION[$_idTarefa] != 5) {
	header('location:login.php');
}
	

include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 


if (isset($_POST['action'])) {
//	echo 	$_POST['hId'];


	$dataHoje = date('Y-m-d');
	$erros = '';

	// data do evento
	$dataEvento = date('Y-m-d');
	$sql = 'select dtEvento from eventos where idEvento  = '.$_SESSION[$_evento];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt = mysqli_fetch_assoc($qr)) {
		$dataEvento = $qt['dtEvento'];
	}

//	if ($_POST['dtCheckIn'] > $dataEvento) $erros .= 'data checkin depois do evento, ';



	if ($erros == '') {
		$sql = 'update eventos_medicos set nroInscricao = "'.$_POST['nroInscricao'].'" WHERE idEvento_medico = ' . $_POST['hId'];
	//	echo $sql; die();
		$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
		atualizaPorcConclusao($conexao,$_SESSION[$_evento],5);
		header('location:processa5.php');
	}
	else {
		echo $erros;
	}
	
}

$sql = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idEvento = ' . $_SESSION[$_evento];
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<?php
include 'head.php';
?>
<script type="text/javascript">

function openModal(id) {
	$('#hId').val(id);
	document.form1.submit();
//	$('#modal1').show();	
}

</script>
</head>

<body>
<?php
include 'header.php';
?>
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
<?php
include 'navbar.php';
?>		
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                <a href="index.php">Dashboard</a> > Inscrição
<?php
$sql = 'SELECT em.*,m.* FROM eventos_medicos em join medicos m on em.idMedico = m.idMedico WHERE em.idEvento = ' . $_SESSION[$_evento] . ' and em.indInscricao = 1 ';

if ($_SESSION[$_adm] > 1) {
	$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
}

$sql .= '  order by m.nome';
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$total = mysqli_num_rows($qr);
?>
                <h2 class="login-form-text2"><?php echo $qt['nome'] . ' ('. $total . ')'; ?></h2>
                <table class="bordered striped">
                    <thead>
                        <tr>
                            <th data-field="nome">Nome</th>
                            <th data-field="rsvp">Número da Inscrição</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--começa o loop-->
<?php
while ($qe = mysqli_fetch_assoc($qr)) {

?>
                        <tr>
                            <td><?php echo $qe['nome']; ?></td><!-- TEM QUE PEGAR O ID DO MÉDICO PRA PASSAR PRO MODAL -->
                            <td><a class="waves-effect waves-light modal-trigger" href="javascript:openModal(<?php echo $qe['idEvento_medico']; ?>);"><i class="fa fa-check-circle <?php echo (strlen($qe['nroInscricao']) > 1 ? 'green-text' : 'grey-text'); ?> light" aria-hidden="true"></i></a></td>
                        </tr>
<?php
}
?>                          
                        <!--termina o loop-->
                    </tbody>
                </table>                
<script type="text/javascript">
<?php

if ($_POST['hId'] > 0) {
	$sql = 'SELECT em.*,m.* FROM eventos_medicos em join medicos m on em.idMedico = m.idMedico WHERE em.idEvento_medico = ' . $_POST['hId'];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	$qe = mysqli_fetch_assoc($qr);

//	echo '$("#modal1").hide();';
}
?>                                
</script>

        <form name="form1" id="form1" method="post" action="processa5.php" class="col s12">
        <input type="hidden" name="hId" id="hId" value="<?php echo $_POST['hId']; ?>">
                 <div id="modal1" class="modal"> <!-- TEM QUE PEGAR O ID DO MÉDICO PRA PASSAR PRO MODAL -->
                    <div class="row">
                       <h3 class="login-form-text2">Inscrição</h3>
                        <div class="input-field col l4 s12">
                          <input id="nroInscricao" name="nroInscricao" type="text" class="validate" value="<?php echo $qe['nroInscricao']; ?>">
                          <label for="nroInscricao">Numero</label>
                        </div> 
                      </div>
<?php
if ($_SESSION[$_adm] == 1) {
?>
                      <div class="row">
                          <button class="btn waves-effect waves-light right" type="submit" name="action" style="margin-right: 10px;">cadastrar
                            <i class="material-icons right">send</i>
                          </button>
                      </div>
<?php
}
?>
                  </div>    
                </div>
                    </form>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
            <?php include 'sidebar.php'; ?>
            <!-- LEFT RIGHT SIDEBAR NAV-->

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <!--bootstrap-->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!--moments and locale-->
    <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <!--date time picker-->
    <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    
    <script type="text/javascript">
        $('#dtCheckOut').bootstrapMaterialDatePicker({ weekStart : 0, format: 'DD-MM-YYYY',time: false });
        $('#dtCheckIn').bootstrapMaterialDatePicker({ weekStart : 0, format: 'DD-MM-YYYY',time: false });
    </script>
</body>
<script type="text/javascript">
<?php

if ($_POST['hId'] > 0) {
	echo '$("#modal1").openModal();';
}
?>                                
</script>
</html>