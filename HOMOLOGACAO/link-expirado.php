<?php
include("conn/conn.php");
include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'");
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
    </head>
    <header>
        <style>
            footer.page-footer {
                margin-top: 20px;
                padding-top: 20px;
                background-color: #fbc02d;
            }
            
            footer {
                padding-left: 0px;
            }
        </style>
    </header>

    <body>
        <main>
            <div class="row">
                <div class="col l2 s12">
                    <br>
                    <img class="left" src="http://sistemarsvp.com.br/assets/img/logo-horizontal.png">
                </div>
                <div class="col l10 s12">
                    <div class="container">
                        <h1 class="login-form-text2">Ops!</h1>
                        <p class="login-form-text1">Parece que você já aceitou esse convite ou o link expirou.
                        </p>
                    </div>
                </div>
            </div>
        </main>
        <!-- END MAIN -->
        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l12 s12">
                        <h5 class="white-text">Sistema R.S.V.P.</h5>
                        <small><i>Av. Chucri Zaidan, 1550, Cj. 2507
Chácara Sto Antônio - 04583-110 - São Paulo - SP<br>+55 11 5051-2020
</i></small>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2016 Todos os direitos revervados
                    <a class="grey-text text-lighten-4 right">AHCME</a>
                </div>
            </div>
        </footer>
    </body>

    </html>