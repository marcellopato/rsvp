<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

if ($_SESSION[$_idTarefa] > 0 && $_SESSION[$_idTarefa] != 10) {
	header('location:login.php');
}
	

include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

$ql = "\r\n";
$ql2 = "<br>";
$ql3 = "'";

if (isset($_POST['adic'])) {
	$sql = 'insert into perguntas (idEvento, txtPergunta, tipoResposta) values (' . $_SESSION[$_evento] . ', "' . $_POST['txtPergunta'] . '", ' . $_POST['group1'] . ')';
//	echo $sql;
	mysqli_query($conexao,$sql) or die(mysqli_error());
	
	
//	header('location:index.php');
	
}



if (isset($_POST['action'])) {
	$sql = 'update eventos_tarefas set indConclusao = '.($_POST['indConclusao'] == 'on' ? 1 : 0).', porcConclusao = ' . ($_POST['indConclusao'] == 'on' ? 100 : 0) . ', txtDescritivo = "'.$texto.'" WHERE idEvento_tarefa = ' . $_POST['hId'];
	//echo $sql;
	mysqli_query($conexao,$sql) or die(mysqli_error());
	header('location:index.php');
}


if ($_POST['hIdTarefa'] > 0) {

	$sql = 'delete from perguntas WHERE idPergunta = ' . $_POST['hIdTarefa'];
	//echo $sql;
	mysqli_query($conexao,$sql) or die(mysqli_error());
	

	//atualizaPorcConclusao($_SESSION[$_evento],6);
	
	//header('location:index.php');
	
}

$sql = 'SELECT t.*,et.* FROM eventos_tarefas et join tarefas t on et.idTarefa = t.idTarefa WHERE et.idEvento = ' . $_SESSION[$_evento] . ' and t.idTarefa = 10';
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);



?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php include 'head.php'; ?>
<script type="text/javascript">

function deletar(idTarefa) {
	$('#hIdTarefa').val(idTarefa);
	document.form1.submit();
}

</script>

</head>
<body>
    <?php include 'header.php';?>
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <?php include 'navbar.php';?>
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <a href="index.php">Dashboard</a> > Gerador de questionario
                    <h2 class="login-form-text2">
                        <?php echo $qe['nome']; ?>
                    </h2>
                    <form method="post" id="form1" name="form1">
                        <input type="hidden" name="hIdTarefa" id="hIdTarefa" value="0">
                        <input type="hidden" name="hId" id="hId" value="<?php echo $qt['idEvento_tarefa']; ?>">
                        <div class="row">
                            <div class="col l12 s12 grey lighten-5" id="left">
                                <h3 class="login-form-text">Perguntas </h3>
                                <div class="row">
                                    <div class="input-field col l4 s12">
                                        <input id="txtPergunta" name="txtPergunta" type="text" class="validate">
                                        <label for="txtPergunta">Pergunta</label>
                                    </div>
                                    <div class="col l2">
                                        <input name="group1" type="radio" id="test1" value="1" />
                                        <label for="test1">Sim/Não</label>
                                    </div>
                                    <div class="col l2">
                                        <input name="group1" type="radio" id="test2" value="2" />
                                        <label for="test2">Dissertativa</label>
                                    </div>
                                    <div class="col l2">
                                        <input name="group1" type="radio" id="test3" value="3" />
                                        <label for="test3">Nível de Satisfação</label>
                                    </div> 
                                    <div class="col l2">  
		                                <button class="btn waves-effect waves-light" type="submit" name="adic">
                                        	adicionar
                                        </button>
                                    </div>                                 
                                </div>
                            </div>
                        </div>
                            <br><br>
                            <div class="divider"></div>
                            <br><br>
                            <div class="row">
                                
                            <?php
                                $sql = 'SELECT * from perguntas where idEvento = ' . $_SESSION[$_evento];
                                $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                                $nroPergunta = 0;
                                while ($qe = mysqli_fetch_assoc($qr)) {
                                    $nroPergunta += 1;

                                    echo '
                                    <ul class="collection">
                                    <li class="collection-item">
                                    <div class="col l1">' . $nroPergunta . ')</div>
                                    <div class="col l7">' . $qe['txtPergunta'] . '</div>
                                    <div class="col l2">';
                                        if ($qe['tipoResposta'] == 1) echo 'Sim/Não';
                                        if ($qe['tipoResposta'] == 2) echo 'Dissertativa';
                                        if ($qe['tipoResposta'] == 3) echo 'Nível';
                            echo '</div>
                                    <div class="col l2">
                                        <button class="btn waves-effect waves-light" type="button" name="delete'.$qe['idPergunta'].'" onClick="javascript:deletar('.$qe['idPergunta'].');">Deletar<i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                    </li>
                                    </ul>';
	                            }
                                 ?>
                            
                        </div> 
                            <p>
                                <input type="checkbox" id="indConclusao" name="indConclusao" checked />
                                <label for="indConclusao">Tarefa está completa?</label>
                            </p>
                            <div class="container">
                                <button class="btn waves-effect waves-light" type="submit" name="action">Cadastrar perguntas
                                        <i class="material-icons right">send</i>
                                </button>
                            </div>
                    </form>
                    <!--end container-->
            </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
            <?php //include 'sidebar.php'; ?>
            <!-- LEFT RIGHT SIDEBAR NAV-->
            </div>
            <!-- END WRAPPER -->
            </div>
            <!-- END MAIN -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START FOOTER -->
            <footer class="page-footer">
                <div class="footer-copyright">
                    <div class="container">
                        Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a>                        Todos os reservados.
                        <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
                    </div>
                </div>
            </footer>
            <!-- END FOOTER -->
            <!-- ================================================
            Scripts
            ================================================ -->
            <!-- jQuery Library -->
            <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
            <!--materialize js-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
            <!--scrollbar-->
            <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
            <!--plugins.js - Some Specific JS codes for Plugin Settings-->
            <script type="text/javascript" src="js/plugins.min.js"></script>
</body>
</html>