<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'"); 

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
include 'head.php';
?>

</head>

<body>
<?php
include 'header.php';
?>
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
<?php
include 'navbar.php';
?>		

            <!-- START CONTENT -->
<section id="content">
        <!--start container-->
        <div class="container">
          <div class="section">
            <div id="full-calendar">              
              <div class="row">
                <div class="col s12 m4 l3">
                  <div id='events'>    
                    <small>Eventos disponíveis</small>
<?php
$listras = array('cyan','teal','cyan darken-1','cyan accent-4','teal accent-4','light-blue accent-3','light-blue accent-4','teal','teal','teal');
$sql = 'SELECT e.*,u.nome as nomeUsuario, s.descricao FROM eventos e join usuarios u on e.idUsuario = u.idUsuario join statusEvento s on e.idStatusEvento = s.idStatusEvento where 1=1 ';
$sql .= ' order by dtEvento asc limit 20';
//echo $sql_usuario;
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$contador = 0;
$parametros = "";
$primeiro = strtotime("2020-01-01 00:00:00");
while ($qt = mysqli_fetch_assoc($qr)) {
	$id = $qt['idEvento'];	
	$nome = $qt['nome'];	
	$evento = strtotime($qt['dtEvento']);
	$hoje = strtotime(date('Y').'-'.date('m').'-'.date('d'));
	//echo $evento.'-'.$hoje.', ';
	if ($evento > $hoje) {
		echo '<div class="eventos '.$listras[$contador].'">'. $nome .'</div>';
		if ($parametros != "") $parametros .= ",";
		$parametros .= "{ title: '".$nome."', start: '".$qt['dtEvento']."', end: '".$qt['dtFinal']."', color: '".$listras[$contador]."' }";
		//echo '1,';
		if ($evento < $primeiro) {
			//echo '2,';
			$primeiro = $evento;
		}
	}

//($primeiro < '2010-01-01' ? date('Y').'-'.date('m').'-'.date('d') : $primeiro );


	$contador++;
}

//echo 'XXX'.date('Y').'-'.date('m').'-'.date('d').'xxx';
//echo $parametros;
?>
                    <p>
                      <input type='hidden' id='drop-remove' />
                    </p>
                  </div>
                </div>
                <div class="col s12 m8 l9">
                  <div id='calendar'></div>
                </div>
              </div>
            </div>
            </div>
       </div>
        <!--end container-->
      </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
<?php
include 'sidebar.php';
?>

            <!-- LEFT RIGHT SIDEBAR NAV-->

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->




    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
        <!--prism-->
    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>

    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    

    <!-- Calendar Script -->
    <script type="text/javascript" src="js/plugins/fullcalendar/lib/jquery-ui.custom.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/js/fullcalendar.min.js"></script>
    <script type="text/javascript">
    
    
  $(document).ready(function() {
    

    /* initialize the external events
    -----------------------------------------------------------------*/
    $('#external-events .fc-event').each(function() {

      // store data so the calendar knows to render an event upon drop
      $(this).data('event', {
        title: $.trim($(this).text()), // use the element's text as the event title
        stick: true, // maintain when user navigates (see docs on the renderEvent method)
        color: '#00bcd4'
      });

      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });

    });


    /* initialize the calendar
    -----------------------------------------------------------------*/
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      defaultDate: '<?php echo date("Y-m-d h:i:s",$primeiro);  ?>',
      editable: false,
      droppable: false, // this allows things to be dropped onto the calendar
      eventLimit: true, // allow "more" link when too many events
      events: [ <?php echo $parametros; ?>  ]
    });
    
  });
  
    </script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <!-- Toast Notification -->
    </script>
</ul></body>
</html>