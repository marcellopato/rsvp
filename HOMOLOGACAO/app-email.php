<?php
session_start();
include ("conn/conn.php");

if ($_SESSION[$codigo] < 1) {
	header('location:login.php');
}

include ("conn/conn.php");
mysql_query("SET NAMES 'utf8'"); 

?>
<!DOCTYPE html>
<html>

<head>
<?php
include 'head.php';
?>
<script></script>
</head>

<body>
<?php
include 'header.php';
?>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
<?php
include 'navbar.php';
?>
            <!-- END LEFT SIDEBAR NAV-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
<section id="content">
        <!--start container-->
        <div class="container">
<div id="mail-app" class="section">
			
            <div id="modal1" class="modal">
              <div class="">  
                <nav class="red">
                  <div class="nav-wrapper">
                    <div class="left col s12 m5 l5">
                      <ul>
                        <li><a href="#!" class="email-menu"><i class="modal-action modal-close fa fa-arrow-left" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#!" class="email-type">Escreva o novo e-mail</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col s12 m7 l7 hide-on-med-and-down">
                      <ul class="right">
                        <li><a href="#!"><i class="fa fa-paperclip" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#!"><i class="modal-action modal-close  fa fa-paper-plane" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#!"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>
                <?php //Validação dos campos do formulário
					if(isset($_POST['acao']) && $_POST['acao'] == 'enviar'){
						$nome 			= 'usuario';
						//$email			= 'webdantas@gmail.com';
                        $email			= 'marcello@ahcme.com';
						$destinatario	= strip_tags(trim($_POST['destinatario']));
						$assunto 		= strip_tags(trim($_POST['assunto']));
						$mensagem 		= strip_tags(trim($_POST['texto']));
						$status  = '1'; // 1 = ainda não lido, 0 = lido.
						$data	 = date('d').'/'.date('m').'/'.date('Y');
						
						if($nome == '' || $email == '' || $destinatario == '' || $assunto == '' || $mensagem == '' || $status == '' || $data == ''){
								$retorno = '<div class="erro">Preencha os campos corretamente</div>';
							}else{
									$cad_msg = mysql_query("INSERT INTO mensagens (nome, email, destinatario, assunto, mensagem, status, data) VALUES ('$nome','$email','$destinatario','$assunto','$mensagem','$status','$data')");
									//echo $nome, $email, $destinatario, $assunto, $mensagem, $status, $data;
								if($cad_msg){
										$retorno = '<div class="ok">Mensagem enviada com sucesso</div>';
									}
							}
					}
					if(isset($retorno)){echo $retorno;}
				?>
                				
              </div>
              <div class="model-email-content" id="formulario">
                <div class="row">
                  <form class="col s12" action="" method="post" enctype="multipart/form-data">
                  <!-- form class="col s12" --> <!-- form original  -->
                  
                  
                    <!--<div class="row">
                      <div class="input-field col s12">
                        <input id="from_email" type="email" class="validate">
                        <label for="from_email">From</label>
                      </div>
                    </div> -->
                    <div class="row">
                      <div class="input-field col s12">
                        <input id="to_email" type="email" class="validate" name="destinatario">
                        <label for="to_email">Para</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input id="subject" type="text" class="validate" name="assunto">
                        <label for="subject">Assunto</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="compose" class="materialize-textarea" length="500" name="texto"></textarea>
                        <label for="compose">Escreva o e-mail</label>
                      <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                    </div>
                    <input type="hidden" name="acao" value="enviar" />
			        <input type="submit" value="Enviar Mensagem" />
                  </form>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                <nav class="blue light-blue">
                  <div class="nav-wrapper">
                    <div class="left col s12 m5 l5">
                      <ul>
                        <li><a href="?pagina=" class="email-type"><i class="fa fa-inbox" aria-hidden="true"></i>  Entrada</a>
                        </li>
                     </ul>
                    </div>
                    <div class="col s12 m7 l7 hide-on-med-and-down">
<!--                      <ul class="right">
                        <li><a href="#!"><i class="fa fa-archive" aria-hidden="true"></i> Arquivar</a>
                        </li>
                        <li><a href="#!"><i class="fa fa-trash white-text" aria-hidden="true"></i></a> <input type="submit" name="deletar" value="Excluir" /></a>
                        </li>
                        <li><i class="fa fa-trash" aria-hidden="true"></i> <input type="submit" name="imap" value="imap" /></a>
                        </li>
                        <li><a href="#!"><i class="fa fa-envelope" aria-hidden="true"></i> Novo</a>
                        </li>
                      </ul>-->
                    </div>
                  </div>
                </nav>
              </div>
              <div class="col s12">
                <div id="email-sidebar" class="col s1 m1 l1 card-panel">
                  <ul>
                    <li>
                      <a href="#!"><i class="fa fa-archive" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-users" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-user-md" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-car" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-plane" aria-hidden="true"></i></a>
                    </li>                    
                  </ul>
                </div>
                <div id="email-list" class="col s11 m11 l11">
                  <?php
					if(!isset($_GET['pagina']) || $_GET['pagina'] == ''){
						include "admin/paginas/home.php";
					}elseif(isset($_GET['pagina']) && $_GET['pagina'] != ''){
						if(file_exists("admin/paginas/".$_GET['pagina'].'.php')){
						include "admin/paginas/".$_GET['pagina'].'.php';
						}else{
							echo 'Desculpe, você não pode acessar esta página';
						}
					}
				  ?>
                </div>
              </div>
            </div>

            <!-- Compose Email Trigger -->
            <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
              <a class="btn-floating btn-large red modal-trigger" href="#modal1">
                <i class="fa fa-pencil" aria-hidden="true"></i>
              </a>
            </div>



            <!-- movido o conteudo do form + modal daqui -->
        </div>
       </div>
        <!--end container-->
      </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
            <aside id="right-sidebar-nav">
                <ul id="chat-out" class="side-nav rightside-navigation">
                    <li class="li-hover">
                    <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                    <div id="right-search" class="row">
                        <form class="col s12">
                            <div class="input-field">
                                <i class="mdi-action-search prefix"></i>
                                <input id="icon_prefix" type="text" class="validate">
                                <label for="icon_prefix">Search</label>
                            </div>
                        </form>
                    </div>
                    </li>
                    <li class="li-hover">
                        <ul class="chat-collapsible" data-collapsible="expandable">
                        <li>
                            <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                            <div class="collapsible-body recent-activity">
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">just now</a>
                                        <p>Jim Doe Purchased new equipments for zonal office.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Yesterday</a>
                                        <p>Your Next flight for USA will be on 15th August 2015.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Last Week</a>
                                        <p>Jessy Jay open a new store at S.G Road.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                            <div class="collapsible-body sales-repoart">
                                <div class="sales-repoart-list  chat-out-list row">
                                    <div class="col s8">Target Salse</div>
                                    <div class="col s4"><span id="sales-line-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Payment Due</div>
                                    <div class="col s4"><span id="sales-bar-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Delivery</div>
                                    <div class="col s4"><span id="sales-line-2"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Progress</div>
                                    <div class="col s4"><span id="sales-bar-2"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                            <div class="collapsible-body favorite-associates">
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Eileen Sideways</p>
                                        <p class="place">Los Angeles, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Zaham Sindil</p>
                                        <p class="place">San Francisco, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Renov Leongal</p>
                                        <p class="place">Cebu City, Philippines</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Weno Carasbong</p>
                                        <p>Tokyo, Japan</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Nusja Nawancali</p>
                                        <p class="place">Bangkok, Thailand</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        </ul>
                    </li>
                </ul>
            </aside>
            <!-- LEFT RIGHT SIDEBAR NAV-->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->

    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
        <!--prism-->
    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>

    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    

    <!-- Calendar Script -->
    <script type="text/javascript" src="js/plugins/fullcalendar/lib/jquery-ui.custom.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/fullcalendar-script.js"></script>
    <script type="text/javascript" src="admin/paginas/ajax-mail.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>

    <!-- Toast Notification -->
    <script type="text/javascript">
   
    $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
       
  });
    $(window).load(function() {
        setTimeout(function() {
            Materialize.toast('<span>Evento OK. Emitir voucher?</span><a class="btn-flat yellow-text" href="#">Sim<a>', 7000);
        }, 15000);
    });
    </script>
</ul></body>
</html>