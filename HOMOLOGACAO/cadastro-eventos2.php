<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

if ($_SESSION[$_adm] > 1) {
	header("location:login.php");
	exit();
}

//header("Content-Type: text/html; charset=ISO-8859-1");
require_once "Classes/PHPExcel.php";
include ("functions.php");

$erros = '';



/*
DELETE FROM eventos WHERE 1;
delete from eventos_medicos where 1;
delete from eventos_tarefas where 1;
delete from medicos where 1;
delete from usuarios where idNivel > 2;

*/

$idMedico = 0;
if (isset($_POST['btnCadastrar'])) {
	
//echo 	date("Y-m-d",strtotime($_POST['dataEvento'])) . '  ' .date('Y-m-d');
	if (date("Y-m-d",strtotime($_POST['dataEvento'])) >= date('Y-m-d') && $_POST['cboUsuario'] > 0 && strlen($_POST['nomeEvento']) > 2) {
			
		//	echo $_POST['arquivo'];
		//	echo ' f:'.$_FILES['arquivo']['tmp_name'];
		//	echo 'xxxxxxxxxxxxxxxxx'.$_FILES['arquivo'].' - ';
		//	echo $_FILES['arquivo']['tmp_name'].'zzzzzzzzzzzz';
		
		
		// --------------------------------------------------------
		// testar data evento nao pode ser menor que hoje
		// laboratorio tem que ser escolhido ( != 0 )
	
	
	
		if ($_POST['hIdEvento'] > 0) $idEvento = $_POST['hIdEvento'];
		else {
			$sql = 'insert into eventos (idUsuario, nome, dtCriacao, dtEvento, hrEvento, local) values (' . $_POST['cboUsuario'] . ',"' . $_POST['nomeEvento'] . '","' . date("Y-m-d H:i") . '","' . date("Y-m-d",strtotime($_POST['dataEvento'])) . '","' . $_POST['horaEvento'] . '","' . $_POST['enderecoEvento']. '")';
			$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
			$idEvento = mysqli_insert_id($conexao);

			$sql = "update eventos set mapa = '";
			$sql .= $_POST['mapa'] . "' where idEvento = " . $idEvento;
			$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	
			$sql = 'SELECT t.* FROM tarefas t order by t.idTarefa';
			$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
			while ($qt = mysqli_fetch_assoc($qr)) {
					$sql = 'insert into eventos_tarefas (idEvento, idTarefa, ativo) values (' . $idEvento . ',' . $qt['idTarefa'] . ', ' . ($_POST['chkTarefa_' . $qt['idTarefa']] == 'on' ? 1 : 0) . ')';
					$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
			}
		}

/////////////////////// loop da carga ////////////////

include ("excell-carga3.php");
		
////////////////////// loop da carga //////////////


	}
	else {
		echo 'data errada e/ou laboratorio nao escolhido';
	}

}


?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
    </head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
include 'navbar.php';
?>

                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <h2 class="login-form-text2">Cadastro de eventos</h2><a href="https://www.google.com.br/maps/" target="_blank"><img src="assets/img/google-maps.png" class="right"></a>
                                <span id="lblErros">
<?php
			if (strlen($erros) > 0) {
				echo '<br>'.$erros;
			}
			?>
                                
                                </span>
                                <div class="row">
                                    <form class="col s12" name="form1" method="post" enctype="multipart/form-data">
                                        <input type="hidden" id="hIdEvento" name="hIdEvento" value="<?php echo $_POST['hIdEvento']; ?>">
                                        <div class="row">
                                            <div class="input-field col s6">
                                                <select class="browser-default" id="cboUsuario" name="cboUsuario" onChange="form1.submit(); ">
                                                    <?php	
	echo '<option value="0"';
	if ($_POST['cboUsuario'] < 1) echo ' selected';
	echo '>Escolha</option>';

	$sql = 'SELECT * FROM usuarios WHERE idNivel = 2 order by nome';
//	echo $sql;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt = mysqli_fetch_assoc($qr)) {
		echo '<option value="' . $qt['idUsuario'] . '"';
			if ($_POST['cboUsuario'] == $qt['idUsuario']) echo ' selected';
		echo '>' . $qt['nome'] . '</option>'; 	
	}

 ?>
                                                </select>
                                                <label></label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input id="nomeEvento" name="nomeEvento" type="text" class="validate" value="<?php echo $_POST['nomeEvento']; ?>">
                                                <label for="nomeEvento">Nome do Evento</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="input-field col l3 s12">
                                            <label for="dataEvento">Data</label>
                                            <input
    type="text"
    id="dataEvento"
    name="dataEvento"
    placeholder="dd-mm-yyyy"
    onkeyup="
        var v = this.value;
        if (v.match(/^\d{2}$/) !== null) {
            this.value = v + '/';
        } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
            this.value = v + '/';
        }"
    maxlength="10" 
    value="<?php echo date('d/m/Y'); ?>">
                                        </div>
                                        <div class="input-field col l3 s12">
                                            <label for="horaEvento">Hora</label>
                                            <input id="horaEvento" name="horaEvento" type="text" class="validate active" value="<?php echo $_POST['horaEvento']; ?>">
                                        </div>
                                            <div class="input-field col l6 s12">
                                                <input id="enderecoEvento" name="enderecoEvento" type="text" class="validate active" value="<?php echo $_POST['enderecoEvento']; ?>">
                                                <label for="enderecoEvento">Endereço <small><i>copie e cole no google maps</i></small></label>
                                            </div>
                                            <div class="input-field col l12 s12">
                                                <input id="mapa" name="mapa" type="text" class="validate active" value='<?php echo $qe['mapa']; ?>'>
                                                <label for="mapa">Mapa do local <small><i>No Google Maps escolha tamanho personalizado em 1056 x 456</i></small></label>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <br>
                                        <div class="row">
                                            <h2 class="login-form-text2">Tarefas</h2>
                                            <br>
                                            <ul class="collection">
                                                <?php
                                    $sql = 'SELECT t.* FROM tarefas t order by t.idTarefa';
                                //	echo $sql;
                                    $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
									$linhaOld = '';
                                    while ($qt = mysqli_fetch_assoc($qr)) {
                                        echo '<li class="collection-item">
                                            <input type="checkbox" id="chkTarefa_' . $qt['idTarefa'] . '" name="chkTarefa_' . $qt['idTarefa'] . '" checked />
                                            <label for="chkTarefa_' . $qt['idTarefa'] . '">' . $qt['tarefa'] . '</label>
                                            </li>';
                                    }
                                ?>
                                            </ul>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <span class="text-red"><?php echo $erros; ?></span>
                                        <div class="row">
                                            <h2 class="login-form-text2">Lista de Convidados para esse evento</h2>
                                            <div class="col l6 s12">
                                                <input type="file" id="arquivo" name="arquivo" class="dropify" data-allowed-file-extensions="xlsx" data-show-errors="true" data-max-file-size="3M" data-default-file="">
                                            </div>
                                            <button class="btn waves-effect waves-light" type="submit" name="btnCadastrar">cadastrar <i class="material-icons right">send</i> </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->

                        <!-- LEFT RIGHT SIDEBAR NAV-->

                </div>
                <!-- END WRAPPER -->

            </div>
            <!-- END MAIN -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START FOOTER -->
            <footer class="page-footer">
                <div class="footer-copyright">
                    <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
                </div>
            </footer>
            <!-- END FOOTER -->

            <!-- ================================================
    Scripts
    ================================================ -->

            <!-- jQuery Library -->
            <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
            <!--materialize js-->
            <script type="text/javascript" src="js/materialize.min.js"></script>
            <!-- upload -->
            <script type="text/javascript" src="js/dropify.js"></script>
            <!--sortable-->
            <script type="text/javascript" src="js/jquery.sortable.js"></script>
            <!--scrollbar-->
            <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
            <!--plugins.js - Some Specific JS codes for Plugin Settings-->
            <script type="text/javascript" src="js/plugins.min.js"></script>
            <!--nestable -->
            <script type="text/javascript" src="js/plugins/jquery.nestable/jquery.nestable.js"></script>
            <script type="text/javascript" src="js/jquery.mask.min.js"></script>
            <script>
                $(document).ready(function () {
                    Materialize.updateTextFields();
                    $('.dropify').dropify();
                    jQuery(function($){
                        $('#dataEvento').mask('00-00-0000');
                        $("#horaEvento").mask("00:00")
                    });
                });
            </script>
            <script>
                $('.sortable').sortable();
            </script>
            </ul>
    </body>

    </html>