<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

if ($_SESSION[$_idTarefa] > 0 && $_SESSION[$_idTarefa] != 2) {
	header('location:login.php');
}
	

include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

$jScript = '';

if (isset($_POST['fechar'])) {
	header('location:processa2.php');
	
}

function insereVoo($conexao,$idaVolta) {
	$sql = 'insert into voos (idaVolta) values ($idaVolta)';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	return mysqli_insert_id($conexao);
}

function gravaVoo($conexao,$idVoo) {
	$sql = 'update voos set ';
	$sql .= 'eTicket = "' . $_POST['eTicket_'.$idVoo] . '", ';
	$sql .= 'loc = "' . $_POST['loc_'.$idVoo] . '", ';
	$sql .= 'dtVoo = "' . date("Y-m-d",strtotime($_POST['dtVoo_'.$idVoo])) . '", ';
	$sql .= 'ciaAerea = "' . $_POST['ciaAerea_'.$idVoo] . '", ';
	$sql .= 'nroVoo = "' . $_POST['nroVoo_'.$idVoo] . '", ';
	$sql .= 'origemVoo = "' . $_POST['origemVoo_'.$idVoo] . '", ';
	$sql .= 'destinoVoo = "' . $_POST['destinoVoo_'.$idVoo] . '", ';
	$sql .= 'horaSaidaVoo = "' . $_POST['horaSaidaVoo_'.$idVoo] . '", ';
	$sql .= 'horaChegadaVoo = "' . $_POST['horaChegadaVoo_'.$idVoo] . '" ';
	$sql .= ' WHERE idVoo = ' . $idVoo;
//	echo $sql.' ';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	
}

function gravaTudo($conexao) {
	$sql = 'SELECT idVoo FROM voos WHERE idEvento_medico = ' . $_POST['hId'] . ' order by idVoo';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qe = mysqli_fetch_assoc($qr)) {
		gravaVoo($conexao,$qe['idVoo']);

	}
	
}

function imprimeVoo($conexao,$idVoo, $jScript, $adm = false) {

	$sql = 'SELECT * FROM voos WHERE idVoo = ' . $idVoo;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qe = mysqli_fetch_assoc($qr)) {
	
		echo '<div class="row">';
		//echo '<h3 class="login-form-text2">DADOS DA '. ($qe['idaVolta'] == 1 ? 'IDA' : 'VOLTA') . '</h3>';
		if ($adm) echo '<input type="button" class="btn red" id="btnDelVoo' . $idVoo . '" name="btnDelVoo' . $idVoo . '" value="Deletar trecho" onClick="javascript:delVoo(' . $idVoo . ');" >';
		echo '<div class="divider"></div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="eTicket_' . $idVoo . '" name="eTicket_' . $idVoo . '" type="text" class="validate" value="'.$qe['eTicket'].'" onChange="javascript:distribuiVoo(' . $idVoo . ');">';
		echo '<label for="eTicket_' . $idVoo . '">eTicket</label>';
		echo '</div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="loc_' . $idVoo . '" name="loc_' . $idVoo . '" type="text" class="validate" value="'.$qe['loc'].'" >';
		echo '<label for="loc_' . $idVoo . '">Loc</label>';
		echo '</div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="dtVoo_' . $idVoo . '" name="dtVoo_' . $idVoo . '" type="text" class="validate dtVoo" value="'.($qe['dtVoo'] > date('Y-m-d') ? date('d-m-Y',strtotime($qe['dtVoo'])) : date('d-m-Y')).'">';
		echo '<label for="dtVoo_' . $idVoo . '">Data do Vôo</label>';
		echo '</div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="ciaAerea_' . $idVoo . '" name="ciaAerea_' . $idVoo . '" type="text" class="validate" value="'.$qe['ciaAerea'].'">';
		echo '<label for="ciaAerea_' . $idVoo . '">Cia Aérea</label>';
		echo '</div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="nroVoo_' . $idVoo . '" name="nroVoo_' . $idVoo . '" type="text" class="validate" value="'.$qe['nroVoo'].'">';
		echo '<label for="nroVoo_' . $idVoo . '">Nº do Vôo</label>';
		echo '</div>';
		echo '</div>';
		echo '<div class="row">';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="origemVoo_' . $idVoo . '" name="origemVoo_' . $idVoo . '" type="text" class="validate" value="'.$qe['origemVoo'].'">';
		echo '<label for="origemVoo_' . $idVoo . '">Origem do Vôo</label>';
		echo '</div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="destinoVoo_' . $idVoo . '" name="destinoVoo_' . $idVoo . '" type="text" class="validate" value="'.$qe['destinoVoo'].'">';
		echo '<label for="destinoVoo_' . $idVoo . '">Destino do Vôo</label>';
		echo '</div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="horaSaidaVoo_' . $idVoo . '" name="horaSaidaVoo_' . $idVoo . '" type="text" class="validate horaSaidaVoo" value="'.$qe['horaSaidaVoo'].'">';
		echo '<label for="horaSaidaVoo_' . $idVoo . '">Horário de Saída</label>';
		echo '</div>';
		echo '<div class="input-field col l3 s12">';
		echo '<input id="horaChegadaVoo_' . $idVoo . '" name="horaChegadaVoo_' . $idVoo . '" type="text" class="validate horaChegadaVoo" value="'.$qe['horaChegadaVoo'].'">';
		echo '<label for="horaChegadaVoo_' . $idVoo . '">Horário da Chegada</label>';
		echo '</div>';
		echo '</div>';
		echo '<br>';
        }
}

if (isset($_POST['btnAddIda'])) {
	gravaTudo($conexao);
	$sql = 'insert into voos (idEvento_medico, idaVolta) values (' . $_POST['hId'] . ', 1)';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
}

if (isset($_POST['btnAddVolta'])) {
	gravaTudo($conexao);
	$sql = 'insert into voos (idEvento_medico, idaVolta) values (' . $_POST['hId'] . ', 2)';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
}

if ($_POST['hDelVoo'] > 0) {
	gravaTudo($conexao);
	$sql = 'delete from voos where idVoo = ' . $_POST['hDelVoo'];
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
}

if (isset($_POST['action'])) {
//	echo 	$_POST['hId'];
	gravaTudo($conexao);
	atualizaPorcConclusao($conexao,$_SESSION[$_evento],2);
	header('location:processa2.php');
	
}

$sql = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idEvento = ' . $_SESSION[$_evento];
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);


?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>

<script type="text/javascript">

function openModal(id) {
	$('#hId').val(id);
	document.form1.submit();
//	$('#modal1').show();	
}

function delVoo(id) {
	$('#hDelVoo').val(id);
	document.form1.submit();
//	$('#modal1').show();	
}

// x1|2016-11-01|x3|x4|x5|x6|13:15:00|14:10:00
// x2|2016-11-01|x3|x4|x5|x6|13:15:00|14:10:00
// x3|2016-11-01|x3|x4|x5|x6|13:15:00|14:10:00


function distribuiVoo(id) {
	//alert('id:'+id);
	var str = '#eTicket_'+id;
	var eTicket = $('#eTicket_'+id).val();
	var res = eTicket.split("|");
	//alert('res:'+res.length);
	if (res.length > 8) {
		$('#eTicket_'+id).val(res[0]);
		$('#loc_'+id).val(res[1]);
		$('#dtVoo_'+id).val(res[2]);
		//alert($('#ciaAerea_'+id).val());
		$('#ciaAerea_'+id).val(res[3]);
		$('#nroVoo_'+id).val(res[4]);
		$('#origemVoo_'+id).val(res[5]);
		$('#destinoVoo_'+id).val(res[6]);
		$('.horaSaidaVoo_'+id).val(res[7]);
		$('.horaChegadaVoo_'+id).val(res[8]);
	}
	
}



</script>
    </head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
include 'navbar.php';
?>
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <a href="index.php">Dashboard</a> > Aéreo
<?php
$sql = 'SELECT em.*,m.*, (select count(idVoo) from voos where idEvento_medico = em.idEvento_medico ) as conta
FROM eventos_medicos em join medicos m on em.idMedico = m.idMedico WHERE em.idEvento = ' . $_SESSION[$_evento] . ' and em.indAereo = 1';

if ($_SESSION[$_adm] > 1) {
	$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
}

$sql .= ' order by nome';
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$total = mysqli_num_rows($qr);
?>
                                <h2 class="login-form-text2"><?php echo $qt['nome'] . ' ('. $total . ')'; ?></h2>
                                <table class="bordered striped">
                                    <thead>
                                        <tr>
                                            <th data-field="nome">Nome</th>
                                            <th data-field="rsvp">Dados de Vôo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--começa o loop-->
<?php
while ($qe = mysqli_fetch_assoc($qr)) {

?>
                                        <tr>
                                            <td><?php echo $qe['nome']; ?></td>
                                            <td><a class="waves-effect waves-light modal-trigger"  href="javascript:openModal(<?php echo $qe['idEvento_medico']; ?>);"><i class="fa fa-plane <?php echo ($qe['conta'] > 0 ? 'green-text' : 'grey-text'); ?> light" aria-hidden="true"></i></a></td>
                                        </tr>
<?php
}
?>                          
                                        <!--termina o loop-->
                                    </tbody>
                                </table>
<script type="text/javascript">

</script>
    <div id="modal1" class="modal"> <!-- TEM QUE PEGAR O ID DO MÉDICO PRA PASSAR PRO MODAL -->
        <form name="form1" id="form1" method="post" class="col s12">
        <input type="hidden" name="hId" id="hId" value="<?php echo $_POST['hId']; ?>">
        <input type="hidden" name="hDelVoo" id="hDelVoo" value="0">
			<h3 class="login-form-text2">DADOS DA IDA</h3>

        
<?php
	$jScript = '';
	$sql = 'SELECT idVoo FROM voos WHERE idaVolta = 1 and idEvento_medico = ' . $_POST['hId'] . ' order by idVoo';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qe = mysqli_fetch_assoc($qr)) {
		$jScript = imprimeVoo($conexao,$qe['idVoo'], $jScript, ($_SESSION[$_adm] == 1));

	}
if ($_SESSION[$_adm] == 1) {
?>
                    <div class="row">
            	<button class="btn waves-effect waves-light left" type="submit" id="btnAddIda" name="btnAddIda" style="margin-right: 10px;">adicionar trecho
                </button>
            </div>
<?php
}
?>
            <div class="divider"></div>
            <br>
            <h3 class="login-form-text2">DADOS DA VOLTA</h3>

<?php
	$sql = 'SELECT idVoo FROM voos WHERE idaVolta = 2 and idEvento_medico = ' . $_POST['hId'] . ' order by idVoo';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qe = mysqli_fetch_assoc($qr)) {
		$jScript = imprimeVoo($conexao,$qe['idVoo'], $jScript, ($_SESSION[$_adm] == 1));

	}
if ($_SESSION[$_adm] == 1) {
?>

            <div class="row">
	            <button class="btn waves-effect waves-light left" type="submit" id="btnAddVolta" name="btnAddVolta" style="margin-right: 10px;">adicionar trecho
                </button>
			</div>
            <div class="row">
                <button class="btn waves-effect waves-light right" type="submit" name="action" style="margin-right: 10px;">cadastrar
                    <i class="material-icons right">send</i>
                </button>
            </div>
<?php
}
else {
?>
            <div class="row">
                <button class="btn waves-effect waves-light right" type="submit" name="fechar" style="margin-right: 10px;">fechar
                    <i class="material-icons right">send</i>
                </button>
            </div>
<?php
}
?>
        </form>
    </div>
                            </div>
                            <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <?php //include 'sidebar.php'; ?>
                            <!-- LEFT RIGHT SIDEBAR NAV-->

                </div>
                <!-- END WRAPPER -->

            </div>
            <!-- END MAIN -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START FOOTER -->
            <footer class="page-footer">
                <div class="footer-copyright">
                    <div class="container">
                        Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                        <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
                    </div>
                </div>
            </footer>
            <!-- END FOOTER -->


            <!-- ================================================
    Scripts
    ================================================ -->

            <!-- jQuery Library -->
            <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
            <!--materialize js-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
            <!--bootstrap-->
            <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
            <!--moments and locale-->
            <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
            <!--date time picker-->
            <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>
            <!--scrollbar-->
            <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
            <!--plugins.js - Some Specific JS codes for Plugin Settings-->
            <script type="text/javascript" src="js/plugins.min.js"></script>
            <script type="text/javascript" src="js/jquery.mask.min.js"></script>
            <script>
            $(document).ready(function() {
                Materialize.updateTextFields();
                    jQuery(function($){
                        $('#dtConvite').mask('00-00-0000');
                        $("#dtResposta").mask("00-00-0000");
                        $(".dtVoo").mask("00-00-0000");
                        $(".dtVolta").mask("00-00-0000");
                        $(".horaSaidaVoo").mask("00:00");
                        $(".horaChegadaVoo").mask("00:00");
                        $("#horaSaidaVolta").mask("00:00");
                        $("#horaChegadaVolta").mask("00:00");

                    });
            });
</script> 

<?php
echo $jScript;
?>
</script>
    </body>
<script type="text/javascript">
<?php

if ($_POST['hId'] > 0) { 
	echo '$("#modal1").openModal();';
}
?>
</script>

</html>
