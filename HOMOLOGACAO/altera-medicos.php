<?php
include("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
	exit();
}

if ($_SESSION[$_adm] > 1) {
	header("location:login.php");
	exit();
}


mysqli_query($conexao,"SET NAMES 'utf8'");
//echo 'U:'.$_SESSION['idUsuario']; die();

$usuario = $_SESSION[$_codigo];
if ($_SESSION[$_adm] > 4) {
	header("location:index.php");
	exit();
}
    $usuarioaltera = $_GET['id'];
	$sql_medicos = 'SELECT * FROM medicos WHERE idMedico = "' . $_GET['id'] . '"';
	//echo $sql_medicos;
	$qr = mysqli_query($conexao,$sql_medicos) or die(mysqli_error());
	$qt = mysqli_fetch_assoc($qr);


if (isset($_POST['atualizar'])){
    $uploaddir = 'assets/img/';
    $uploadfile = $uploaddir . basename($_FILES['inputFile']['name']);
    move_uploaded_file($_FILES['inputFile']['tmp_name'], $uploadfile);
    $avatar = $uploadfile;
       
    $sql = 'UPDATE medicos SET nome = "'.$_POST['nome'].
	'", endereco = "'.$_POST['endereco'].
    '", email = "'.$_POST['email'].
	'", bairro = "'.$_POST['bairro'].
	'", cidade = "'.$_POST['cidade'].
	'", UF = "'.$_POST['UF'].
	'", cep = "'.$_POST['cep'].
	'", empresa = "'.$_POST['empresa'].
	'", especialidade = "'.$_POST['especialidade'].
	'", crm = "'.$_POST['crm'].
	'", rg = "'.$_POST['rg'].
	'", cpf = "'.$_POST['cpf'].
	'", foto = "'.$avatar.
	'", dtNascimento = "'.date("Y-m-d",strtotime($_POST['dtNascimento'])).
	'", passaporte = "'.$_POST['passaporte'].
	'", nacionalidade = "'.$_POST['nacionalidade'].
	'", validade = "'.date("Y-m-d",strtotime($_POST['validade'])).
	'", telefone = "'.$_POST['telefone'].
	'", celular = "'.$_POST['celular'].
//	'", observacoes = "'.$_POST['observacoes'].
	'"  WHERE idMedico = '.$qt['idMedico'];
                        
    //echo $sql;
    mysqli_query($conexao,$sql) or die(mysqli_error());
    header("location:lista-medicos.php");
    exit();
}


?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
<script type="text/javascript">

function checaCPF(cpf) {
	$("#cpf").css("color","black");
	if (!ValidarCPF(cpf)) $("#cpf").css("color","red");

}
</script>
</head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
                    include 'navbar.php';
                    ?>

                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <a href="index.php">Dashboard</a> > <a href="lista-medicos.php">Lista de Convidados</a> > Atualização de Convidados
                                <form id="form1" name="form1" class="col s12" action="" method="post" enctype="multipart/form-data">
                                    <input type="hidden" value="cadastrar" id="cadastrar" name="cadastrar">
                                    <h2 class="login-form-text2">ATUALIZAÇÃO DE CONVIDADOS</h2>
                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <input id="nome" name="nome" type="text" class="validate active" value="<?php echo $qt['nome'] ?>" required>
                                                <label for="nome">Nome Completo</label>
                                            </div>
                                            <div class="input-field col l6 s12">
                                                <input id="endereco" name="endereco" type="text" class="validate active" value="<?php echo $qt['endereco'] ?>">
                                                <label class="active" for="end">Endereço</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l2 s12">
                                                <input id="bairro" name="bairro" type="text" class="validate active" value="<?php echo $qt['bairro'] ?>">
                                                <label class="active" for="bairro">Bairro</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="cidade" name="cidade" type="text" class="validate active" value="<?php echo $qt['cidade'] ?>">
                                                <label class="active" for="cidade">Cidade</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="UF" name="UF" type="text" class="validate active" value="<?php echo $qt['UF'] ?>">
                                                <label class="active" for="UF">UF</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="cep" name="cep" type="text" class="validate active" value="<?php echo $qt['cep'] ?>">
                                                <label class="active" for="cep">CEP</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="empresa" name="empresa" type="text" class="validate active" value="<?php echo $qt['empresa'] ?>">
                                                <label class="active" for="empresa">Empresa</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="especialidade" name="especialidade" type="text" class="validate active" value="<?php echo $qt['especialidade'] ?>">
                                                <label class="active" for="especialidade">Especialidade</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l6 s12">
                                                <input id="email" name="email" type="text" class="validate active" value="<?php echo $qt['email'] ?>" required>
                                                <label class="active" for="email">Email</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="crm" name="crm" type="text" class="validate active" value="<?php echo $qt['crm'] ?>">
                                                <label class="active" for="crm">CRM</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="rg" name="rg" type="text" class="validate active" value="<?php echo $qt['rg'] ?>">
                                                <label class="active" for="rg">RG</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="cpf" name="cpf" type="text" onKeyPress="MascaraCPF(form1.cpf);" onBlur="checaCPF(form1.cpf);" class="validate active" value="<?php echo $qt['cpf'] ?>">
                                                <label class="active" for="cpf">CPF</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col l2 s12">
                                        <input id="dtNascimento" name="dtNascimento" type="text" class="validate active" value="<?php echo ($qt['dtNascimento'] < '1800-01-01' ? date('d-m-Y') : date("d-m-Y", strtotime($qt['dtNascimento']))); ?>">
                                                <label class="active" for="dtNascimento">Data de Nascimento</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="passaporte" name="passaporte" type="text" class="validate active" value="<?php echo $qt['passaporte'] ?>">
                                                <label class="active" for="passaporte">Passaporte</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                        <input id="validade" name="validade" type="text" class="validate active" value="<?php echo ($qt['validade'] > date('Y-m-d') ? date("d-m-Y", strtotime($qt['validade'])) : date('d-m-Y')); ?>">
                                                <label class="active" for="validade">Validade</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="nacionalidade" name="nacionalidade" type="text" class="validate active" value="<?php echo $qt['nacionalidade'] ?>">
                                                <label class="active" for="nacionalidade">Nacionalidade</label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="telefone" name="telefone" type="text" class="validate tagit-input ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" value="<?php echo $qt['telefone'] ?>">
                                                <label class="active" for="telefone">Telefone <small><i>separe por virgulas</i></small></label>
                                            </div>
                                            <div class="input-field col l2 s12">
                                                <input id="celular" name="celular" type="text" class="validate tagit-input ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" value="<?php echo $qt['celular'] ?>">
                                                <label class="active" for="celular">Celular <small><i>separe por virgulas</i></small></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col l6 s12">
                                                <input type="file" id="inputFile" name="inputFile" class="dropify" data-allowed-file-extensions="png jpg jpeg gif bmp" data-show-errors="true" data-max-file-size="3M" data-default-file="<?php echo $qt['foto'] ?>">
                                            </div>
                                            <div class="input-field col  l6 s12">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="divider"></div>
                                        <br>
               <table class="bordered highlight responsive-table">
                <thead>
                  <tr>
                      <th data-field="id">Evento</th>
                      <th data-field="name">Pergunta</th>
                      <th data-field="price">Resposta</th>
                      <th data-field="price">Data</th>
                  </tr>
                </thead>
                <tbody>

<?php

	$sql = 'SELECT p.*, e.nome, r.resposta, r.dtResposta from perguntas p join eventos e on p.idEvento = e.idEvento join respostasMedicos r on p.idPergunta = r.idPergunta where idMedico = ' . $_GET['id'] . ' order by idEvento, idPergunta';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qe = mysqli_fetch_assoc($qr)) {
		echo '<tr><td>' . $qe['nome'] . '</td><td>' . $qe['txtPergunta'] . '</td><td>' . $qe['resposta'] . '</td><td>' . date("d-m-Y H:i", strtotime($qe['dtResposta'])) . '</td></tr>';
		
	}



?>                                       
				</tbody>
                </table>
                                        <div class="divider"></div>
                                        <br>
                                        <button class="btn waves-effect waves-light" type="submit" name="atualizar" id="atualizar">atualizar
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                                <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <?php include('sidebar.php');?>
                            <!-- LEFT RIGHT SIDEBAR NAV-->
                            </div>
                            <!-- END WRAPPER -->
                </div>
                <!-- END MAIN -->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
                <!-- START FOOTER -->
                <footer class="page-footer">
                    <div class="footer-copyright">
                        <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
                    </div>
                </footer>
                <!-- END FOOTER -->
                <!-- ================================================
    Scripts
    ================================================ -->
                <script src="https://code.jquery.com/jquery-1.11.2.js"></script>
                <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                <!--materialize js-->
                <script type="text/javascript" src="js/materialize.min.js"></script>
        <!-- upload -->
        <script type="text/javascript" src="js/dropify.js"></script>
        <!--moments and locale-->
        <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
        <!--date time picker-->
        <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>
                <!--scrollbar-->
                <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
                <!--plugins.js - Some Specific JS codes for Plugin Settings-->
                <script type="text/javascript" src="js/plugins.min.js"></script>
                <!-- Toast Notification -->
                <script src="ckeditor/ckeditor.js"></script>
                <script type="text/javascript" src="js/jquery.mask.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        Materialize.updateTextFields();
                        $('.dropify').dropify();
                        jQuery(function($){
                            $('#dtNascimento').mask('00-00-0000');
                            $("#validade").mask("00-00-0000")
                        });
                    });
                </script>
                <script>
                    CKEDITOR.replace('observacoes', {
                        extraPlugins: 'imageuploader'
                    });
                </script>
<script type="text/javascript" src="js/funcoes.js"></script> 
    </body>

    </html>