﻿<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="msapplication-tap-highlight" content="no">
	<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
	<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
	<title>404 - Página não encontrada</title>

	<!-- Compiled and minified CSS -->
	
	<link rel="stylesheet" href="assets/css/style.min.css">
	<link rel="stylesheet" href="assets/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/estilo.css">
	<!-- Opera Speed Dial Favicon -->
	<link rel="icon" type="image/png" href1="favicon.png" />
				
	<!-- Standard Favicon -->
	<link rel="icon" type="image/x-icon" href="favicon.ico" />

	<!-- For iPhone 4 Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="favicon.png">

	<!-- For iPad: -->
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="favicon.png">

	<!-- For iPhone: -->
	<link rel="apple-touch-icon-precomposed" href="favicon.png">
	<!-- Compiled and minified JavaScript -->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
<style>
html,
body {
    height: 100%;
}
html {
    display: table;
    margin: auto;
}
body {
    display: table-cell;
    vertical-align: middle;
}
</style>  
</head>

<body class="yellow">
  <div id="login-page" class="row" style="width: 350px;">
    <div class="col s12 z-depth-4 card-panel">
        <div class="row"><br>
          <div class="col s12 center">
            <a href="javascript:history.go(-1);"><img src="assets/img/logo-ganbatte.jpg" alt="" class="responsive-img valign profile-image-login"></a>
            <p class="center login-form-text">ganbatte</p>
          </div>
        </div>
        <div class="row margin">
		<div class="center erro">404</div></br>
		<h3 class="center login-form-text">Página não encontrada!</h3>
        </div>
        </div>
  </div>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php 
	if (isset($_GET['erro']) && ($_GET['erro'] = '1')) {
		?> <script>$( "#login-page" ).effect( "shake" );</script>
	<?php
}
?>
  </body>
</html>