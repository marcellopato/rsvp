<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'"); 

if (isset($_POST['btnNovo'])) {
	header('location:cadastro-medicos.php');
}
	



?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php
include 'head.php';
?>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main">
<!-- START WRAPPER -->
<div class="wrapper">
	<?php
include 'navbar.php';
?>
	<!-- START CONTENT -->
	<section id="content">
	<!--start container-->
	<div class="container">
	<form id="form1" name="form1" class="col s12" method="post" action="lista-medicos.php">
		<div class="row">
          <div class="col l6 s12">
           <h2 class="login-form-text2">Médicos cadastrados</h2>
            </div>
           <div class="row">
            <div class="input-field input-field col l4 s6">
              <input id="txtBusca" name="txtBusca" type="text" value="<?php echo $_POST['txtBusca']; ?>">
              <label for="txtBusca"><i class="material-icons">search</i></label>
            </div>
            <div class="col l2 s6">
                <a class="waves-effect waves-light btn" style="margin-top: 20px;"  onClick="envia();">buscar</a>
            </div>
            </div>
        </div>
             <!--<ul class="pagination">
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="waves-effect"><a href="#!">2</a></li>
                <li class="waves-effect"><a href="#!">3</a></li>
                <li class="waves-effect"><a href="#!">4</a></li>
                <li class="waves-effect"><a href="#!">5</a></li>
                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
              </ul><br>-->
              <div class="divider"></div>
              <br>
              <button class="btn waves-effect waves-light" id="btnNovo" name="btnNovo" type="submit" name="action">Cadastrar novo médico
                <i class="material-icons right">send</i>
              </button>
               <table class="bordered highlight responsive-table">
                <thead>
                  <tr>
                      <th data-field="id">Nome</th>
                      <th data-field="name">CRM</th>
                      <th data-field="price">CPF</th>
                      <th data-field="price">RG</th>
                  </tr>
                </thead>
                <tbody>
            <?php	
            $sql = 'SELECT * FROM medicos where 1=1 ';
			if (strlen($_POST['txtBusca']) > 2) {
				$busca = '"%' . $_POST['txtBusca'] . '%"';
				$sql .= ' and (nome like ' . $busca . ' or crm like ' . $busca . ' or cpf like ' . $busca . ' or rg like ' . $busca . ')';
			}
			$sql .= ' order by nome ';
//			echo $sql;
            $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
            while ($qt = mysqli_fetch_assoc($qr)) {
                echo '<tr><td><a href="altera-medicos.php?id=' . $qt['idMedico'] . '">' . $qt['nome'] . '</a></td><td>' . $qt['crm'] . '</td><td>' . $qt['cpf'] . '</td><td>' . $qt['rg'] . '</td><td></tr>';
            }
         ?>
                </tbody>
              </table>
              <br>
              <button class="btn waves-effect waves-light" id="btnNovo" name="btnNovo" type="submit" name="action">Cadastrar novo médico
                <i class="material-icons right">send</i>
              </button>
        </form>
	</div>
			<!--end container-->
			</section>
			<!-- END CONTENT --> 
			<!-- //////////////////////////////////////////////////////////////////////////// --> 
			<!-- START RIGHT SIDEBAR NAV-->
			<aside id="right-sidebar-nav">
			<ul id="chat-out" class="side-nav rightside-navigation">
			<li class="li-hover"> <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
				<div id="right-search" class="row">
			<form class="col s12">
				<div class="input-field"> <i class="mdi-action-search prefix"></i>
					<input id="icon_prefix" type="text" class="validate">
					<label for="icon_prefix">Search</label>
				</div>
			</form>
		</div>
		</li>

		</ul>
		</aside>
		<!-- LEFT RIGHT SIDEBAR NAV--> 
	</div>
	<!-- END WRAPPER --> 
</div>
<!-- END MAIN --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START FOOTER -->
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
	</div>
</footer>
<!-- END FOOTER --> 
<!-- ================================================
    Scripts
    ================================================ --> 
<!-- jQuery Library --> 
<script type="text/javascript" src="../js/plugins/jquery-1.11.2.min.js"></script> 
<!--materialize js--> 
<script type="text/javascript" src="../js/materialize.min.js"></script> 
<!-- upload --> 
<script type="text/javascript" src="../js/dropify.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="../js/plugins.min.js"></script> 
<script>
  $(document).ready(function() {
    Materialize.updateTextFields();
  });
  
function novo() {
	getElementById('btnCadastrar').value = "Cadastrar";
}

function envia() {
	var x = document.getElementById('txtBusca').value;
//	alert (x);
//	if (x.length > 2) 
form1.submit();	
}
</script>
<?php if (isset($_GET['cadastro']) && ($_GET['cadastro'] == '1')) { ?>
	<script>
		var $toastContent = $('<span>Cadastro atualizado com SUCESSO!</span>');
		Materialize.toast($toastContent, 5000);
</script>
	<?php } ?>
</body>
</html>
