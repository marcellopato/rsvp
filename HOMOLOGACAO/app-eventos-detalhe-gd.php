<?php
session_start();
include ("conn/conn.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 
//echo $_SESSION[$adm];
$usuario = $_SESSION[$codigo];
if (!isset($_SESSION[$adm])) {
	header("location:login.php");
	exit();
}
if ($_SESSION[$adm] == '') {
	header("location:login.php");
	exit();
}
if ($_SESSION[$adm] < 1) {
	header("location:login.php");
	exit();
}
if ($_SESSION[$adm] > 4) {
	header("location:dashboard-gr.php");
	exit();
}

//echo $usuario;
	$sql_usuario = 'SELECT us.*,ni.txtNivel FROM usuarios us JOIN nivels ni on ni.idNivel = us.idNivel WHERE ((us.idUsuario = "' . $usuario . '"))';
	//echo $sql_usuario;
	$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
	$qt = mysqli_fetch_assoc($qr);
	
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
        <?php
        include 'head.php';
        ?>
</head>
<body>
        <?php
include 'header.php';
?>
<!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- START MAIN -->
<div id="main"> 
  <!-- START WRAPPER -->
  <div class="wrapper">
  
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details yellow darken-2">
                <div class="row">
                    <div class="col col s8 m8 l8">
                        <span class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn">Monteiro Lobato</span>
                        <p class="user-roal">Gerente Regional</p>
                    </div>
                </div>
                </li>
                </ul>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
  
  <!-- //////////////////////////////////////////////////////////////////////////// --> 
  
  <!-- START CONTENT -->
  <section id="content"> 
    <!--start container-->
    <div class="container"> 
      <!--card widgets start--> 
      <!--<div id="card-widgets"> -->
      <div class="row">
        <div class="col s12 m12 l4">
          <div id="flight-card" class="card">
            <div class="card-header amber darken-2">
              <div class="card-title">
                <h4 class="flight-card-title">Aéreo</h4>
                <p class="flight-card-date">21 de abr, Qui 04:50</p>
              </div>
            </div>
            <div class="card-content-bg white-text">
              <div class="card-content">
                <div class="row flight-state-wrapper">
                  <div class="col s5 m5 l5 center-align">
                    <div class="flight-state">
                      <h4 class="margin">BRL</h4>
                      <p class="ultra-small">Brasil</p>
                    </div>
                  </div>
                  <div class="col s2 m2 l2 center-align"> <i class="fa fa-plane fa-3x" aria-hidden="true"></i> </div>
                  <div class="col s5 m5 l5 center-align">
                    <div class="flight-state">
                      <h4 class="margin">LAX</h4>
                      <p class="ultra-small">Los Angeles</p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col s6 m6 l6 center-align">
                    <div class="flight-info">
                      <p class="small"><span class="grey-text text-lighten-4">Decolagem:</span> 04.50</p>
                      <p class="small"><span class="grey-text text-lighten-4">Vôo:</span> IB 5786</p>
                      <p class="small"><span class="grey-text text-lighten-4">Terminal:</span> B</p>
                    </div>
                  </div>
                  <div class="col s6 m6 l6 center-align flight-state-two">
                    <div class="flight-info">
                      <p class="small"><span class="grey-text text-lighten-4">Chegada:</span> 08.50</p>
                      <p class="small"><span class="grey-text text-lighten-4">Vôo:</span> IB 5786</p>
                      <p class="small"><span class="grey-text text-lighten-4">Terminal:</span> C</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 m12 l4">
          <div class="blog-card"  style="margin-top: 7px;">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light"> <img src="assets/img/hotel.jpg" alt="blog-img"> </div>
              <ul class="card-action-buttons">
                <li><a class="btn-floating waves-effect waves-light light-blue"><i class="fa fa-info activator" aria-hidden="true"></i></a> </li>
                <li><a class="btn-floating waves-effect waves-light green accent-4"><i class="fa fa-envelope" aria-hidden="true"></i></a> </li>
              </ul>
              <div class="card-content">
                <p class="row"> <span class="left"><a href="http://www.yatra.com/hotels/hotels-in-bangalore/ibis-bengaluru-city-centre-hotel">Ibis</a></span> <span class="right">21 de abr, Sex 6:20</span> </p>
                <h4 class="card-title grey-text text-darken-4"><a href="#" class="grey-text text-darken-4">Hotel Ibis</a> </h4>
                <div class="row">
                  <div class="col s12"> +1 213 555-2016 </div>
                </div>
              </div>
              <div class="card-reveal"> <span class="card-title grey-text text-darken-4"><i class="fa fa-times right" aria-hidden="true"></i> Facilidades</span>
                <p>Wi-Fi, Café da Manhã, piscina, sauna com massagista.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 m12 l4">
          <div class="map-card">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light"> <img src="assets/img/mapa.jpg" alt=""> </div>
              <div class="card-content"> <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right"> <i class="fa fa-info activator" aria-hidden="true"></i> </a>
                <h4 class="card-title grey-text text-darken-4"><a href="#" class="grey-text text-darken-4">Salão de Convenções do Hotel</a> </h4>
                <p class="blog-post-content">Saiba mais.</p>
              </div>
              <div class="card-reveal"> <span class="card-title grey-text text-darken-4">Salão de Convenções do Hotel <i class="fa fa-times right" aria-hidden="true"></i></span>
                <p>Here is some more information about this company. As a creative studio we believe no client is too big nor too small to work with us to obtain good advantage.By combining the creativity of artists with the precision of engineers we develop custom solutions that achieve results.Some more information about this company.</p>
                <p><i class="fa fa-user" aria-hidden="true"></i> Manager Name</p>
                <p><i class="fa fa-building" aria-hidden="true"></i> 125, ABC Street, Los Angeles, USA</p>
                <p><i class="fa fa-phone-square" aria-hidden="true"></i> +1 (612) 222 8989</p>
                <p><i class="fa fa-envelope" aria-hidden="true"></i> support@gibis.com</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--card widgets end--> 
      
      <!-- //////////////////////////////////////////////////////////////////////////// --> 
      
      <!--work collections start-->
      <div id="work-collections">
        <div class="row">
          <div class="col s12 m12 l12">
          <ul id="projects-collection" class="collection">
            <li class="collection-item avatar"> <i class="fa fa-plane circle light-blue darken-2" aria-hidden="true"></i> <span class="collection-title">RSVP</span>
              <p>Enviado ontem, 20h</p>
              <!-- <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>--> 
            </li>
          </ul>
          <form>
            <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
              <li>
                <div class="collapsible-header ">
                  <div class="row">
                    <div class="col s6">
                      <p class="collections-title"><a href="#!" class="black-text">Dr. Paulo da Silva</a></p>
                      <p class="collections-content">CRM 12345</p>
                    </div>
                    <div class="col s6"> <span class="task-cat green lighten-1">Confirmado</span> - <span class="task-cat blue lighten-1">PAX 2</span><br>
                      Seu GD: <a href="mailto:fernando@paesleme.com.br">Monteiro Lobato</a> checou essa lista em 10 - abr, às 22h </div>
                  </div>
                </div>
                <div class="collapsible-body">
                  <div class="container">
                    <div class="row">
                      <div class="col l6 s12">
                        <h3>Residência</h3>
                        <span id="nome1">Rua Balthazar da Veiga, 273 - apto 113 </span><br>
                          <span id="CEP">CEP: 0450-000 </span><br>
                          <span id="fone">(11) 5555-9876 - (11) 91234-5678 </span>
                      </div>
                      <div class="col l6 s12">
                        <h3>Clínica</h3>
                        <span id="nome2">Avenida Paulista, 1444 - CJ 14 - 1º andar <i class="fa fa-pencil-square" aria-hidden="true"></i></span><br>
                        <span id="CEP2">CEP 01310-916 <i class="fa fa-pencil-square" aria-hidden="true"></i></span><br>
                        <span id="fone2">(11) 5555-9876 - (11) 91234-5678 <i class="fa fa-pencil-square" aria-hidden="true"></i></span><br>
                      </div>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" id="atualizar" name="action">atualizar <i class="material-icons right">send</i> </button>
                  </div><br>
<br>
                  <div class="divider"></div>
                  <div class="container">
                    <h5>Histórico</h5>
                    <table class="bordered responsive-table">
                      <thead>
                        <tr>
                          <th data-field="id">Dia - Hora</th>
                          <th data-field="name">Histórico</th>
                          <th data-field="price">Aúdio</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>14 - abr, 10h</td>
                          <td>Atendeu e confirmou</td>
                          <td><audio controls></audio></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </li>
              <li>
                <div class="collapsible-header">
                  <div class="row">
                    <div class="col s6">
                      <p class="collections-title"><a href="#!" class="black-text">Dr. Spock</a></p>
                      <p class="collections-content">CRM 159753</p>
                    </div>
                    <div class="col s6"> <span class="task-cat orange">Não confirmado</span><br>
                      Seu GD: <a href="mailto:fernando@paesleme.com.br">Monteiro Lobato</a> checou essa lista em 10 - abr, às 22h </div>
                  </div>
                </div>
              <div class="collapsible-body">
                <div class="row">
                <div class="container">
                  <div class="col l6 s12">
                    <h3>Residência</h3>
                    <p>Rua Balthazar da Veiga, 273 - apto 113<br>
                      CEP: 0450-000<br>
                      (11) 5555-9876 - (11) 91234-5678 
                  </div>
                  <div class="col l6 s12">
                    <h3>Clínica</h3>
                    <p>Avenida Paulista, 1444 - CJ 14 - 1º andar<br>
                      CEP 01310-916<br>
                      (11) 5555-9876 - (11) 91234-5678 
                  </div>
                  </div>
                </div>
                <div class="divider"></div>
                <div class="container">
                  <h5>Histórico</h5>
                  <table class="bordered responsive-table">
                    <thead>
                      <tr>
                        <th data-field="id">Dia - Hora</th>
                        <th data-field="name">Histórico</th>
                        <th data-field="price">Aúdio</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>14 - abr, 11h</td>
                        <td>Secretária atendeu e ficou de ligar em uma hora</td>
                        <td><audio controls></audio></td>
                      </tr>
                      <tr>
                        <td>14 - abr, 15h</td>
                        <td>Retornamos a ligação, a secretária atendeu e passou o recado. Ele ficou de entrar no site.</td>
                        <td><audio controls></audio></td>
                      </tr>
                      <tr>
                        <td>14 - abr, 18h</td>
                        <td>Secretária atendeu e garantiu que ele vai entrar no site de sua residência</td>
                        <td><audio controls></audio></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              </li>
            </ul>
          </form>
        </div>
      </div>
    </div>
    <!--                    <div class="container">
                      <button class="btn waves-effect waves-light disabled" type="submit" name="action">Emitir Voucher
                        <i class="material-icons right">send</i>
                      </button>
                    
                    </div>--> 
    <!--work collections end--> 
    
    <!-- Floating Action Button --> 
    <!--                   <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                        <a class="btn-floating btn-large" href="#topo">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </a>
                        <ul>
                          <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                          <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                          <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                          <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
                        </ul>
                    </div>--> 
    <!-- Floating Action Button -->
    
    </div>
    <!--end container--> 
  </section>
  <!-- END CONTENT --> 
</div>
<!-- END WRAPPER -->
</div>
<!-- END MAIN --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START FOOTER -->
<footer class="page-footer">
  <div class="footer-copyright">
    <div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
  </div>
</footer>
<!-- END FOOTER --> 
<!-- ================================================
    Scripts
    ================================================ --> 

<!-- jQuery Library --> 
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script> 
<!--materialize js--> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 

<!-- chartist --> 
<script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script> 

<!-- chartjs --> 
<script type="text/javascript" src="js/plugins/chartjs/chart.min.js"></script> 
<script type="text/javascript" src="js/plugins/chartjs/chart-script.js"></script> 

<!-- sparkline --> 
<script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script> 
<script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script> 

<!-- google map api --> 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAZnaZBXLqNBRXjd-82km_NO7GUItyKek"></script>

<!--jvectormap--> 
<script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> 
<script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 
<script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script> 

<!-- altera campos --> 
<script type="text/javascript" src="js/funcoes.js"></script> 

<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="js/plugins.min.js"></script> 

</body>
</html>