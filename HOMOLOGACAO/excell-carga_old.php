﻿<?php


function acerta_data($minhaData) {
	$minhaData = str_replace('/','-',$minhaData);
	return (strlen($minhaData) < 1 ? '1500-01-01' : date('Y-m-d', strtotime($minhaData)));
}

$aborta = false;

$linhaComando = '';
$row = 1;
if (file_exists($_FILES['arquivo']['tmp_name']))  {
    $excelReader = PHPExcel_IOFactory::createReaderForFile($_FILES['arquivo']['tmp_name']);
    $excelReader->setReadDataOnly();
    $excelReader->setLoadAllSheets();
    $excelObj = $excelReader->load($_FILES['arquivo']['tmp_name']);
    $lastRow = $excelObj->getActiveSheet()->getHighestRow();

    $linha = 1;
    while (++$linha <= $lastRow) {
        if (strlen(pegaCell($excelObj, 5,$linha)) < 3) {
            if (strlen(pegaCell($excelObj, 0,$linha)) < 3) {
            }
            else {
                $erros .= 'falta cpf:'.pegaCell($excelObj, 0,$linha).'<br>';	
            }
        }
        else {
            $sql = 'SELECT idMedico FROM medicos WHERE cpf = "' . cpfLimpo(pegaCell($excelObj, 5,$linha)) . '"';
            //echo '<br>'.$sql;
            $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
            $idMedico = 0;
            while ($qt = mysqli_fetch_assoc($qr)) {
                $idMedico = $qt['idMedico'];
            }
            //echo '<br>m:'.$idMedico;
            
            
            
            // se nao existe, cadastra e pega o id
            if ($idMedico == 0) {
                //cadastra o medico
                $sql = 'insert into medicos (nome, empresa, especialidade, crm, rg, cpf, dtNascimento, passaporte, validade, nacionalidade, telefone, celular, email, endereco, bairro, cep, cidade, uf) values ("' . (pegaCell($excelObj, 0,$linha)) . '","' . pegaCell($excelObj, 1,$linha) . '","' . pegaCell($excelObj, 2,$linha) . '","' . pegaCell($excelObj, 3,$linha) . '","' . pegaCell($excelObj, 4,$linha) . '","' . cpfLimpo(pegaCell($excelObj, 5,$linha)) . '","' . acerta_data(pegaCell($excelObj, 6,$linha)) . '","' . pegaCell($excelObj, 7,$linha) . '","' . date('Y-m-d', strtotime(pegaCell($excelObj, 8,$linha))) . '","' . pegaCell($excelObj, 9,$linha) . '","' . pegaCell($excelObj, 10,$linha) . '","' . pegaCell($excelObj, 11,$linha) . '","' . pegaCell($excelObj, 12,$linha) . '","' . pegaCell($excelObj, 13,$linha) . '","' . pegaCell($excelObj, 14,$linha) . '","' . pegaCell($excelObj, 15,$linha) . '","' . pegaCell($excelObj, 16,$linha) . '","' . pegaCell($excelObj, 17,$linha) . '")';
                $qr = mysqli_query($conexao,$sql) or die(mysqli_error());
                $idMedico = mysqli_insert_id($conexao);
            }

            if ($idMedico == 0) {
                $erros .= 'nao foi possivel cadastrar o medico: '.pegaCell($excelObj, 0,$linha).', '.pegaCell($excelObj, 5,$linha).', '.pegaCell($excelObj, 12,$linha).'<br>';
            }

            
            
            // acha o repr
            $idRep = 0;
            $idGD = 0;
            $idGR = 0;
            $idRep = achaUsuario($conexao,pegaCell($excelObj, 18,$linha), 5);
            if ($idRep == 0) {
                if (strlen(pegaCell($excelObj, 19,$linha)) > 2) {
                    $idGD = achaUsuario($conexao,pegaCell($excelObj, 19,$linha), 4);
                    if ($idGD == 0) {
                        $idGR = achaUsuario($conexao,pegaCell($excelObj, 20,$linha), 3);
                        if ($idGR == 0) {
                            $idGR = criaUsuario($conexao,pegaCell($excelObj, 20,$linha), 3, $_POST['cboUsuario']);
                        }
                        $idGD = criaUsuario($conexao,pegaCell($excelObj, 19,$linha), 4, $idGR);
                    }
                }
                else {
                    $idGR = achaUsuario($conexao,pegaCell($excelObj, 20,$linha), 3);
                    if ($idGR == 0) {
                        $idGR = criaUsuario($conexao,pegaCell($excelObj, 20,$linha), 3, $_POST['cboUsuario']);
                    }
                }
                $idRep = criaUsuario($conexao,pegaCell($excelObj, 18,$linha), 5, (strlen(pegaCell($excelObj, 19,$linha)) > 2 ? $idGD : $idGR));
            }

            if ($idRep == 0) {						
                $erros .= 'repr nao cadastrado:'.pegaCell($excelObj, 18,$linha).'<br> ';
            }

            
            if ($idRep > 0 && $idMedico > 0) {
                $linhaComando = 'select count(*) as achado from eventos_medicos where idEvento = ' . $idEvento . ' and idMedico = ' . $idMedico;	
                $qc = mysqli_query($conexao,$linhaComando) or die(mysqli_error());
                $achou = 0;
                while ($qt = mysqli_fetch_assoc($qc)) {
                    $achou = $qt['achado'];
                }
                
                if ($achou == 0) {						
                    $arvoreGen = montaFamilia($conexao,$idRep);
                    
                    $linhaComando = 'insert into eventos_medicos (idEvento, idMedico, idUsuarioNivel5, idUsuarioNivel4, idUsuarioNivel3, idUsuarioNivel2, idStatusConvite, indAereo, indTransfer, indHospedagem, indInscricao) values (' . $idEvento . ',' . $idMedico . ',' . $arvoreGen[5] . ',' . $arvoreGen[4] . ',' . $arvoreGen[3] .',' . $_POST['cboUsuario'] .',0, '.(strtoupper(pegaCell($excelObj, 22,$linha)) == 'SIM' ? 1 : 0).', '.(strtoupper(pegaCell($excelObj, 23,$linha)) == 'SIM' ? 1 : 0).', '.(strtoupper(pegaCell($excelObj, 24,$linha)) == 'SIM' ? 1 : 0).', '.(strtoupper(pegaCell($excelObj, 25,$linha)) == 'SIM' ? 1 : 0).'); ';	
                    $qc = mysqli_query($conexao,$linhaComando) or die(mysqli_error());
                }
            }



///////////////////////////

        }
    }
    //fclose ($handle);
    //echo '<br>'.$erros;
    //echo '<br>'.$linhaComando;
    //die();
    if (strlen($erros) > 0) {
        //echo '<br>'.$erros;
        $aborta = true;
    }
    else {
        //$qr = mysqli_query($conexao,$linhaComando) or die(mysqli_error());
        //header('location:lista-eventos.php');
    }
}

/*
DELETE FROM eventos WHERE 1;
delete from eventos_medicos where 1;
delete from eventos_tarefas where 1;
delete from medicos where 1;
delete from usuarios where idNivel > 2;
*/
?>