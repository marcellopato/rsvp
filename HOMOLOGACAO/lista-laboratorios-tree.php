<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

mysqli_query($conexao,"SET NAMES 'utf8'"); 

if (isset($_POST['btnNovo'])) {
//	echo $_POST['hUsuarioPai']; die();
	$_SESSION['idUsuario'] = 0; //$_POST['hUsuarioPai'];
//	header('location:cadastro-usuarios.php');
}
	
if (isset($_POST['action'])){
//    $uploaddir = 'assets/img/';
//    $uploadfile = $uploaddir . basename($_FILES['input-file-now']['name']);
//    move_uploaded_file($_FILES['input-file-now']['tmp_name'], $uploadfile);
//    $avatar = $uploadfile;
       
    $nome = $_POST['nome'];
    $login = $_POST['login'];
    $senha = $_POST['senha'];
    $nivel = $_POST['cboNivelAtual'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $celular = $_POST['celular'];
    $endereco = $_POST['endereco'];
    $ativo = 1; //$_POST['group1'];
//    $avatar;            
//    $insere_usuario = 'UPDATE usuarios SET nome = "' . $nome . '", foto = "' . $avatar . '", telefone = "' . $tel . '", celular = "' . $cel . '", endereco = "' . $rua . '", ativo = "' . $ativo . '", email = "' . $email . '" WHERE usuarios.idUsuario ='.$qt['idUsuario'];
    $insere_usuario = 'UPDATE usuarios SET login = "' . $login . '", senha = "' . $senha . '", telefone = "' . $telefone . '", celular = "' . $celular . '", endereco = "' . $endereco . '", ativo = "' . $ativo . '", email = "' . $email . '" WHERE usuarios.idUsuario ='.$_POST['hUsuarioPai'];
                        
//    echo $insere_usuario;
    mysqli_query($conexao,$insere_usuario) or die(mysqli_error());
    header("location:lista-laboratorios-tree.php");
    exit();
}


	
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php
include 'head.php';
?>
</head>

<body>
<?php
include 'header.php';
?>
<!-- START MAIN -->
<div id="main"> 
	<!-- START WRAPPER -->
	<div class="wrapper">
		<?php
include 'navbar.php';
?>
		
		<!-- START CONTENT -->
		<section id="content"> 
			<!--start container-->
			<div class="container">
			<form id="form1" name="form1" class="col s12" action="lista-laboratorios-tree.php" method="post">
				<h2 class="login-form-text2">Lista de funcionários do laboratorio</h2>
<?php
$idUsuarioPaiSelecionado = 0;
if ($_POST['hUsuarioPai'] > 0) {
	$sql = 'SELECT u.*,n.txtNivel FROM usuarios u join nivels n on u.idNivel = n.idNivel WHERE u.idNivel > 1 and u.idUsuario = ' . $_POST['hUsuarioPai'];
//	echo $sql;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	$nivel = 0;
	$idUsuarioPaiSelecionado = 0;
	while ($qt = mysqli_fetch_assoc($qr)) {
		echo '<div class="chip" style="cursor:pointer;" id="txtUsuarioPai" onClick="sobe(' . $qt['idUsuarioPai'] . ');">' . $qt['txtNivel'] . ' - ' . $qt['nome'] . '&nbsp;<i class="fa fa-level-up" aria-hidden="true"></i></div><br><br>'; 	
		$nivel = $qt['idNivel'];
		$idUsuarioPaiSelecionado = $qt['idUsuario'];
	}
}

?>

					<input type="hidden" id="hUsuarioPai" name="hUsuarioPai" value="<?php echo $idUsuarioPaiSelecionado; ?>">
				<div class="row">
					<div class="col l8 s12">
					<select class="browser-default" id="cboUsuario" name="cboUsuario"<?php if ($nivel < 5) echo ' onChange="troca(this.value); "'; ?>>
						<?php	
//	$idUsuarioPaiSelecionado = $qt['idUsuarioPai'];						
	if ($_POST['cboUsuario'] > 0) {
		$idUsuarioPaiSelecionado = $_POST['cboUsuario'];
	}

	echo '<option value="0" selected>Selecione</option>';

	$sql = 'SELECT u.*,n.txtNivel FROM usuarios u join nivels n on u.idNivel = n.idNivel WHERE ';
	if ($idUsuarioPaiSelecionado == 0) {
		$sql .= 'u.idNivel = 2 order by nome';
	}
	else {
		$sql .= 'u.idNivel > 2 and  u.idUsuarioPai = ' . $idUsuarioPaiSelecionado . ' order by nome';
	}


//	echo 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'.$sql.'xxxxxx';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($qt = mysqli_fetch_assoc($qr)) {
		echo '<option value="' . $qt['idUsuario'] . '">' . $qt['txtNivel'] . ' - ' . $qt['nome'] . '</option>'; 	
	}
	

 ?>
					</select>
					</div>
					<div class="col l4 s12">
					<!-- <button class="btn waves-effect waves-light right" id="btnNovo" name="btnNovo" type="submit">Novo usuário
                        <i class="material-icons right">send</i>
                      </button> -->
					</div>
				</div>
<?php
$usuario = $idUsuarioPaiSelecionado; //$_SESSION[$codigo];
$sql_usuario = 'SELECT us.*,ni.txtNivel FROM usuarios us JOIN nivels ni on ni.idNivel = us.idNivel WHERE ((us.idUsuario = "' . $usuario . '"))';
//echo $sql_usuario;
$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
$qu = mysqli_fetch_assoc($qr);

if ($idUsuarioPaiSelecionado > 0 && $nivel > 2) {
?>
                
				<h2 class="login-form-text2">Dados do funcionário</h2>
				<div class="row">
					<div class="row">
						<div class="input-field col s12">
							<input id="nome" name="nome" type="text" class="validate" required value="<?php echo $qu['nome']; ?>">
							<label for="nome">Nome Completo</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col l6 s12">
							<input id="login" name="login" type="text" class="validate" required value="<?php echo $qu['login']; ?>">
							<label class="active" for="login">Nome de Usuário</label>
						</div>
						<div class="input-field col l6 s12">
							<input id="senha" name="senha" type="password" class="validate" required value="<?php echo $qu['senha']; ?>">
							<label class="active" for="senha">Senha do Usuário</label>
						</div>
						<div class="input-field col l6 s12">
							<input id="endereco" name="endereco" type="text" class="validate active" required value="<?php echo $qu['endereco']; ?>">
							<label class="active" for="endereco">Endereço</label>
						</div>
						<div class="input-field col l6 s12">
							<input id="email" name="email" type="email" class="validate active" required value="<?php echo $qu['email']; ?>">
							<label class="active" for="email">Email</label>
						</div>
					</div>
					<div class="row">
						<!-- <div class="col l6 s12">
                            <input type="file" id="input-file-now" class="dropify"  data-allowed-file-extensions="png jpg jpeg gif bmp" data-show-errors="true" data-max-file-size="3M" data-default-file="http://sistemarsvp.com.br/assets/img/anonymous.gif">
                        </div> -->
						</div>
						<div class="input-field col l6 s12">
						<?php	
//					<select class="browser-default" id="cboNivelAtual" name="cboNivelAtual">
/*	$nivelAtual = isset($_POST['cboNivelAtual']) ? $_POST['cboNivelAtual'] : $qt['idNivel'];

	$sql = 'SELECT * FROM nivels WHERE idNivel > ' . $qu['idNivel'] . ' order by idNivel';
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	while ($q2 = mysqli_fetch_assoc($qr)) {
		echo '<option value="' . $q2['idNivel'] . '"';
		if ($nivelAtual == $q2['idNivel']) echo ' selected';
		echo '>' . $q2['txtNivel'] . '</option>'; 	
	}*/
	
//					</select>

//							<label>Nível de permissões</label>
 ?>
							<br>
						</div>
					</div>
					<div class="row">
						<div class="input-field col l6 s12">
							<input id="telefone" name="telefone" type="text" class="validate" required value="<?php echo $qu['telefone']; ?>">
							<label class="active" for="telefone">Telefone <small><i>separe por virgulas</i></small></label>
						</div>
						<div class="input-field col l6 s12">
							<input id="celular" name="celular" type="text" class="validate" required value="<?php echo $qu['celular']; ?>">
							<label class="active" for="celular">Celular <small><i>separe por virgulas</i></small></label>
						</div>
						<button class="btn waves-effect waves-light pink" type="submit" name="action">cadastrar <i class="material-icons right">send</i> </button>
					</div>
				</div>
<?php
}
?>
			</form>
			<!--end container--> 
		</section>
		<!-- END CONTENT --> 
		<!-- //////////////////////////////////////////////////////////////////////////// --> 
		<!-- START RIGHT SIDEBAR NAV-->
		<aside id="right-sidebar-nav">

		</aside>
		<!-- LEFT RIGHT SIDEBAR NAV--> 
	</div>
	<!-- END WRAPPER --> 
</div>
<!-- END MAIN --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START FOOTER -->
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container"> Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados. <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span> </div>
	</div>
</footer>
<!-- END FOOTER --> 
<!-- ================================================
    Scripts
    ================================================ --> 
<!-- jQuery Library --> 
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script> 
<!--materialize js--> 
<script type="text/javascript" src="js/materialize.min.js"></script> 
<!-- upload --> 
<script type="text/javascript" src="js/dropify.js"></script> 
<!--scrollbar--> 
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!--plugins.js - Some Specific JS codes for Plugin Settings--> 
<script type="text/javascript" src="js/plugins.min.js"></script> 
<!-- Toast Notification --> 
<script>
$(document).ready(function() {
  Materialize.updateTextFields();
  $('.dropify').dropify();
});    
function troca(id) {
	if (id > 0) {
		document.getElementById("hUsuarioPai").value = id; 
		form1.submit();
        
	}
}

function sobe(id) {
	document.getElementById("hUsuarioPai").value = id; 
	document.getElementById("cboUsuario").selected = id; 
	form1.submit();
}

</script>


<?php if (isset($_GET['cadastro']) && ($_GET['cadastro'] == '1')) { ?>
	<script>
		var $toastContent = $('<span>Cadastro atualizado com SUCESSO!</span>');
		Materialize.toast($toastContent, 5000);
</script>
	<?php } ?>
</body>
</html>
