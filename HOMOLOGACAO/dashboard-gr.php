<?php
include ("conn/conn.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 
//echo $_SESSION[$adm];
$usuario = $_SESSION[$codigo];
if (!isset($_SESSION[$adm])) {
	header("location:login.php");
	exit();
}
if ($_SESSION[$adm] == '') {
	header("location:login.php");
	exit();
}
if ($_SESSION[$adm] < 1) {
	header("location:login.php");
	exit();
}
if ($_SESSION[$adm] > 4) {
	header("location:dashboard-gr.php");
	exit();
}

//echo $usuario;
	$sql_usuario = 'SELECT us.*,ni.txtNivel FROM usuarios us JOIN nivels ni on ni.idNivel = us.idNivel WHERE ((us.idUsuario = "' . $usuario . '"))';
	//echo $sql_usuario;
	$qr = mysqli_query($conexao,$sql_usuario) or die(mysqli_error());
	$qt = mysqli_fetch_assoc($qr);
	
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
        <?php
        include 'head.php';
        ?>
</head>
<body>
        <?php
include 'header.php';
?>

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details yellow darken-2">
                <div class="row">
                    <div class="col col s8 m8 l8">
                        <span class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn">Monteiro Lobato</span>
                        <p class="user-roal">Gerente Regional</p>
                    </div>
                </div>
                </li>
                </ul>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--work collections start-->
                    <div id="work-collections">
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <ul id="projects-collection" class="collection">
                                    <li class="collection-item avatar">
                                        <i class="fa fa-folder circle light-blue darken-2" aria-hidden="true"></i>
                                        <span class="collection-header">Eventos</span>
                                        <p>Em produção</p>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <a href="app-eventos-detalhe-gd.php"><p class="collections-title">Convenção de Cardiologia</p></a>
                                                <p class="collections-content">TEVA</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat green lighten-1">Completo</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">2º Encontro de Ginecologistas</p>
                                                <p class="collections-content">TEVA</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat blue lighten-1">90%</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Lançamento PROVASIC</p>
                                                <p class="collections-content">TEVA</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat orange">30%</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Palestra de Neurologia</p>
                                                <p class="collections-content">TEVA</p>
                                            </div>
                                            <div class="col s6">
                                                <span class="task-cat red yellow-text">10%</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                </div>
                <!--end container-->
            </section>
        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->




    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START FOOTER -->
    <footer class="page-footer">
<!--        <div class="container">
            <div class="row section">
                <div class="col l12 s12">
                    <h5 class="white-text">Aonde estamos!</h5>
                    <p class="grey-text text-lighten-4">Eventos sendo realizados agora</p>
                    <div id="world-map-markers"></div>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Sales by Country</h5>
                    <p class="grey-text text-lighten-4">A sample polar chart to show sales by country.</p>
                    <div id="polar-chart-holder">
                        <canvas id="polar-chart-country" width="200"></canvas>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    

    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>   

    <!-- chartjs -->
    <script type="text/javascript" src="js/plugins/chartjs/chart.min.js"></script>
    <script type="text/javascript" src="js/plugins/chartjs/chart-script.js"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script>

    <!--jvectormap-->
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script>
    

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>


</body>

</html>