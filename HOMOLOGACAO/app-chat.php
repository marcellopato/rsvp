<?php include_once "conn/conn.php";?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Apresentação / ganbatte - versão 1</title>

    <!-- Compiled and minified CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.min.css">
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/style.min.css">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/fullcalendar/css/fullcalendar.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <script type="text/javascript" src="js/jquery.js"></script> <!-- Utilizando o JS local -->


</head>

<body>
    <!-- Start Page Loading -->
<!--    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>-->
    <!-- End Page Loading -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="index.html" class="brand-logo darken-1"><img src="assets/img/logo-horizontal.png" alt="ganbatte"></a> <span class="logo-text">ganbatte</span></h1></li>
                    </ul>
                       <ul class="right hide-on-med-and-down black-text">
<!--                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button"  data-activates="translation-dropdown"><img src="images/flag-icons/United-States.png" alt="USA" /></a>
                        </li>
-->                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen black-text"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button black-text" data-activates="notifications-dropdown"><i class="fa fa-bell-o" aria-hidden="true"><small class="notification-badge">5</small></i>
                        </a>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse black-text"><i class="fa fa-comments-o" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <!-- translation-button -->
<!--                    <ul id="translation-dropdown" class="dropdown-content black-text">
                      <li>
                        <a href="#!"><img src="images/flag-icons/United-States.png" alt="English" />  <span class="language-select">English</span></a>
                      </li>
                      <li>
                        <a href="#!"><img src="images/flag-icons/France.png" alt="French" />  <span class="language-select">French</span></a>
                      </li>
                      <li>
                        <a href="#!"><img src="images/flag-icons/China.png" alt="Chinese" />  <span class="language-select">Chinese</span></a>
                      </li>
                      <li>
                        <a href="#!"><img src="images/flag-icons/Germany.png" alt="German" />  <span class="language-select">German</span></a>
                      </li>
                      
                    </ul>-->
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>AVISOS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="fa fa-check" aria-hidden="true"></i> CHECK-IN completo</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 horas atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-certificate" aria-hidden="true"></i> Evento OK.</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 horas atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-cog" aria-hidden="true"></i> Sistema atualizado</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 dias atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Reunião de Pauta</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 horas atrás</time>
                      </li>
                      <li>
                        <a href="#!"><i class="fa fa-line-chart" aria-hidden="true"></i> Relatório gerado</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 semana atrás</time>
                      </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details yellow darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="assets/img/patricia.png" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Perfil</a>
                            </li>
                            <li><a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> Confs</a>
                            </li>
                            <li><a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i> Ajuda</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="login-gr.html"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                            </li>
                        </ul>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">Patrícia Penteado<i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrador</p>
                    </div>
                </div>
                </li>
                <li class="bold "><a href="dashboard-ganbatte.html" class="waves-effect waves-cyan"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a>
                </li>
                <li class="bold active"><a href="app-email.html" class="waves-effect waves-cyan disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Email <span class="new badge">4</span></a>
                </li>
                
                <li class="bold "><a href="app-calendar.html" class="waves-effect waves-cyan"><i class="fa fa-calendar" aria-hidden="true"></i> Calendário</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="fa fa-archive" aria-hidden="true"></i> Cadastro Geral</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="Cadastro-de-Eventos" style="color:#444;">Eventos</a>
                                    </li>
                                    <li><a href="Cadastro-de-Usuarios">Usuários</a>
                                    </li>
                                    <li><a href="#!">Médicos</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        </ul>
                        <ul>
                <li class="li-hover"><div class="divider"></div></li>
            </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
<section id="content">
        <!--start container-->
        <div class="container">
<div id="mail-app" class="section">
			
            <!-- movido o conteudo para cá -->
            <!-- Compose Email Structure -->
            <!--div id="modal1" class="modal">
              <div class="modal-content"-->
            <div id="modal1" class="">
              <div class="">  
                <nav class="red">
                  <div class="nav-wrapper">
                    <div class="left col s12 m5 l5">
                      <ul>
                        <li><a href="#!" class="email-menu"><i class="modal-action modal-close fa fa-arrow-left" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#!" class="email-type">Escreva o novo e-mail</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col s12 m7 l7 hide-on-med-and-down">
                      <ul class="right">
                        <li><a href="#!"><i class="fa fa-paperclip" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#!"><i class="modal-action modal-close  fa fa-paper-plane" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#!"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>
                <?php //Validação dos campos do formulário
					if(isset($_POST['acao']) && $_POST['acao'] == 'enviar'){
						$nome 			= 'usuario';
						$email			= 'webdantas@gmail.com';
						$destinatario	= strip_tags(trim($_POST['destinatario']));
						$assunto 		= strip_tags(trim($_POST['assunto']));
						$mensagem 		= strip_tags(trim($_POST['texto']));
						$status  = '1'; // 1 = ainda não lido, 0 = lido.
						$data	 = date('d').'/'.date('m').'/'.date('Y');
						
						if($nome == '' || $email == '' || $destinatario == '' || $assunto == '' || $mensagem == '' || $status == '' || $data == ''){
								$retorno = '<div class="erro">Preencha os campos corretamente</div>';
							}else{
									$cad_msg = mysql_query("INSERT INTO mensagens (nome, email, destinatario, assunto, mensagem, status, data) VALUES ('$nome','$email','$destinatario','$assunto','$mensagem','$status','$data')");
									//echo $nome, $email, $destinatario, $assunto, $mensagem, $status, $data;
								if($cad_msg){
										$retorno = '<div class="ok">Mensagem enviada com sucesso</div>';
									}
							}
					}
					if(isset($retorno)){echo $retorno;}
				?>
                				
              </div>
              <div class="model-email-content" id="formulario">
                <div class="row">
                  <form class="col s12" action="" method="post" enctype="multipart/form-data">
                  <!-- form class="col s12" --> <!-- form original  -->
                  
                  
                    <!--<div class="row">
                      <div class="input-field col s12">
                        <input id="from_email" type="email" class="validate">
                        <label for="from_email">From</label>
                      </div>
                    </div> -->
                    <div class="row">
                      <div class="input-field col s12">
                        <input id="to_email" type="email" class="validate" name="destinatario">
                        <label for="to_email">Para</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input id="subject" type="text" class="validate" name="assunto">
                        <label for="subject">Assunto</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="compose" class="materialize-textarea" length="500" name="texto"></textarea>
                        <label for="compose">Escreva o e-mail</label>
                      <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                    </div>
                    <input type="hidden" name="acao" value="enviar" />
			        <input type="submit" value="Enviar Mensagem" />
                  </form>
                </div>
              </div>
            </div>
            <!--final -->
            


            <div class="row">
              <div class="col s12">
                <nav class="blue light-blue">
                  <div class="nav-wrapper">
                    <div class="left col s12 m5 l5">
                      <ul>
<!--                        <li><a href="#!" class="email-menu"><i class="mdi-navigation-menu"></i></a>
                        </li>-->
                        <li><a href="#!" class="email-type"><i class="fa fa-inbox" aria-hidden="true"></i>  Entrada</a>
                        </li>
                     </ul>
                    </div>
                    <div class="col s12 m7 l7 hide-on-med-and-down">
                      <ul class="right">
                        <li><a href="#!"><i class="fa fa-archive" aria-hidden="true"></i> Arquivar</a>
                        </li>
                        <li><i class="fa fa-trash" aria-hidden="true"></i> <input type="submit" name="deletar" value="Excluir" /></a>
                        </li>
                        <li><a href="#!"><i class="fa fa-envelope" aria-hidden="true"></i> Novo</a>
                        </li>
<!--                        <li><a href="#!"><i class="mdi-navigation-more-vert"></i></a>
                        </li>-->
                      </ul>
                    </div>

                  </div>
                </nav>
              </div>
              <div class="col s12">
                <div id="email-sidebar" class="col s2 m1 s1 card-panel">
                  <ul>
                    <li>
                      <a href="#!"><i class="fa fa-archive" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-users" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-user-md" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-car" aria-hidden="true"></i></a>
                    </li>
                    <li>
                      <a href="#!"><i class="fa fa-plane" aria-hidden="true"></i></a>
                    </li>                    
                  </ul>
                </div>
                <div id="email-list" class="col s10 m4 l4 card-panel z-depth-1">
                  <?php
					if(!isset($_GET['pagina']) || $_GET['pagina'] == ''){
						include "admin/paginas/home.php";
					}elseif(isset($_GET['pagina']) && $_GET['pagina'] != ''){
						if(file_exists("admin/paginas/".$_GET['pagina'].'.php')){
						include "admin/paginas/".$_GET['pagina'].'.php';
						}else{
							echo 'Desculpe, você não pode acessar esta página';
						}
					}
				  ?>
                  <!--<ul class="collection">
                    <li class="collection-item avatar email-unread selected">
                      <i class="fa fa-users" aria-hidden="true"></i>
                      <span class="email-title">Equipe</span>
                      <p class="truncate grey-text ultra-small">Etapa concluída</p>
                      <a href="#!" class="secondary-content"><span class="new badge blue">4</span></a>
                    </li>
                    <li class="collection-item avatar email-unread">
                      <i class="fa fa-car" aria-hidden="true"></i>
                      <span class="email-title">Transporte</span>
                      <p class="truncate grey-text ultra-small">Olá Patricia, confirmado o nú...</p>
                      <a href="#!" class="secondary-content"><span class="new badge green">6</span></a>
                    </li>
                    <li class="collection-item avatar email-unread">
                      <i class="fa fa-plane" aria-hidden="true"></i>
                      <span class="email-title">RSVP</span>
                      <p class="truncate grey-text ultra-small">RSVP de Oncologia enviado com sucesso</p>
                    </li>
               </ul>-->
                </div>
                <div id="email-details" class="col s12 m7 l7 card-panel">
                  <p class="email-subject truncate">Etapa concluída <span class="email-tag grey lighten-3">inbox</span><i class="fa fa-star yellow-text text-darken-3 right" aria-hidden="true"></i>
                  </p>
                  <hr class="grey-text text-lighten-2">
                  <div class="email-content-wrap">
                    <div class="row">
                      <div class="col s10 m10 l10">
                        <ul class="collection">
                          <li class="collection-item avatar">
                            <img src="assets/img/ana.jpg" alt="" class="circle">
                            <span class="email-title">Ana Gaspar Delgado</span>
                            <p class="truncate grey-text ultra-small">pra mim, Equipe</p>
                            <p class="grey-text ultra-small">Ontem</p>
                          </li>
                        </ul>
                      </div>
                      <div class="col s2 m2 l2 email-actions">
                        <a href="#!"><span><i class="fa fa-reply" aria-hidden="true"></i></span></a>
                        <a href="#!"><span><i class="fa fa-ellipsis-v" aria-hidden="true"></i></span></a>
                      </div>
                    </div>
                    <div class="email-content">
                      <p>Cara equipe,</p>
                      <p>Estamos orgulhosas de saber que contamos com um time tão competente.</p>
                      <p>O evento de cardiologia foi um sucesso. O jantar é por conta da empresa!</p>
                      <p>Viva!
                        <br>Direção</p>
                    </div>
                  </div>
                  <hr>
                  <div class="email-content-wrap">
                    <div class="row">
                      <div class="col s10 m10 l10">
                        <ul class="collection">
                          <li class="collection-item avatar">
                          <img src="assets/img/ana.jpg" alt="" class="circle">
                            <span class="email-title">Ana Gaspar Delgado</span>
                            <p class="truncate grey-text ultra-small">pra mim, Equipe</p>
                            <p class="grey-text ultra-small">Ontem 18:10</p>
                          </li>
                        </ul>
                      </div>
                      <div class="col s2 m2 l2 email-actions">
                        <a href="#!"><span><i class="fa fa-reply" aria-hidden="true"></i></span></a>
                        <a href="#!"><span><i class="fa fa-ellipsis-v" aria-hidden="true"></i></span></a>
                      </div>
                    </div>
                    <div class="email-content">
                      <p>Ah! Já ia me esquecendo,</p>
                      <p>O jantar tem direito a acompanhante!</p>
                      <p>Beijos,
                        <br>Ana</p>
                    </div>
                  </div>
                  <div class="email-reply">
                    <div class="row">
                      <div class="col s4 m4 l4 center-align">
                        <a href="!#"><i class="fa fa-reply" aria-hidden="true"></i></a>
                        <p class="ultra-small">Responder</p>
                      </div>
                      <div class="col s4 m4 l4 center-align">
                        <a href="!#"><i class="fa fa-reply-all" aria-hidden="true"></i></a>
                        <p class="ultra-small">Responder a todos</p>
                      </div>
                      <div class="col s4 m4 l4 center-align">
                        <a href="!#"><i class="fa fa-share" aria-hidden="true"></i></a>
                        <p class="ultra-small">Encaminhar</p>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            <!-- Compose Email Trigger -->
            <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
              <a class="btn-floating btn-large red modal-trigger" href="#modal1">
                <i class="fa fa-pencil" aria-hidden="true"></i>
              </a>
            </div>

            <!-- área de janelas do chat -->
            <aside id="chats">
                <div class="window" id="janela_x">
                    <div class="header_windows"> <!-- cabeçalho da mensagem -->
                        <a href="#" class="close">X</a> <span class="name">CHAT USER 01</span> <span id="5" class="status on"></span>
                    </div>
                    <div class="body">
                        <div class="mensagens">
                            <ul>
                                <li class="eu"><p>Este é um exemplo de mensagem que aparecerá na página</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
    

        </div>
       </div>
        <!--end container-->
      </section>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START RIGHT SIDEBAR NAV-->
            <aside id="users_online"> <!-- início do CHAT -->
                <ul id="chat-out" class="side-nav rightside-navigation">
                    <!--li class="li-hover">
                        <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                        <div id="right-search" class="row">
                            <form class="col s12">
                                <div class="input-field">
                                    <i class="mdi-action-search prefix"></i>
                                    <input id="icon_prefix" type="text" class="validate">
                                    <label for="icon_prefix">Opções</label>
                                </div>
                            </form>
                        </div>
                    </li-->
                    <div class="row">
                    <div class="col l6">
                    <li class="li-hover">
                        <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                        <ul class="chat-collapsible" data-collapsible="expandable">
                        <li id="5">
                            <div class="collapsible-header teal white-text active"><i class="mdi-action-stars"></i>Usuários On line</div>
                            <div class="collapsible-body favorite-associates">
                                <?php for($i=1; $i<=4; $i++):?> <!-- Qtde de repetições visíveis -->
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class=""><img src="images/defaultavatar_small.png" alt="">
                                    </div>
                                    
                                  
                                    
                                    <div class="">
                                        <a href="#" id="3:5" class="comecar"><p>Eduardo Dantas</p></a>
                                        <p class="place">Marketing Ganbatte</p>
                                        <span id="5" class="status on"></span> <!-- Criar CSS para status ON/OFF -->
                                    </div>
                                </div>
                                <?php endfor;?>
                            </div>
                        </li>
                            
                        <li>
                            <div class="collapsible-header red white-text"><i class="mdi-social-whatshot"></i>Usuários Off line</div>
                            <div class="collapsible-body sales-repoart">
                                <?php for($i=1; $i<=4; $i++):?> <!-- Qtde de repetições visíveis -->
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4 imgSmall"><img src="fotos/edu.png" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <a href="#" id="3:5" class="comecar"><p>Eduardo Dantas</p></a>
                                        <p class="place">Marketing Ganbatte</p>
                                        <span id="5" class="status on"></span> <!-- Criar CSS para status ON/OFF -->
                                    </div>
                                </div>
                                <?php endfor;?>
                            </div>
                        </li>
                        
                        
 
                        </ul>
                    </li>
                    </div>
                        <div class="col l6">
                            <ul>
                                <li>
                            <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Conversas </div>
                                <div class="collapsible-body recent-activity">
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Ativas</a>
                                        <p>Eduardo - Olá Patrícia, onde estão.....</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Histórico</a>
                                        <p>Jaqueline - Sim, eu irei até o evento...</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                            </ul>
                        </div>
                    </div>
                </ul>
            </aside>
            <!-- LEFT RIGHT SIDEBAR NAV-->

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->




    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
        <!--prism-->
    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>

    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    

    <!-- Calendar Script -->
    <script type="text/javascript" src="js/plugins/fullcalendar/lib/jquery-ui.custom.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/fullcalendar/fullcalendar-script.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>

    <!-- Toast Notification -->
    <script type="text/javascript">
    // Toast Notification
    $(window).load(function() {
        setTimeout(function() {
            Materialize.toast('<span>Evento OK. Emitir voucher?</span><a class="btn-flat yellow-text" href="#">Sim<a>', 7000);
        }, 15000);
    });
    </script>
</ul></body>
</html>