<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

$idTarefa = $_GET['i'];

if ($_SESSION[$_idTarefa] > 0 && $_SESSION[$_idTarefa] != $idTarefa) {
	header('location:login.php');
}
	

if (isset($_POST['action'])) {
	$sql = 'update eventos_tarefas set indConclusao = "'.($_POST['indConclusao'] == 'on' ? 1 : 0).'", porcConclusao = ' . ($_POST['indConclusao'] == 'on' ? 100 : 0) . ', txtDescritivo = "'.$_POST['txtDescritivo'].'" WHERE idEvento_tarefa = ' . $_POST['hId'];
	//echo $sql;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
	header('location:index.php');
}

$sql = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idEvento = ' . $_SESSION[$_evento];
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qe = mysqli_fetch_assoc($qr);

$sql = 'SELECT t.*,et.* FROM eventos_tarefas et join tarefas t on et.idTarefa = t.idTarefa WHERE et.idEvento = ' . $_SESSION[$_evento] . ' and t.idTarefa = ' . $idTarefa;
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);

?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php
include 'head.php';
?>
            <script type="text/javascript">
                function openModal(id) {
                    $('#hId').val(id);
                    document.form1.submit();
                    //	$('#modal1').show();	
                }
            </script>
    </head>

    <body>
        <?php
include 'header.php';
?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php
include 'navbar.php';
?>
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <a href="index.php">Dashboard</a> >
                                <?php echo $qt['tarefa']; ?>
                                    <h3><?php echo $qe['nome']; ?></h3>
                                    <div class="row">

                                        <form name="form1" id="form1" method="post" class="col s12" action="processaX.php?i=<?php echo $idTarefa; ?>">
                                            <input type="hidden" name="hId" id="hId" value="<?php echo $qt['idEvento_tarefa']; ?>">
                                            <input type="hidden" name="idTarefa" id="idTarefa" value="<?php echo $idTarefa; ?>">
                                            <div class="row">
                                                <h3 class="login-form-text2">Observações:</h3>
                                                <div class="input-field col s12">
                                                    <input id="indConclusao" name="indConclusao" type="checkbox" class="<?php echo ($qt['indConclusao'] > 0 ? " checked " : " "); ?>">
                                                    <label for="indConclusao">Concluido</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <textarea id="txtDescritivo" name="txtDescritivo" class="materialize-textarea">
                                                        <?php echo $qt['txtDescritivo']; ?>
                                                    </textarea>
                                                    <label for="txtDescritivo"></label>
                                                </div>
                                            </div><br>
                                            <div class="row">
                                                <button class="btn waves-effect waves-light right" type="submit" name="action" style="margin-right: 10px;">cadastrar
                                                    <i class="material-icons right">send</i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                            </div>
                            <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <?php include 'sidebar.php'; ?>
                            <!-- LEFT RIGHT SIDEBAR NAV-->

                </div>
                <!-- END WRAPPER -->

            </div>
            <!-- END MAIN -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START FOOTER -->
            <footer class="page-footer">
                <div class="footer-copyright">
                    <div class="container">
                        Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                        <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
                    </div>
                </div>
            </footer>
            <!-- END FOOTER -->


            <!-- ================================================
    Scripts
    ================================================ -->

            <!-- jQuery Library -->
            <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
            <!--materialize js-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
            <!--bootstrap-->
            <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
            <!--moments and locale-->
            <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
            <!--date time picker-->
            <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>
            <!--scrollbar-->
            <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
            <!--plugins.js - Some Specific JS codes for Plugin Settings-->
            <script type="text/javascript" src="js/plugins.min.js"></script>
            <script type="text/javascript">
                tinymce.init({
                    selector: '#txtDescritivo',
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                });
            </script>
        </body>

    </html>