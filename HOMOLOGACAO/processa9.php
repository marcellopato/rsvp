<?php
include ("conn/conn.php");

if ($_SESSION[$_codigo] < 1) {
	header('location:login.php');
}

if ($_SESSION[$_idTarefa] > 0 && $_SESSION[$_idTarefa] != 9) {
	header('location:login.php');
}


include ("functions.php");
mysqli_query($conexao,"SET NAMES 'utf8'"); 

$ql = "\r\n";
$ql2 = "<br>";
$ql3 = "'";

function mostraVoo($qv) {
		$saida = ' <div class="flight-info style="text-align: justify;">';
		$saida .= '   <p><span class=" text-lighten-4">Data:</span> '.date("d-m-Y", strtotime($qv['dtVoo'])).'</p>';
		$saida .= '   <p><span class=" text-lighten-4">Cia Aérea:</span> '.$qv['ciaAerea'].'</p>';
		$saida .= '   <p><span class=" text-lighten-4">Vôo Nº:</span> '.$qv['nroVoo'].'</p>';
		$saida .= '   <p><span class=" text-lighten-4">Origem:</span> '.$qv['origemVoo'].'</p>';
		$saida .= '   <p><span class=" text-lighten-4">Destino:</span> '.$qv['destinoVoo'].'</p>';
		$saida .= '   <p><span class=" text-lighten-4">Saída:</span> '.date("H:i", strtotime($qv['horaSaidaVoo'])).'</p>';
		$saida .= '   <p><span class=" text-lighten-4">Chegada:</span> '.date("H:i", strtotime($qv['horaChegadaVoo'])).'</p>';
		$saida .= '   <p><span class=" text-lighten-4">   </span> ';
		$saida .= ' </div>';
//		echo 'XXXXXXXXXXXXXXXX'.$saida.'xxxxxxxx';
		return $saida;
}

if (isset($_POST['action'])) {
    $texto = str_replace('"', $ql3, $_POST['txtDescritivo']);
	$sql = 'update eventos_tarefas set indConclusao = '.($_POST['indConclusao'] == 'on' ? 1 : 0).', porcConclusao = ' . ($_POST['indConclusao'] == 'on' ? 100 : 0) . ', txtDescritivo = "'.$texto.'" WHERE idEvento_tarefa = ' . $_POST['hId'];
	//echo $sql;
	mysqli_query($conexao,$sql) or die(mysqli_error());
	

	// criando links para os medicos entrarem em seu cadastro
	// envia os convites
	$sql = 'SELECT em.*, e.nome as evento, e.local, e.dtEvento, e.foto, m.nome as medico, m.email FROM eventos e join eventos_medicos em on e.idEvento = em.idEvento join medicos m on em.idMedico = m.idMedico WHERE e.idEvento = ' . $_SESSION[$_evento];
	if ($_POST['chkParticipacao'] == '0') {
		$sql .= ' and qtdCartaCheckOut = 0';
	}
//echo $sql;
	$qr = mysqli_query($conexao,$sql) or die(mysqli_error());

	$tempName = 'ZIP_'.date('Y-m-d_H:i').'.zip';
	$zip = new ZipArchive();
	$res = $zip->open($tempName, ZipArchive::CREATE);
	if ($res === TRUE) {
		while ($qe = mysqli_fetch_assoc($qr)) {
			
			$corpoAereo = '';
			
			if ($qe['indTransfer'] == 1) {
				$corpoTransfer = '';
				$corpoTransfer .= '                  
							  
							  
							  <div class="col s12 m12 l4">
								<div class="card">
								  <div class="card-image">
									<span class="card-title">Traslado</span>
								  </div>
								  <div class="card-content">
									<p>Dados de seu traslado</p><br>';
/*									
			   if (strlen($qe['origemTransfer']) > 1) $corpoTransfer .= '<p><i class="fa fa-home" aria-hidden="true"></i> - <i class="fa fa-plane" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qe['dataTransfer'])).'</p><p>Hora: '.date("H:i", strtotime($qe['horaTransfer'])).'</p><p>Motorista: '.$qe['motorista'].'</p><p>Veículo: '.$qe['txtVeiculo'].'</p><p>de: '.$qe['origemTransfer'].'</p><p>para: '.$qe['destinoTransfer'].'</p>';
			   if (strlen($qe['origemTransfer2']) > 1) $corpoTransfer .= '<p><i class="fa fa-plane" aria-hidden="true"></i> - <i class="fa fa-bed" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qe['dataTransfer2'])).'</p><p>Hora: '.date("H:i", strtotime($qe['horaTransfer2'])).'</p><p>Motorista: '.$qe['motorista2'].'</p><p>Veículo: '.$qe['txtVeiculo2'].'</p><p>de: '.$qe['origemTransfer2'].'</p><p>para: '.$qe['destinoTransfer2'].'</p>';
			   if (strlen($qe['origemTransfer3']) > 1) $corpoTransfer .= '<p > <i class="fa fa-bed" aria-hidden="true"></i> - <i class="fa fa-building" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qe['dataTransfer3'])).'</p><p>Hora: '.date("H:i", strtotime($qe['horaTransfer3'])).'</p><p>Motorista: '.$qe['motorista3'].'</p><p>Veículo: '.$qe['txtVeiculo3'].'</p><p>de: '.$qe['origemTransfer3'].'</p><p>para: '.$qe['destinoTransfer3'].'</p>';
*/
			   if (strlen($qe['origemTransfer4']) > 1) $corpoTransfer .= '<p><i class="fa fa-building" aria-hidden="true"></i> - <i class="fa fa-bed" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qe['dataTransfer4'])).'</p><p>Hora: '.date("H:i", strtotime($qe['horaTransfer4'])).'</p><p>Motorista: '.$qe['motorista4'].'</p><p>Veículo: '.$qe['txtVeiculo4'].'</p><p>de: '.$qe['origemTransfer4'].'</p><p>para: '.$qe['destinoTransfer4'].'</p>';
			   if (strlen($qe['origemTransfer5']) > 1) $corpoTransfer .= '<p > <i class="fa fa-bed" aria-hidden="true"></i> - <i class="fa fa-plane" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qe['dataTransfer5'])).'</p><p>Hora: '.date("H:i", strtotime($qe['horaTransfer5'])).'</p><p>Motorista: '.$qe['motorista5'].'</p><p>Veículo: '.$qe['txtVeiculo5'].'</p><p>de: '.$qe['origemTransfer5'].'</p><p>para: '.$qe['destinoTransfer5'].'</p>';
			   if (strlen($qe['origemTransfer6']) > 1) $corpoTransfer .= '<p><i class="fa fa-plane" aria-hidden="true"></i> - <i class="fa fa-home" aria-hidden="true"></i></p><p>Data: '.date("d-m-Y", strtotime($qe['dataTransfer6'])).'</p><p>Hora: '.date("H:i", strtotime($qe['horaTransfer6'])).'</p><p>Motorista: '.$qe['motorista6'].'</p><p>Veículo: '.$qe['txtVeiculo6'].'</p><p>de: '.$qe['origemTransfer6'].'</p><p>para: '.$qe['destinoTransfer6'].'</p>';
			$corpoTransfer .= '
								  </div>
								</div>
							  </div>';
			}

/////////////////////////////////////////////		
			if ($qe['indAereo'] == 1) {
				$corpoAereo .= '
							  <div class="col s12 m12 l4">
								<div id="" class="card">
								  <div class="">
									<div class="card-content">
									  <div class="">
										<div class="col s5 m5 l5 center-align">
										  <div class="">
											<h2 class="margin">VOLTA</h4>
										  </div>';
				$sql = 'SELECT v.* FROM voos v join eventos_medicos em on v.idEvento_medico = em.idEvento_medico where v.idaVolta = 2 and v.idEvento_medico = ' . $qe['idEvento_medico'];
				if ($_SESSION[$_adm] > 1) {
					$sql .= ' and em.idUsuarioNivel'.$_SESSION[$_adm].' = ' . $_SESSION[$_codigo];	
				}
				$sql .=  ' order by idVoo';
				$qr2 = mysqli_query($conexao,$sql) or die(mysqli_error());
				while ($qv = mysqli_fetch_assoc($qr2)) {
					$corpoAereo .= mostraVoo($qv);
				}
				$corpoAereo .= '
										</div>
									  </div>
									</div>
								  </div>
								</div>
							  </div>';
			}
			
//////////////////////////////////			
			
			
//			$carta = $_POST['txtDescritivo'];
			$carta = '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css"><div class="container">'.$_POST['txtDescritivo'];


			$carta = str_replace('@@nome@@', $qe['medico'], $carta);
			$carta = str_replace('@@evento@@', $qe['evento'], $carta);
			$carta = str_replace('@@data@@', $qe['dtEvento'], $carta);
			$carta = str_replace('@@local@@', $qe['local'], $carta);
			$carta = str_replace('@@transfer@@', $corpoTransfer, $carta);
			$carta = str_replace('@@voo@@', $corpoAereo, $carta);
			$carta = str_replace('"', $ql3, $carta);

			if (strlen($qe['']) > 1 ) $carta .= 'Transfer casa-aerop: ';
			
			
			$zip->addFromString('carta_'.$_SESSION[$_evento].'_'.$qe['idMedico'].'_'.$qe['medico'].'.html', $carta);
			//echo $carta.'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
			$sql = 'update eventos_medicos set qtdCartaCheckOut = (qtdCartaCheckOut + 1) WHERE idEvento_medico = ' . $qe['idEvento_medico'];
			//echo $sql;
			$qx = mysqli_query($conexao,$sql) or die(mysqli_error());
		
		}
		$zip->close();

		//echo 'ok';
	} else {
		echo 'erro: nao pode criar arq ZIP';
	}

	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
	header ("Cache-Control: no-cache, must-revalidate");
	header ("Pragma: no-cache");
	header ("Content-type: application/zip");
	header ("Content-Disposition: attachment; filename=\"{$tempName}\"" );
	readfile($tempName);
	unlink($tempName);
	exit();
	
	//header('location:index.php');
	
}


$sql = 'SELECT e.*,s.descricao FROM eventos e join statusEvento s on e.idStatusEvento = s.idStatusEvento WHERE e.idEvento = ' . $_SESSION[$_evento];
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qe = mysqli_fetch_assoc($qr);

$sql = 'SELECT t.*,et.* FROM eventos_tarefas et join tarefas t on et.idTarefa = t.idTarefa WHERE et.idEvento = ' . $_SESSION[$_evento] . ' and t.idTarefa = 9';
$qr = mysqli_query($conexao,$sql) or die(mysqli_error());
$qt = mysqli_fetch_assoc($qr);

?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include 'head.php'; ?>
            <script type="text/javascript">
                function openModal(id) {
                    $('#hId').val(id);
                    document.form1.submit();
                    //	$('#modal1').show();	
                }
            </script>
    </head>

    <body>
        <?php include 'header.php';?>
            <!-- START MAIN -->
            <div id="main">
                <!-- START WRAPPER -->
                <div class="wrapper">
                    <?php include 'navbar.php';?>
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START CONTENT -->
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <a href="index.php">Dashboard</a> > Criar carta checkout
                                <h2 class="login-form-text2">carta checkout <?php echo $qe['nome']; ?></h2>
                                <div class="row">
                                    <form name="form1" id="form1" method="post" class="col s12">
                                        <input type="hidden" name="hId" id="hId" value="<?php echo $qt['idEvento_tarefa']; ?>">
                                        <div class="input-field col l12 s12">
                                            <input id="assunto" name="assunto" type="text" class="validate">
                                            <label for="first_name">Assunto</label>
                                        </div>
                                        <textarea id='txtDescritivo' name='txtDescritivo'>
                                            <?php 
											
$mensagem  = 'Prezado (a) Doutor (a) @@nome@@

Agradecemos sua participação no @@evento@@ realizado em @@data@@, no @@local@@. 

Para agilizar o processo de saída do hotel amanhã, por favor, atente-se ao horário do seu transporte do hotel ao aeroporto e antecipe seu check-out para evitar atrasos na sua partida. 

No momento de embarcar no transfer, identifique sua bagagem no lobby e certifique-se que a mesma foi colocada no veículo.


Saída do transfer do Hotel:  

@@transfer@@


Horário do check-out: Seu check out precisa ser efetuado até as 12h. 
Sua bagagem poderá ser guardada no maleteiro do Hotel, para retirada no momento do seu transfer de saída.

Qualquer dúvida ou alteração em sua programação, por favor, entre em contato para que possamos alinhar toda a logística de sua viagem:

Coordenadora Viviani Moralis
Telefone: (11) 99119 0748

Desejamos uma excelente viagem de regresso!

Equipe Shire
';

if ($qt['txtDescritivo'] == '') 
	echo $mensagem;
else											
	echo $qt['txtDescritivo']; 

?>
                                        </textarea>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col l12 s12">
                                        LEGENDA:
                                        <br>
                                        <div class="chip">@@nome@@: Nome do médico</div>
                                        <div class="chip">@@evento@@: Nome do Evento</div>
                                        <div class="chip">@@data@@: Data do Evento</div>
                                        <div class="chip">@@local@@: Local do Evento</div>
                                        <div class="chip">@@transfer@@: Transfers</div>
                                        <div class="chip">@@voo@@: Voos</div>
                                    </div>
                                </div> 
                                <br>
                                <div class="divider"></div>
                                <br>
                                <div class="row">
                                    <p>
                                        <input type="checkbox" id="indConclusao" name="indConclusao" checked />
                                        <label for="indConclusao">Tarefa está completa?</label>
                                    </p>
                                    <p> Gerar para quem?
                                        <input name="chkParticipacao" type="radio" id="sim" value="1" />
                                        <label for="sim">Todos os medicos</label>&nbsp;OU&nbsp;
                                        <input name="chkParticipacao" type="radio" id="nao" value="0" checked />
                                        <label for="nao">Somente quem ainda nao recebeu</label>
                                    </p>
                                    <button class="btn waves-effect waves-light" type="submit" name="action">Gerar
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                                </form>
                            </div>
                            <!--end container-->
                        </section>
                        <!-- END CONTENT -->
                        <!-- //////////////////////////////////////////////////////////////////////////// -->
                        <!-- START RIGHT SIDEBAR NAV-->
                        <?php //include 'sidebar.php'; ?>
                            <!-- LEFT RIGHT SIDEBAR NAV-->

                </div>
                <!-- END WRAPPER -->

            </div>
            <!-- END MAIN -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START FOOTER -->
            <footer class="page-footer">
                <div class="footer-copyright">
                    <div class="container">
                        Copyright © 2016 <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br" target="_blank">AHCME</a> Todos os reservados.
                        <span class="right"> Designed by <a class="grey-text text-lighten-4" href="http://www.ahcme.com.br">AHCME</a></span>
                    </div>
                </div>
            </footer>
            <!-- END FOOTER -->


            <!-- ================================================
            Scripts
            ================================================ -->

            <!-- jQuery Library -->
            <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
            <!--materialize js-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
            <!--scrollbar-->
            <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
            <!--plugins.js - Some Specific JS codes for Plugin Settings-->
            <script type="text/javascript" src="js/plugins.min.js"></script>
            <script src="ckeditor/ckeditor.js"></script>
            <script>
                CKEDITOR.replace('txtDescritivo', {
                    extraPlugins: 'imageuploader'
                });
            </script>

    </body>

    </html>