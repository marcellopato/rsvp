<!-- START HEADER -->
<header id="header" class="page-topbar"> 
<!-- start header nav-->
<div class="navbar-fixed">
<nav class="navbar-color">
    <div class="nav-wrapper">
        <ul class="left">
            <li>
                <h1 class="logo-wrapper"><a href="index.php" class="brand-logo darken-1"><img src="assets/img/logo-horizontal.png" alt="ganbatte"></a> <span class="logo-text">ganbatte</span></h1>
            </li>
        </ul>
        <ul class="right hide-on-med-and-down black-text">
            <!--<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen black-text"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a> </li>
            <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button black-text" data-activates="notifications-dropdown"><i class="fa fa-bell-o" aria-hidden="true"><small class="notification-badge">5</small></i> </a> </li>
            <li><a href="#!" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse black-text"><i class="fa fa-comments-o" aria-hidden="true"></i></a> </li> -->
<?php
if (strpos($_SERVER['REQUEST_URI'], 'OMOLOGA') > 0)
	echo 'Homologação';
?>        
            <li><a href="login.php" class="waves-effect waves-block waves-light black-text"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
        </ul>
        <ul id="notifications-dropdown" class="dropdown-content">
        </ul>
    </div>
</nav>
</div>
<!-- end header nav--> 
</header>
<!-- END HEADER --> 
